import tensorflow as tf
K = tf.keras.backend

def create_optimizer(params):
  return tf.keras.optimizers.Adam(1e-6,
      beta_1=params['optimizer_adam_beta1'],
      beta_2=params['optimizer_adam_beta2'], 
      epsilon=params['optimizer_adam_epsilon'])

class CustomSchedule(tf.keras.optimizers.schedules.LearningRateSchedule):
  def __init__(self, d_model, learning_rate_modifier, warmup_steps):
    super(CustomSchedule, self).__init__()
    
    self.d_model = tf.cast(d_model, tf.float32)
    self.warmup_steps = tf.cast(warmup_steps, tf.float32)
    self.learning_rate_modifier = learning_rate_modifier
    
  def __call__(self, step):
    arg1 = tf.math.rsqrt(step)
    arg2 = step * (self.warmup_steps ** -1.5)
    
    return self.learning_rate_modifier * tf.math.rsqrt(self.d_model) * tf.math.minimum(arg1, arg2)

class LearningRateCallback(tf.keras.callbacks.Callback):

  def __init__(self, params):
    super(LearningRateCallback, self).__init__()

    self.d_model = tf.cast(params['d_model'], tf.float32)
    self.warmup_steps = tf.cast(params['warmup_steps'], tf.float32)
    self.learning_rate_modifier = tf.cast(params['learning_rate_modifier'], tf.float32)

  def on_train_batch_begin(self, batch, logs=None):
    step = self.model.optimizer.iterations
    step = tf.cast(step, tf.float32)

    arg1 = tf.math.rsqrt(step)
    arg2 = step * (self.warmup_steps ** -1.5)
    lr = self.learning_rate_modifier * tf.math.rsqrt(self.d_model) * tf.math.minimum(arg1, arg2)

    K.set_value(self.model.optimizer.lr, lr)
  
  def on_train_batch_end(self, batch, logs=None):
    if self.model.optimizer.iterations.numpy() % 1000 == 0:
      print(" - ", logs['loss'], 
          self.model.optimizer.iterations.numpy(), 
          self.model.optimizer.lr.numpy())

  def on_epoch_end(self, epoch, logs=None):
    logs = logs or {}
    logs['lr'] = self.model.optimizer.lr.numpy()
    logs['steps'] = self.model.optimizer.iterations.numpy()
