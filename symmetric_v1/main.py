import tensorflow as tf
import tensorflow_datasets as tfds
import os
from symmetric_v1 import symmetric_transformer as st
from symmetric_v1 import optimizer
from symmetric_v1 import metrics
from symmetric_v1 import data_pipeline

class MainTask:
  def __init__(self, params):
    self.params = params

    try:
      resolver = tf.distribute.cluster_resolver.TPUClusterResolver(tpu='grpc://' + os.environ['COLAB_TPU_ADDR'])
      tf.config.experimental_connect_to_cluster(resolver)
      tf.tpu.experimental.initialize_tpu_system(resolver)
      self.distribution_strategy = tf.distribute.experimental.TPUStrategy(resolver)
      self.params['static_batch'] = True
    except:
      self.distribution_strategy = tf.distribute.MirroredStrategy()
      self.params['static_batch'] = False
  
  def save_cp_as_weights(self, weight_path):
    params = self.params

    with self.distribution_strategy.scope():
      model = st.create_training_model(params)

      checkpoint_path = params['model_dir']

      ckpt = tf.train.Checkpoint(model=model)

      ckpt_manager = tf.train.CheckpointManager(ckpt, checkpoint_path, max_to_keep=params['max_to_keep'])

      if ckpt_manager.latest_checkpoint:
        ckpt.restore(ckpt_manager.latest_checkpoint)
        print ('Latest checkpoint restored!!')
        model.compile()
        model.summary()
        model.save_weights(weight_path)
      else:
        print ('no checkpoint found!!')

  def load_weights(self, weight_path):
    params = self.params

    with self.distribution_strategy.scope():
      model = st.create_training_model(params)

      model.load_weights(weight_path)

    model.summary()

    ds = data_pipeline.get_train_ds(params)

    for (ex, _) in ds.take(1):
      print(model(ex))

  def train(self):
    params = self.params

    with self.distribution_strategy.scope():
      model = st.create_training_model(params)
      opt = optimizer.create_optimizer(params)

      checkpoint_path = params['model_dir']

      ckpt = tf.train.Checkpoint(model=model, optimizer=opt)

      ckpt_manager = tf.train.CheckpointManager(ckpt, checkpoint_path, max_to_keep=params['max_to_keep'])

      current_step = 0
      if ckpt_manager.latest_checkpoint:
        ckpt.restore(ckpt_manager.latest_checkpoint)
        print ('Latest checkpoint restored!!')
        current_step = opt.iterations.numpy()
      
      print('current train step', current_step)
      model.compile(optimizer=opt, loss="sparse_categorical_crossentropy")

    model.summary()

    train_ds = data_pipeline.get_train_ds(params)

    callbacks = self._create_callbacks(ckpt_manager)

    current_epoch = 0
    while current_epoch < params['epochs']:
      model.fit(train_ds,
          initial_epoch=current_epoch,
          epochs=current_epoch + 1,
          steps_per_epoch=params['steps_per_epoch'],
          callbacks=callbacks,
          verbose=params['verbose'])

      ckpt_manager.save()

      current_epoch += 1

  def eval(self):
    params = self.params

    with self.distribution_strategy.scope():
      model = st.create_training_model(params)

      checkpoint_path = params['model_dir']

      ckpt = tf.train.Checkpoint(model=model)

      ckpt_manager = tf.train.CheckpointManager(ckpt, checkpoint_path, max_to_keep=params['max_to_keep'])

      # if a checkpoint exists, restore the latest checkpoint.
      if ckpt_manager.latest_checkpoint:
        ckpt.restore(ckpt_manager.latest_checkpoint).expect_partial()
        print ('Latest checkpoint restored!!')
      
      model.compile(metrics=['accuracy', metrics.MaskedAccuracy()])

    model.summary()

    ds = data_pipeline.get_test_ds(params)

    if params['single_batch_test']:
      ds = ds.take(1)

    model.evaluate(ds)

    return

  def _create_callbacks(self, ckpt_manager=None):
    callbacks = []
    learning_rate_callback = optimizer.LearningRateCallback(self.params)
    callbacks.append(learning_rate_callback)
    # if ckpt_manager:
    #   ckpt_callback = CheckpointCallback(ckpt_manager)
    #   callbacks.append(ckpt_callback)
    return callbacks

class CheckpointCallback(tf.keras.callbacks.Callback):

  def __init__(self, ckpt_manager):
    super(CheckpointCallback, self).__init__()

    self.ckpt_manager = ckpt_manager

  def on_train_batch_end(self, batch, logs=None):
    if self.model.optimizer.iterations.numpy() < 50000:
      return
    if self.model.optimizer.iterations.numpy() % 10000 == 0:
      self.ckpt_manager.save()
      print("checkpoint saved")

