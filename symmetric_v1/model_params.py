from collections import defaultdict

BASE_PARAMS = defaultdict(
    lambda: None,

    max_to_keep=1,
    data_dir='',
    data_name='',
    model_dir='',
    vocab_path='',

    epochs=1,
    steps_per_epoch=10000,
    verbose=1,

    d_model=512,
    num_heads=8,
    num_layers=3,
    dff=2048,
    vocab_size=32698,
    # vocab_size=33708,
    max_length=256,
    dropout_rate=0.1,
    batch_size=1024,
    test_batch_size=128,
    label_smoothing=0.1,
    shuffle_buffer_size=None,
    single_batch_test=False,

    learning_rate_modifier=1.0,
    warmup_steps=4000,

    optimizer_adam_beta1=0.9,
    optimizer_adam_beta2=0.98,
    optimizer_adam_epsilon=1e-09,

    num_gpus=1,
)
