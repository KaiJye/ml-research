import tensorflow as tf
from common import embedding_layer, attention_layer, ffn_layer, model_utils

def create_training_model(params):
  inputs = tf.keras.layers.Input((None,), dtype=tf.int64)
  internal_model = SymmetricTransformer(params["num_layers"], params["d_model"],
      params["num_heads"], params["dff"], params["dropout_rate"], params["vocab_size"])
  logits = internal_model(inputs, mode="mirrored")
  model = tf.keras.Model(inputs, logits)
  return model

def create_embedding_model(params):
  inputs = tf.keras.layers.Input((None,), dtype=tf.int64)
  internal_model = SymmetricTransformer(params["num_layers"], params["d_model"],
      params["num_heads"], params["dff"], params["dropout_rate"], params["vocab_size"])
  logits = internal_model(inputs)
  model = tf.keras.Model(inputs, logits)
  return model

class SymmetricLayer(tf.keras.layers.Layer):
  def __init__(self, d_model, num_heads, dff, dropout_rate):
    super(SymmetricLayer, self).__init__()
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate

  def build(self, input_shape):
    self.self_attention = attention_layer.SelfAttention(self.d_model, self.num_heads, self.dropout_rate)
    self.attention_norm = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.ffn = ffn_layer.FeedForwardNetwork(self.d_model, self.dff, self.dropout_rate)
    self.ffn_norm = tf.keras.layers.LayerNormalization(epsilon=1e-6)

    super(SymmetricLayer, self).build(input_shape)

  def call(self, x, attention_mask, training):
    attention_output = self.self_attention(x, attention_mask, training=training)  # (batch_size, input_seq_len, d_model)
    if training:
      attention_output = tf.nn.dropout(attention_output, rate=self.dropout_rate)
    attention_output = self.attention_norm(x + attention_output)  # (batch_size, input_seq_len, d_model)
    
    ffn_output = self.ffn(attention_output, training=training)  # (batch_size, input_seq_len, d_model)
    if training:
      ffn_output = tf.nn.dropout(ffn_output, rate=self.dropout_rate)
    ffn_output = self.ffn_norm(attention_output + ffn_output)  # (batch_size, input_seq_len, d_model)

    return ffn_output

class SymmetricTransformer(tf.keras.Model):
  def __init__(self, num_layers, d_model, num_heads, dff, dropout_rate, vocab_size):
    super(SymmetricTransformer, self).__init__()
    self.num_layers = num_layers
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate    
    self.vocab_size = vocab_size    

  def build(self, input_shape):
    self.embedding_softmax_layer = embedding_layer.EmbeddingSharedWeights(
        self.vocab_size, self.d_model)
    self.layernorm = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.attention_layers = [SymmetricLayer(self.d_model, self.num_heads, self.dff, self.dropout_rate)
        for _ in range(self.num_layers)]

    super(SymmetricTransformer, self).build(input_shape)

  def call(self, x, mode="mirrored", training=None):
    if mode == "mirrored":
      x, attention_mask = self.forward(x, training=training)
      x = self.backward(x, attention_mask, training=training)
      return x
    elif mode == "forward":
      x, attention_mask = self.forward(x, training=training)
      return x, attention_mask
    elif mode == "backward":
      x, attention_mask = x[0], x[1]
      x = self.backward(x, attention_mask, training=training)
      return x
  
  def forward(self, x, training):
    attention_mask = model_utils.get_padding_bias(x)
    x = self.embedding_softmax_layer(x)  # (batch_size, target_seq_len, d_model)

    pos_encoding = model_utils.get_position_encoding(
        tf.shape(x)[1],
        self.d_model)
    x += pos_encoding

    if training:
      x = tf.nn.dropout(x, rate=self.dropout_rate)

    x = self.layernorm(x)

    for layer in self.attention_layers:
      x = layer(x, attention_mask, training=training) 

    return x, attention_mask

  def backward(self, x, attention_mask, training):
    for layer in reversed(self.attention_layers):
      x = layer(x, attention_mask, training=training)
  
    x = self.embedding_softmax_layer(x, mode="linear")

    return x