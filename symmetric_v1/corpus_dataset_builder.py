import sys
import tensorflow as tf
import tensorflow_datasets as tfds
from common import dataset_utils
import re

NUM_THREAD = 4
SHARDS = 10
ARTICLES = 100
SENTENCES = 4000000
FILE_PREFIX = "test"
SPLIT = tfds.Split.TEST
VOCAB_PATH = r"D:/vocab/vocab_en_de_v3"
MAX_LENGTH = 256

START = "_START_PARAGRAPH_"
START_LEN = len(START)
END = "_START_SECTION_"
END_LEN = len(END)

vocab = tfds.features.text.SubwordTextEncoder.load_from_file(VOCAB_PATH)

def gen(raw):
  text = raw.decode('utf-8')
  clean = ''
  while True:
    start_index = text.index(START)
    try:
      end_index = text.index(END)
      clean = clean + ' ' + text[start_index + START_LEN : end_index]
      text = text[end_index + END_LEN :]
    except:
      clean = clean + ' ' + text[start_index + START_LEN :]
      break
  clean = re.sub(r"(_NEWLINE_)", ' ', clean)
  clean = re.sub(dataset_utils.PUNC_PATTERN, '', clean)
  clean = re.sub(r"(?<=[\.!\?])\s", '#', clean)
  clean = re.sub(r"([,\.!\?\'\"$%&()\-:;@])", r" \1 ", clean)
  clean = re.sub(r"\s+", ' ', clean)
  splits = clean.split(r"#")
  for split in splits:
    split = split.lstrip()
    yield vocab.encode(split)

def flat_map_fn(raw):
  return tf.data.Dataset.from_generator(
      gen, tf.int64,
      args=(raw['text'],))

def build(lang):
  ds = tfds.load(
        name=f"wiki40b/Wiki40B.{lang}", 
        data_dir=r"D:\data", 
        split=SPLIT,
        )

  ds = ds.flat_map(flat_map_fn)

  ds = ds.filter(lambda x: tf.size(x) <= MAX_LENGTH)

  ds = ds.map(tf.io.serialize_tensor)

  ds = ds.take(SENTENCES)

  if SPLIT == tfds.Split.TRAIN:
    ds = ds.cache(rf"D:/data/cache/wiki_{lang}_encode_serialize")
  else:
    ds = ds.cache(r"D:/data/cache/temp")

  for i in range(SHARDS):
    print(f"building {lang} shard: {i}")
    writer = tf.data.experimental.TFRecordWriter(f"D:/data/wiki_{lang}_preprocessed_v1/{FILE_PREFIX}-{i}.tfrecord")
    writer.write(ds.shard(SHARDS, i))

def build_2(lang):
  raw_ds = tfds.load(
        name=f"wiki40b/{lang}", 
        data_dir=r"D:\data", 
        split=SPLIT,
        )

  # raw_ds = raw_ds.take(ARTICLES)
  # raw_ds = raw_ds.take(10)

  for i in range(SHARDS):
    # if i % NUM_THREAD - int(sys.argv[1]) != 0:
    #   continue
    print(f"building {lang} shard: {i}")

    shard_ds = raw_ds.shard(SHARDS, i)

    shard_ds = shard_ds.flat_map(flat_map_fn)

    shard_ds = shard_ds.filter(lambda x: tf.size(x) <= MAX_LENGTH)

    shard_ds = shard_ds.map(tf.io.serialize_tensor)

    writer = tf.data.experimental.TFRecordWriter(f"D:/data/wiki_{lang}_preprocessed_v2/{FILE_PREFIX}-{i}.tfrecord")
    writer.write(shard_ds)

def build_combined():
  en_ds = tfds.load(
        name=f"wiki40b/Wiki40B.en", 
        data_dir=r"D:\data", 
        split=SPLIT,
        )

  de_ds = tfds.load(
        name=f"wiki40b/Wiki40B.de", 
        data_dir=r"D:\data", 
        split=SPLIT,
        )
  
  en_ds = en_ds.flat_map(flat_map_fn)

  de_ds = de_ds.flat_map(flat_map_fn)

  ds = tf.data.Dataset.zip((en_ds, de_ds))

  def zip_gen(en, de):
    yield en
    yield de

  def combine_flat_map_fn(en, de):
    return tf.data.Dataset.from_generator(
        zip_gen, tf.int64, args=(en, de))

  ds = ds.flat_map(combine_flat_map_fn)

  ds = ds.filter(lambda x: tf.size(x) <= MAX_LENGTH)

  ds = ds.map(tf.io.serialize_tensor)

  ds = ds.take(SENTENCES)

  if SPLIT == tfds.Split.TRAIN:
    ds = ds.cache(r"D:/data/cache/wiki_combined_encode_serialize")
  else:
    ds = ds.cache(r"D:/data/cache/temp")

  for i in range(SHARDS):
    print(f"building combined shard: {i}")
    writer = tf.data.experimental.TFRecordWriter(f"D:/data/wiki_combined_preprocessed_v1/{FILE_PREFIX}-{i}.tfrecord")
    writer.write(ds.shard(SHARDS, i))

# build_combined()
# build('de')
build_2('de')