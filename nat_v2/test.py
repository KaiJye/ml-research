import tensorflow as tf

a = tf.constant([5,2,3,4,5,6,7,8,1,1,1,0,0,0,0,0,0])
not_zeros = tf.where(tf.logical_and(tf.not_equal(a, 0), tf.not_equal(a, 1)))
a = tf.gather_nd(a, not_zeros)
print(a)