import tensorflow as tf
import tensorflow_datasets as tfds
import os, json
from unlabelled_transformer_v2 import unlabelled_transformer as ut
from transformer_v1 import optimizer
from unlabelled_transformer_v2 import metrics
from nat_v2 import data_pipeline
from common import evaluate as eval_local
from transformer_v1 import evaluate as eval_v1

class MainTask:
  def __init__(self, params, resolver=None, epoch_callback=None):
    self.params = params
    self.epoch_callback = epoch_callback

    if resolver:
      self.distribution_strategy = tf.distribute.experimental.TPUStrategy(resolver)
      self.params['static_batch'] = True
    else:
      self.distribution_strategy = tf.distribute.MirroredStrategy()
      self.params['static_batch'] = False

  def train(self):
    params = self.params

    model_dir = params["model_dir"]
    model_name = params["model_name"]

    model_path = os.path.join(model_dir, f"{model_name}_model.h5")
    train_state_path = os.path.join(model_dir, f"{model_name}_train_state.json")

    with self.distribution_strategy.scope():
      model = ut.create_training_model(params)
      opt = optimizer.create_optimizer(params)
      current_step = 0
      
      try:
        model.load_weights(model_path)
        print("model loaded")
      except:
        print("create new model")

      try:
        with open(train_state_path) as json_file:
          train_state = json.load(json_file)
          opt.iterations = tf.Variable(train_state["steps"])
          current_step = opt.iterations.numpy()
      except:
        train_state = {}
        train_state["steps"] = 0
        train_state["loss"] = []
        # train_state["accuracy"] = []

      print(f"current train step: {current_step}")

      loss_fn = metrics.loss_function
      accuracy_function = metrics.accuracy_function
      model.compile(optimizer=opt, 
          loss={ "output": loss_fn, "length": tf.keras.losses.MAE }, 
          metrics={ "output": [accuracy_function], "length": [tf.keras.metrics.MAE] },
          loss_weights={ "output": 2, "length": 1 })

    model.summary()

    train_ds = data_pipeline.get_train_ds(params)
    validation_ds = data_pipeline.get_validation_ds(params)

    callbacks = self._create_callbacks()

    current_epoch = 0
    while current_epoch < params['epochs']:
      history = model.fit(train_ds,
          validation_data=validation_ds,
          initial_epoch=current_epoch,
          epochs=current_epoch + 1,
          steps_per_epoch=params['steps_per_epoch'],
          callbacks=callbacks,
          verbose=params['verbose'])

      print(history.history)

      model.save_weights(model_path, save_format='h5')
      train_state["steps"] = int(history.history["steps"][0])
      train_state["loss"].append(float(history.history["loss"][0]))
      # train_state["accuracy"].append(float(history.history["accuracy_function"][0]))
      with open(train_state_path, 'w') as json_file:
        json.dump(train_state, json_file)

      if self.epoch_callback:
        self.epoch_callback()

      current_epoch += 1

  def eval(self):
    params = self.params

    with self.distribution_strategy.scope():
      model = ut.create_predict_model(params)
      
      try:
        model.load_weights(params["model_path"])
        print("model loaded")
      except:
        print("create new model")

      model.compile()

    model.summary()

    test_ds = data_pipeline.get_test_ds(params)

    if params['single_batch_test']:
      test_ds = test_ds.take(2)

    test_ds = test_ds.map(lambda x, y: ((x[0], y[0]),))

    pred, tar = model.predict(test_ds)

    pred = eval_local.decode(params, tf.constant(pred))
    tar = eval_local.decode(params, tar)
    print(pred)
    print(tar)

    # reference = tf.constant([], tf.string)
    # translated = tf.constant([], tf.string)
    # for (inp, length), (tar, length) in test_ds:
    #   de = eval_local.decode(params, tar)
    #   reference = tf.concat([reference, de], axis=0)
    #   ids = model(inp, training=False)
    #   translated_batch = eval_local.decode(params, ids)
    #   translated = tf.concat([translated, translated_batch], axis=0)

    # bleu_score = eval_v1.compute_bleu(reference, translated)

    # return bleu_score

  def _create_callbacks(self, ckpt_manager=None):
    callbacks = []
    learning_rate_callback = optimizer.LearningRateCallback(self.params)
    callbacks.append(learning_rate_callback)
    # if ckpt_manager:
    #   ckpt_callback = CheckpointCallback(ckpt_manager)
    #   callbacks.append(ckpt_callback)
    return callbacks

class CheckpointCallback(tf.keras.callbacks.Callback):

  def __init__(self, ckpt_manager):
    super(CheckpointCallback, self).__init__()

    self.ckpt_manager = ckpt_manager

  def on_train_batch_end(self, batch, logs=None):
    if self.model.optimizer.iterations.numpy() < 50000:
      return
    if self.model.optimizer.iterations.numpy() % 10000 == 0:
      self.ckpt_manager.save()
      print("checkpoint saved")

