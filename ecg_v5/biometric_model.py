import tensorflow as tf
import math
from ecg_v3 import biometric_model as v3
from common import attention_layer, ffn_layer, model_utils


def create_training_model(params, return_logit=False):
  d0 = tf.keras.layers.Input([None, None], dtype=tf.float32)
  d1 = tf.keras.layers.Input([None, None], dtype=tf.float32)
  internal_model = CustomModel(params, return_logit=return_logit)
  logits = internal_model([d0, d1])
  model = tf.keras.Model([d0, d1], logits)
  return model, internal_model


class CustomModel(tf.keras.Model):

  def __init__(self, params, return_logit=False):
    super(CustomModel, self).__init__()
    self.params = params
    self.return_logit = return_logit
    self.pos = model_utils.positional_encoding(512, params["d_model"])


  def build(self, input_shape):
    params = self.params

    self.conv = v3.ConvLayer(params['num_conv_layers'], params['filter_size'], 
        params['kernel_size'], params['conv_strides'], params['pool_size'], params['dropout_rate'])

    # self.converter = tf.keras.layers.Dense(params['d_model'], use_bias=False)

    self.clz = self.add_weight(
        "clz",
        shape=[params["d_model"]],
        initializer='glorot_uniform')
    
    self.first_enc_norm = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.first_dec_norm = tf.keras.layers.LayerNormalization(epsilon=1e-6)

    self.encoder = Encoder(params["num_encoder_layers"], params["d_model"],
        params["num_heads"], params["dff"], params["dropout_rate"])

    self.decoder = Decoder(params["num_decoder_layers"], params["d_model"],
        params["num_heads"], params["dff"], params["dropout_rate"])

    self.ffn_0 = tf.keras.layers.Dense(params['d_model'], activation=tf.nn.relu)
    self.final_norm_0 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.ffn_1 = tf.keras.layers.Dense(params['d_model'] / 2, activation=tf.nn.relu)
    self.final_norm_1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.final = tf.keras.layers.Dense(1)

    super(CustomModel, self).build(input_shape)

  def call(self, inputs, training=None):
    batch_size = tf.shape(inputs[0])[0]
    num_left = tf.shape(inputs[0])[1]
    num_right = tf.shape(inputs[1])[1]
    d_model = self.params['d_model']

    # padding_value = tf.constant(-1e9, dtype=tf.float32)
    # left = tf.where(tf.math.is_finite(inputs[0]), inputs[0], padding_value)
    # right = tf.where(tf.math.is_finite(inputs[1]), inputs[1], padding_value)
    
    left = inputs[0]
    right = inputs[1]

    left_conv_out, left_conv_mask = self.conv(left)
    # left_conv_out = self.converter(left_conv_out)

    segment_pos = self.pos[:num_left, tf.newaxis, :]
    left_conv_out += segment_pos

    left_conv_out = tf.where(tf.equal(left_conv_mask, 0), left_conv_out, tf.constant(0, tf.float32))

    if training:
      left_conv_out = tf.nn.dropout(left_conv_out, rate=self.params["dropout_rate"])
    left_conv_out = self.first_enc_norm(left_conv_out)

    left_conv_out = tf.reshape(left_conv_out, [batch_size, -1, d_model])
    left_conv_mask = tf.reshape(left_conv_mask, [batch_size, -1])
    left_mask = model_utils.get_padding_bias(left_conv_mask, padding_value=1)

    left_eye = tf.eye(tf.shape(left_conv_out)[1]) * -1e9
    left_logits = self.encoder([left_conv_out, tf.minimum(left_mask, left_eye)], training=training)

    right_conv_out, right_conv_mask = self.conv(right)
    # right_conv_out = self.converter(right_conv_out)

    segment_pos = self.pos[:num_right, tf.newaxis, :]
    right_conv_out += segment_pos

    right_conv_out = tf.where(tf.equal(right_conv_mask, 0), right_conv_out, tf.constant(0, tf.float32))

    if training:
      right_conv_out = tf.nn.dropout(right_conv_out, rate=self.params["dropout_rate"])
    right_conv_out = self.first_dec_norm(right_conv_out)

    right_conv_out = tf.reshape(right_conv_out, [batch_size, -1, d_model])
    right_conv_mask = tf.reshape(right_conv_mask, [batch_size, -1])
    right_conv_mask = tf.pad(right_conv_mask, [[0, 0], [1, 0]], constant_values=0)
    right_mask = model_utils.get_padding_bias(right_conv_mask, padding_value=1)

    clz = self.clz[tf.newaxis, tf.newaxis, :]
    clz = tf.repeat(clz, batch_size, axis=0)
    right_conv_out = tf.concat([clz, right_conv_out], 1)
    
    right_eye = tf.eye(tf.shape(right_conv_out)[1]) * -1e9
    right_logits = self.decoder([left_logits, right_conv_out, left_mask, tf.minimum(right_mask, right_eye)], training=training)

    # logits = right_logits
    logits = right_logits[:, 0, :]

    if self.return_logit:
      return tf.reshape(logits, [batch_size, -1])

    ffn_out = self.ffn_0(logits)

    if training:
      ffn_out = tf.nn.dropout(ffn_out, rate=self.params["dropout_rate"])

    logits = self.final_norm_0(logits + ffn_out)

    logits = self.ffn_1(logits)

    if training:
      logits = tf.nn.dropout(logits, rate=self.params["dropout_rate"])

    logits = self.final_norm_1(logits)

    logits = self.final(logits)

    # logits = tf.reshape(logits, [batch_size, -1])
    # reduce_mask = tf.cast(right_conv_mask, tf.float32) * -1e9
    # logits += reduce_mask
    # logits = tf.math.reduce_max(logits, axis=-1)

    logits = tf.reshape(logits, [batch_size])

    if not training:
      logits = tf.sigmoid(logits)
    
    return logits


class ConvLayer(tf.keras.layers.Layer):
  def __init__(self, num_conv_layers, filter_size, kernel_size, conv_strides, pool_size, dropout_rate):
    super(ConvLayer, self).__init__()
    self.num_conv_layers = num_conv_layers
    self.filter_size = filter_size
    self.kernel_size = kernel_size
    self.conv_strides = conv_strides
    self.pool_size = pool_size
    self.dropout_rate = dropout_rate

    k = kernel_size - 1
    self.kernel_reduce = int(k)
    self.lower_kernel_reduce = int(math.floor(float(k/2)))
    self.upper_kernel_reduce = int(math.ceil(float(k/2)))

  def build(self, input_shape):
    self.mask_conv = tf.keras.layers.Conv1D(1, self.kernel_size,
        padding='valid', strides=self.conv_strides,
        kernel_initializer=tf.keras.initializers.Constant(1), use_bias=False)
    self.mask_conv.trainable = False
    
    self.logit_pool = tf.keras.layers.MaxPool1D(pool_size=self.pool_size,
        strides=None, padding='valid')

    self.internal_layers = [{
      "conv": tf.keras.layers.Conv1D(self.filter_size, self.kernel_size,
          padding='same', strides=self.conv_strides,
          kernel_initializer='glorot_uniform', use_bias=True, 
          kernel_regularizer='l1_l2', bias_regularizer='l1_l2'),
      "norm": tf.keras.layers.LayerNormalization(epsilon=1e-6),
    } for _ in range(self.num_conv_layers)]
    super(ConvLayer, self).build(input_shape)

  def call(self, inputs, training=None):
    logits = inputs[:, :, tf.newaxis]

    mask = tf.cast(tf.less(logits, -1e7), tf.float32)
    
    for i, layer in enumerate(self.internal_layers):
      conv = layer["conv"]
      norm = layer["norm"]

      internal_output = conv(logits)
      if i == self.num_conv_layers - 1:
        internal_output = tf.nn.swish(internal_output)
      else:
        internal_output = tf.nn.swish(internal_output)

      if training:
        internal_output = tf.nn.dropout(internal_output, rate=self.dropout_rate)
      logits = norm(logits[:, ::self.conv_strides, :] + internal_output)

      mask = self.mask_conv(mask)
      mask = tf.cast(tf.not_equal(mask, 0), tf.float32)
    
      # logits = logits[:, self.lower_kernel_reduce:-1*self.upper_kernel_reduce, :]
      d_length = tf.cast(tf.shape(logits)[1] - tf.shape(mask)[1], tf.float32)
      lower_kernel_reduce = tf.cast(tf.math.floor(d_length / 2), tf.int32)
      upper_kernel_reduce = tf.cast(tf.math.ceil(d_length / 2), tf.int32)
      logits = logits[:, lower_kernel_reduce:-1*upper_kernel_reduce, :]

    logits = self.logit_pool(logits)

    mask = self.logit_pool(mask)
    mask = tf.cast(tf.not_equal(mask, 0), tf.int64)

    return logits, mask


class Encoder(tf.keras.layers.Layer):
  def __init__(self, num_layers, d_model, num_heads, dff, dropout_rate):
    super(Encoder, self).__init__()
    self.num_layers = num_layers
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate

  def build(self, input_shape):
    self.internal_layers = [{
      "self_attention": attention_layer.SelfAttention(self.d_model, self.num_heads, self.dropout_rate),
      "attention_norm": tf.keras.layers.LayerNormalization(epsilon=1e-6),
      "ffn": ffn_layer.FeedForwardNetwork(self.d_model, self.dff, self.dropout_rate),
      "ffn_norm": tf.keras.layers.LayerNormalization(epsilon=1e-6)
    } for _ in range(self.num_layers)]
    super(Encoder, self).build(input_shape)

  def call(self, inputs, training=None):
    logits, mask = inputs[0], inputs[1]
    
    for layer in self.internal_layers:
      self_attention = layer["self_attention"]
      attention_norm = layer["attention_norm"]
      ffn = layer["ffn"]
      ffn_norm = layer["ffn_norm"]

      attn_output = self_attention(logits, bias=mask, training=training)
      if training:
        attn_output = tf.nn.dropout(attn_output, rate=self.dropout_rate)
      logits = attention_norm(logits + attn_output)
      
      ffn_output = ffn(logits, training=training)
      if training:
        ffn_output = tf.nn.dropout(ffn_output, rate=self.dropout_rate)
      logits = ffn_norm(logits + ffn_output)
    
    return logits


class Decoder(tf.keras.layers.Layer):
  def __init__(self, num_layers, d_model, num_heads, dff, dropout_rate):
    super(Decoder, self).__init__()
    self.num_layers = num_layers
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate

  def build(self, input_shape):
    self.internal_layers = [{
      "self_attention": attention_layer.SelfAttention(self.d_model, self.num_heads, self.dropout_rate),
      "self_attention_norm": tf.keras.layers.LayerNormalization(epsilon=1e-6),
      "cross_attention": attention_layer.Attention(self.d_model, self.num_heads, self.dropout_rate),
      "cross_attention_norm": tf.keras.layers.LayerNormalization(epsilon=1e-6),
      "ffn": ffn_layer.FeedForwardNetwork(self.d_model, self.dff, self.dropout_rate),
      "ffn_norm": tf.keras.layers.LayerNormalization(epsilon=1e-6)
    } for _ in range(self.num_layers)]
    super(Decoder, self).build(input_shape)

  def call(self, inputs, training=None):
    enc_logits, logits, enc_mask, dec_mask = inputs[0], inputs[1], inputs[2], inputs[3]
    
    for layer in self.internal_layers:
      self_attention = layer["self_attention"]
      self_attention_norm = layer["self_attention_norm"]
      cross_attention = layer["cross_attention"]
      cross_attention_norm = layer["cross_attention_norm"]
      ffn = layer["ffn"]
      ffn_norm = layer["ffn_norm"]

      cross_output = cross_attention(logits, enc_logits, bias=enc_mask, training=training)
      if training:
        cross_output = tf.nn.dropout(cross_output, rate=self.dropout_rate)
      logits = cross_attention_norm(logits + cross_output)
      
      self_output = self_attention(logits, bias=dec_mask, training=training)
      if training:
        self_output = tf.nn.dropout(self_output, rate=self.dropout_rate)
      logits = self_attention_norm(logits + self_output)
      
      ffn_output = ffn(logits, training=training)
      if training:
        ffn_output = tf.nn.dropout(ffn_output, rate=self.dropout_rate)
      logits = ffn_norm(logits + ffn_output)

    return logits

