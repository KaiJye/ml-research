import tensorflow as tf
import tensorflow_datasets as tfds
from common import dataset_utils

VOCAB_PATH = r"D:/vocab/vocab_en_de_v3"

def gen(de, en):
  yield de
  yield en

def flat_map_fn(de, en):
  return tf.data.Dataset.from_generator(
      gen, tf.string,
      args=(de, en))

raw_ds = tfds.load(
      name="wmt14_translate/de-en", 
      data_dir="D:/data", 
      split=tfds.Split.TRAIN,
      shuffle_files=False,
      as_supervised=True)

raw_ds = raw_ds.filter(dataset_utils.filter_fn)

raw_ds = raw_ds.map(dataset_utils.tf_replace_punctuation_fn, num_parallel_calls=tf.data.experimental.AUTOTUNE)

raw_ds = raw_ds.cache(r"D:\data\cache\en_de_filter_clean_punc")

# raw_ds = raw_ds.take(1000)

# for de, en in raw_ds:
#   de = de.numpy().decode('utf-8')
#   en = en.numpy().decode('utf-8')
#   print(de)
#   print(len(de))
#   print("***")
#   print(en)
#   print(len(en))
#   print("===")

raw_ds = raw_ds.flat_map(flat_map_fn)

vocab_en_de = tfds.features.text.SubwordTextEncoder.build_from_corpus(
    [ex.numpy() for ex in raw_ds], 
    target_vocab_size=2**15, reserved_tokens=['<pad>', '<SOS>', '<EOS>', '<pos>', 
    '<emp>', '<mas>', '<sep>', '<cls>', '<cu9>', '<cu10>', '<cu11>', '<cu12>', 
    '<cu13>', '<cu14>', '<cu15>', '<cu16>'])

print(vocab_en_de.vocab_size)

vocab_en_de.save_to_file(VOCAB_PATH)