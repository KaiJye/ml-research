import tensorflow as tf

def _pad_tensors_to_same_length(x, y):
  x_length = tf.shape(x)[1]
  y_length = tf.shape(y)[1]

  max_length = tf.maximum(x_length, y_length)

  x = tf.pad(x, [[0, 0], [0, max_length - x_length], [0, 0]])
  y = tf.pad(y, [[0, 0], [0, max_length - y_length]])
  return x, y

def loss_function(pred, real, smoothing, vocab_size):
  pred, real = _pad_tensors_to_same_length(pred, real)
  
  confidence = 1.0 - smoothing
  low_confidence = smoothing / tf.cast(vocab_size - 1, tf.float32)
  soft_targets = tf.one_hot(
      tf.cast(real, tf.int64),
      depth=vocab_size,
      on_value=confidence,
      off_value=low_confidence)
  xentropy = tf.nn.softmax_cross_entropy_with_logits(
      logits=pred, labels=soft_targets)

  normalizing_constant = -(
          confidence * tf.math.log(confidence) +
          smoothing * tf.math.log(low_confidence + 1e-20))
  xentropy -= normalizing_constant

  mask = tf.cast(tf.not_equal(real, 0), tf.float32)
  xentropy *= mask
  loss = tf.reduce_sum(xentropy) / tf.reduce_sum(mask)

  return loss

def loss_function_1(smoothing, vocab_size):

  def loss_fn(y_true, y_pred):
    confidence = 1.0 - smoothing
    low_confidence = smoothing / tf.cast(vocab_size - 1, tf.float32)
    soft_targets = tf.one_hot(
        tf.cast(y_true, tf.int64),
        depth=vocab_size,
        on_value=confidence,
        off_value=low_confidence)
    xentropy = tf.nn.softmax_cross_entropy_with_logits(
        logits=y_pred, labels=soft_targets)

    normalizing_constant = -(
            confidence * tf.math.log(confidence) +
            smoothing * tf.math.log(low_confidence + 1e-20))
    xentropy -= normalizing_constant

    mask = tf.cast(tf.not_equal(y_true, 0), tf.float32)
    xentropy *= mask
    loss = tf.reduce_sum(xentropy) / tf.reduce_sum(mask)

    return loss

  return loss_fn

def loss_function_2(y_true, y_pred):
  xentropy = tf.keras.losses.sparse_categorical_crossentropy(
      y_true, y_pred, from_logits=True)

  mask = tf.cast(tf.not_equal(y_true, 0), tf.float32)
  xentropy *= mask
  loss = tf.reduce_sum(xentropy) / tf.reduce_sum(mask)

  return loss