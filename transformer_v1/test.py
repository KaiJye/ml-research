import importlib
import tensorflow as tf
import tensorflow_datasets as tfds
from common import embedding_layer, attention_layer, ffn_layer, dataset_utils
from common import model_utils
from common import beam_search
from transformer_v1 import data_pipeline, model_params
import re

params = model_params.BASE_PARAMS.copy()
params.update(
    data_dir=r"D:\data\wmt14_preprocessed_v2",
    vocab_path=r"D:/vocab/vocab_en_de_v2",
    cache_dir=r"D:\data\cache\training_cache",
    static_batch=True,
)

ds = data_pipeline.get_train_ds_2(params)

for ex in ds.enumerate():
  ex[0]