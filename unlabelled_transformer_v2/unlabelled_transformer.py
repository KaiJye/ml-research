import tensorflow as tf
from common import embedding_layer, attention_layer, ffn_layer, model_utils

def create_training_model(params):
  inp = tf.keras.layers.Input((None,), dtype=tf.int64)
  length = tf.keras.layers.Input([], dtype=tf.int64)

  embedding = Embedding(params["d_model"], params["vocab_size"], params["static_batch"], params["rped"])

  encoder = Encoder(params["num_layers"], params["d_model"],
      params["num_heads"], params["dff"], params["dropout_rate"])

  length_predictor = LengthPredictorLayer(params["d_model"],
      params["num_heads"], params["dropout_rate"])

  decoder_initializer = DecoderInitializerLayer(params["max_length"], params["d_model"],
      params["num_heads"], params["dff"], params["dropout_rate"], params["static_batch"], params["rped"])

  decoder = Decoder(params["num_layers"], params["d_model"],
      params["num_heads"], params["dff"], params["dropout_rate"])

  logits, attention_mask, inp_pos_encoding = embedding(inp, mode="embedding")

  enc_output = encoder([logits, attention_mask, inp_pos_encoding])

  enc_logits = embedding(enc_output[:, 1:, :], mode="linear")

  pred_length, pass_on_length = length_predictor([enc_output, attention_mask, length])

  dec_inputs, padding_mask, _, dec_pos_encoding = decoder_initializer([enc_output[:, 1:, :], pass_on_length, attention_mask[:, :, :, 1:]])

  dec_output = decoder(
        [dec_inputs, enc_output[:, 1:, :], padding_mask, attention_mask[:, :, :, 1:], dec_pos_encoding])

  logits = embedding(dec_output, mode="linear")

  enc_logits = tf.keras.layers.Lambda(lambda x: x, 
      name="enc_output",
      dtype=tf.float32)(enc_logits)

  logits = tf.keras.layers.Lambda(lambda x: x, 
      name="output",
      dtype=tf.float32)(logits)

  pred_length = tf.keras.layers.Lambda(lambda x: x, 
      name="length",
      dtype=tf.int64)(pred_length)

  model = tf.keras.Model([inp, length], [enc_logits, logits, pred_length])

  return model, embedding, encoder, length_predictor, decoder_initializer, decoder

def create_predict_model(params):
  inp = tf.keras.layers.Input((None,), dtype=tf.int64)
  enc_tar = tf.keras.layers.Input((None,), dtype=tf.int64)
  tar = tf.keras.layers.Input((None,), dtype=tf.int64)
  length = tf.keras.layers.Input([], dtype=tf.int64)

  embedding = Embedding(params["d_model"], params["vocab_size"], params["static_batch"], params["rped"])

  encoder = Encoder(params["num_layers"], params["d_model"],
      params["num_heads"], params["dff"], params["dropout_rate"])

  length_predictor = LengthPredictorLayer(params["d_model"],
      params["num_heads"], params["dropout_rate"])

  decoder_initializer = DecoderInitializerLayer(params["max_length"], params["d_model"],
      params["num_heads"], params["dff"], params["dropout_rate"], params["static_batch"], params["rped"])

  decoder = Decoder(params["num_layers"], params["d_model"],
      params["num_heads"], params["dff"], params["dropout_rate"])

  logits, attention_mask, inp_pos_encoding = embedding(inp, mode="embedding")

  enc_output = encoder([logits, attention_mask, inp_pos_encoding])

  enc_logits = embedding(enc_output[:, 1:, :], mode="linear")

  pred_length, pass_on_length = length_predictor([enc_output, attention_mask, length])

  logits, padding_mask, length_mask, dec_pos_encoding = decoder_initializer([enc_output[:, 1:, :], pass_on_length, attention_mask[:, :, :, 1:]])
  
  logits = decoder(
        [logits, enc_output[:, 1:, :], padding_mask, attention_mask[:, :, :, 1:], dec_pos_encoding])

  logits = embedding(logits, mode="linear")

  enc_logits = tf.keras.layers.Lambda(lambda x: tf.argmax(x, axis=-1),
      name="enc_output",
      dtype=tf.float32)(enc_logits)

  logits = tf.keras.layers.Lambda(lambda x: tf.argmax(x[0], axis=-1) * x[1],
      name="output",
      dtype=tf.float32)([logits, length_mask])

  model = tf.keras.Model([inp, enc_tar, tar, length], [inp, enc_tar, tar, enc_logits, logits, pred_length])

  return model

class Embedding(tf.keras.Model):
  
  def __init__(self, d_model, vocab_size, static_batch, rped):
    super(Embedding, self).__init__()
    self.d_model = d_model
    self.vocab_size = vocab_size
    self.static_batch = static_batch
    self.rped = rped

  def build(self, input_shape):
    self.embedding_softmax_layer = embedding_layer.EmbeddingSharedWeights(
        self.vocab_size, self.d_model)
    super(Embedding, self).build(input_shape)

  def call(self, x, mode="embedding", training=None):
    if mode == "embedding":
      length = tf.reduce_sum(tf.cast(tf.not_equal(x, 0), dtype=tf.int32), axis=-1)
      # -1 to offset at <POS> position
      pos_encoding = tf.gather(self.rped, length - 1)
      if not self.static_batch:
        pos_encoding = pos_encoding[:, :tf.reduce_max(length), :]
      # pad and shift to offset at <POS> position
      pos_encoding = tf.pad(pos_encoding, [[0, 0], [1, 0], [0, 0]])[:, :-1, :]

      attention_mask = model_utils.get_padding_bias(x)
      x = self.embedding_softmax_layer(x, mode=mode)
      return x, attention_mask, pos_encoding
    elif mode == "linear":
      x = self.embedding_softmax_layer(x, mode=mode)
      return x

class DecoderInitializerLayer(tf.keras.Model):
  def __init__(self, max_length, d_model, num_heads, dff, dropout_rate, static_batch, rped):
    super(DecoderInitializerLayer, self).__init__()
    self.max_length = max_length
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate
    self.static_batch = static_batch
    self.rped = rped

  def build(self, input_shape):
    self.position_attention = attention_layer.Attention(self.d_model, self.num_heads, self.dropout_rate)
    self.ffn = ffn_layer.FeedForwardNetwork(self.d_model, self.dff, self.dropout_rate)

    self.layernorm1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.layernorm2 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    
    self.dropout1 = tf.keras.layers.Dropout(self.dropout_rate)
    self.dropout2 = tf.keras.layers.Dropout(self.dropout_rate)
    super(DecoderInitializerLayer, self).build(input_shape)
  
  def call(self, inputs, training=None):
    encoder_output, length, attention_mask = inputs[0], tf.cast(inputs[1], dtype=tf.int32), inputs[2]

    if self.static_batch:
      length_mask = tf.sequence_mask(length, maxlen=self.max_length, dtype=tf.int64)
      pos_encoding = tf.gather(self.rped, length)
    else:
      length_mask = tf.sequence_mask(length, dtype=tf.int64)
      pos_encoding = tf.gather(self.rped, length)[:, :tf.reduce_max(length), :]

    padding_mask = model_utils.get_padding_bias(length_mask)

    # pos_encoding = model_utils.positional_encoding(tf.shape(length_mask)[1], self.d_model)

    # pos_encoding *= tf.expand_dims(length_mask, axis=-1)

    attn_output = self.position_attention(pos_encoding, encoder_output, 
        tf.minimum(tf.transpose(padding_mask, perm=[0, 1, 3, 2]), attention_mask), 
        training=training)
    attn_output = self.dropout1(attn_output, training=training)
    attn_output = self.layernorm1(pos_encoding + attn_output)
    
    ffn_output = self.ffn(attn_output, training=training)
    ffn_output = self.dropout2(ffn_output, training=training)
    ffn_output = self.layernorm2(attn_output + ffn_output)

    return ffn_output, padding_mask, length_mask, pos_encoding

class LengthPredictorLayer(tf.keras.Model):
  def __init__(self, d_model, num_heads, dropout_rate):
    super(LengthPredictorLayer, self).__init__()
    self.d_model = d_model
    self.num_heads = num_heads
    self.dropout_rate = dropout_rate

  def build(self, input_shape):
    self.self_attention = attention_layer.SelfAttention(self.d_model, self.num_heads, self.dropout_rate)
    self.attention_norm = tf.keras.layers.LayerNormalization(epsilon=1e-6)

    self.dense_0 = tf.keras.layers.Dense(self.d_model, activation=tf.nn.relu)
    self.dense_1 = tf.keras.layers.Dense(1, activation=None)
    self.relu = tf.keras.layers.ReLU()
    super(LengthPredictorLayer, self).build(input_shape)
  
  def call(self, inputs, training=None):
    if len(inputs) == 2:
      encoder_output, attention_mask, length = inputs[0], inputs[1], None
    else:
      encoder_output, attention_mask, length = inputs[0], inputs[1], inputs[2]

    attention_output = self.self_attention(encoder_output, attention_mask, training=training)
    if training:
      attention_output = tf.nn.dropout(attention_output, rate=self.dropout_rate)
    attention_output = self.attention_norm(encoder_output + attention_output)

    out = self.dense_0(attention_output[:, 0, :])
    if training:
      out = tf.nn.dropout(out, rate=self.dropout_rate)
    out = self.dense_1(out)
    out = self.relu(out)

    out = tf.reshape(out, [-1])

    if length is None:
      return out, out
    else: 
      return out, length

class EncoderLayer(tf.keras.layers.Layer):
  def __init__(self, d_model, num_heads, dff, dropout_rate):
    super(EncoderLayer, self).__init__()
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate

  def build(self, input_shape):
    self.self_attention = attention_layer.SelfAttention(self.d_model, self.num_heads, self.dropout_rate)
    self.ffn = ffn_layer.FeedForwardNetwork(self.d_model, self.dff, self.dropout_rate)

    self.layernorm1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.layernorm2 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    
    self.dropout1 = tf.keras.layers.Dropout(self.dropout_rate)
    self.dropout2 = tf.keras.layers.Dropout(self.dropout_rate)
    super(EncoderLayer, self).build(input_shape)
  
  def call(self, inputs, training=None):
    x, attention_mask = inputs[0], inputs[1]

    attn_output = self.self_attention(x, attention_mask, training=training)  # (batch_size, input_seq_len, d_model)
    attn_output = self.dropout1(attn_output, training=training)
    out1 = self.layernorm1(x + attn_output)  # (batch_size, input_seq_len, d_model)
    
    ffn_output = self.ffn(out1, training=training)  # (batch_size, input_seq_len, d_model)
    ffn_output = self.dropout2(ffn_output, training=training)
    out2 = self.layernorm2(out1 + ffn_output)  # (batch_size, input_seq_len, d_model)
    
    return out2

class DecoderLayer(tf.keras.layers.Layer):
  def __init__(self, d_model, num_heads, dff, dropout_rate):
    super(DecoderLayer, self).__init__()
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate
    
  def build(self, input_shape):
    self.self_attention = attention_layer.SelfAttention(self.d_model, self.num_heads, self.dropout_rate)
    self.attention = attention_layer.Attention(self.d_model, self.num_heads, self.dropout_rate)
    self.ffn = ffn_layer.FeedForwardNetwork(self.d_model, self.dff, self.dropout_rate)

    self.layernorm1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.layernorm2 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.layernorm3 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    
    self.dropout1 = tf.keras.layers.Dropout(self.dropout_rate)
    self.dropout2 = tf.keras.layers.Dropout(self.dropout_rate)
    self.dropout3 = tf.keras.layers.Dropout(self.dropout_rate)
    super(DecoderLayer, self).build(input_shape)

  def call(self, inputs, training):
    x, enc_output, padding_mask, attention_mask = inputs[0], inputs[1], inputs[2], inputs[3]
    
    attn1 = self.self_attention(x, padding_mask, training=training)  # (batch_size, target_seq_len, d_model)
    attn1 = self.dropout1(attn1, training=training)
    out1 = self.layernorm1(attn1 + x)
    
    attn2 = self.attention(out1, enc_output, 
        tf.minimum(tf.transpose(padding_mask, perm=[0, 1, 3, 2]), attention_mask), 
        training=training)  # (batch_size, target_seq_len, d_model)
    attn2 = self.dropout2(attn2, training=training)
    out2 = self.layernorm2(attn2 + out1)  # (batch_size, target_seq_len, d_model)
    
    ffn_output = self.ffn(out2, training=training)  # (batch_size, target_seq_len, d_model)
    ffn_output = self.dropout3(ffn_output, training=training)
    out3 = self.layernorm3(ffn_output + out2)  # (batch_size, target_seq_len, d_model)
    
    return out3

class Encoder(tf.keras.Model):
  def __init__(self, num_layers, d_model, num_heads, dff, dropout_rate):
    super(Encoder, self).__init__()
    self.num_layers = num_layers
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate
  
  def build(self, input_shape):
    self.enc_layers = [EncoderLayer(self.d_model, self.num_heads, self.dff, self.dropout_rate)
        for _ in range(self.num_layers)]
    self.dropout = tf.keras.layers.Dropout(self.dropout_rate)
    self.layernorm = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    super(Encoder, self).build(input_shape)

  def call(self, inputs, training=None):
    x, attention_mask, pos_encoding = inputs[0], inputs[1], inputs[2]
    
    # seq_len = tf.shape(x)[1]
    # pos_encoding = model_utils.positional_encoding(
    #     seq_len,
    #     self.d_model)
    x += pos_encoding

    x = self.dropout(x, training=training)
    x = self.layernorm(x)

    for layer in self.enc_layers:
      x = layer([x, attention_mask], training)
    
    return x  # (batch_size, input_seq_len, d_model)

class Decoder(tf.keras.Model):
  def __init__(self, num_layers, d_model, num_heads, dff, dropout_rate):
    super(Decoder, self).__init__()
    self.num_layers = num_layers
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate

  def build(self, input_shape):
    self.dec_layers = [DecoderLayer(self.d_model, self.num_heads, self.dff, self.dropout_rate)
        for _ in range(self.num_layers)]
    self.dropout = tf.keras.layers.Dropout(self.dropout_rate)
    self.layernorm = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    super(Decoder, self).build(input_shape)

  def call(self, inputs, training=None):
    x, enc_output, padding_mask, attention_mask, pos_encoding = inputs[0], inputs[1], inputs[2], inputs[3], inputs[4]

    # seq_len = tf.shape(x)[1]
    # pos_encoding = model_utils.positional_encoding(
    #     seq_len,
    #     self.d_model)
    x += pos_encoding
    
    x = self.dropout(x, training=training)
    x = self.layernorm(x)

    for layer in self.dec_layers:
      x = layer([x, enc_output, padding_mask, attention_mask], training)
    
    # x.shape == (batch_size, target_seq_len, d_model)
    return x

class Transformer(tf.keras.Model):
  def __init__(self, max_length, num_layers, d_model, num_heads, dff, dropout_rate):
    super(Transformer, self).__init__()
    self.max_length = max_length
    self.num_layers = num_layers
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate

  def build(self, input_shape):
    self.encoder = Encoder(self.num_layers, self.d_model, self.num_heads, self.dff, self.dropout_rate)

    self.decoder = Decoder(self.num_layers, self.d_model, self.num_heads, self.dff, self.dropout_rate)

    self.length_predictor = LengthPredictorLayer(self.d_model, self.num_heads, self.dropout_rate)

    self.decoder_initializer = DecoderInitializerLayer(self.max_length, self.d_model, self.num_heads, self.dff, self.dropout_rate)

    super(Transformer, self).build(input_shape)
    
  def call(self, inputs, training=None):
    if len(inputs) == 3:
      inp, length, attention_mask = inputs[0], inputs[1], inputs[2]
    else:
      inp, length, attention_mask = inputs[0], None, inputs[1]

    enc_output = self.encoder([inp, attention_mask], training=training)  # (batch_size, inp_seq_len, d_model)

    pred_length = self.length_predictor([enc_output, attention_mask])

    if length is None:
      dec_inputs, padding_mask, length_mask = self.decoder_initializer([enc_output[:, 1:, :], pred_length, attention_mask[:, :, :, 1:]])
    else:
      dec_inputs, padding_mask, _ = self.decoder_initializer([enc_output[:, 1:, :], length, attention_mask[:, :, :, 1:]])
        
    dec_output = self.decoder(
        [dec_inputs, enc_output[:, 1:, :], padding_mask, attention_mask[:, :, :, 1:]], training=training)
    
    if length is None:
      return dec_output, pred_length, length_mask
    else:
      return dec_output, pred_length
