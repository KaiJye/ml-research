import tensorflow as tf
from common import dataset_utils

def loss_function(y_true, y_pred):
  xentropy = tf.keras.losses.sparse_categorical_crossentropy(
      y_true, y_pred, from_logits=True)

  mask = tf.cast(tf.not_equal(y_true, 0), dtype=tf.float32)
  xentropy *= mask
  loss = tf.reduce_sum(xentropy) / tf.reduce_sum(mask)

  return loss

def accuracy_function(y_true, y_pred):
  accuracy = tf.keras.metrics.sparse_categorical_accuracy(y_true, y_pred)

  mask = tf.cast(tf.not_equal(y_true, 0), dtype=tf.float32)
  accuracy *= mask
  accuracy = tf.reduce_sum(accuracy) / tf.reduce_sum(mask)

  return accuracy

class MaskedAccuracy(tf.keras.metrics.SparseCategoricalAccuracy):

  def __init__(self):
    super(MaskedAccuracy, self).__init__()

  def update_state(self, y_true, y_pred, sample_weight=None):
    sample_weight = tf.cast(tf.not_equal(y_true, 0), dtype=tf.float32)
    super(MaskedAccuracy, self).update_state(y_true, y_pred, sample_weight)