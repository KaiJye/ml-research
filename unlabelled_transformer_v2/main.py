import tensorflow as tf
import tensorflow_datasets as tfds
import os, json
import numpy as np
from unlabelled_transformer_v2 import unlabelled_transformer as ut
from transformer_v1 import optimizer
from unlabelled_transformer_v2 import metrics
from unlabelled_transformer_v2 import data_pipeline
from common import evaluate, model_utils

class MainTask:
  def __init__(self, params, resolver=None, epoch_callback=None):
    self.params = params
    self.epoch_callback = epoch_callback

    if resolver:
      self.distribution_strategy = tf.distribute.experimental.TPUStrategy(resolver)
      self.params["static_batch"] = True
    else:
      self.distribution_strategy = tf.distribute.MirroredStrategy()
      self.params["static_batch"] = False

    self.params["rped"] = model_utils.relative_positional_encoding_dict(params)

  def train(self):
    params = self.params

    language = params["language"]
    model_dir = params["model_dir"]
    model_name = params["model_name"]

    model_path = os.path.join(model_dir, f"{model_name}_{language}_model.h5")
    train_state_path = os.path.join(model_dir, f"{model_name}_{language}_train_state.json")

    with self.distribution_strategy.scope():
      model, _, _, _, _, _ = ut.create_training_model(params)

      try:
        model.load_weights(model_path)
        print("model loaded")
      except:
        print("create new model")
      
      opt = optimizer.create_optimizer(params)
      current_step = 0
      
      try:
        with open(train_state_path) as json_file:
          train_state = json.load(json_file)
        opt.iterations = tf.Variable(train_state["steps"])
        current_step = opt.iterations.numpy()
      except:
        print("no train state found")

      print(f"current train step: {current_step}")

      loss_fn = metrics.loss_function
      accuracy_function = metrics.accuracy_function
      model.compile(optimizer=opt, 
          loss={ "enc_output": loss_fn, "output": loss_fn, "length": tf.keras.losses.MAE }, 
          metrics={ "enc_output": [accuracy_function], "output": [accuracy_function], "length": [] },
          loss_weights={ "enc_output": 1, "output": 1, "length": 1 })

    model.summary()

    train_ds = data_pipeline.get_train_ds(params)

    if params["with_val"]:
      validation_ds = data_pipeline.get_validation_ds(params).take(16)
    else:
      validation_ds = None

    callbacks = self._create_callbacks(model_path, train_state_path)

    history = model.fit(train_ds,
        validation_data=validation_ds,
        epochs=params['epochs'],
        steps_per_epoch=params['steps_per_epoch'],
        callbacks=callbacks,
        verbose=params['verbose'])

    print(history.history)

  def eval(self):
    params = self.params

    language = params["language"]
    model_dir = params["model_dir"]
    model_name = params["model_name"]

    model_path = os.path.join(model_dir, f"{model_name}_{language}_model.h5")

    with self.distribution_strategy.scope():
      model = ut.create_predict_model(params)
      
      model.load_weights(model_path)
      print("model loaded")

    model.summary()

    test_ds = data_pipeline.get_test_ds(params)

    if params['single_batch_test']:
      test_ds = test_ds.take(1)

    test_ds = test_ds.map(lambda x, y: ((x[0], y[0], y[1], x[1]),))

    inp, enc_tar, tar, enc_logits, logits, pred_length = model.predict(test_ds)

    inp = evaluate.decode(params, tf.constant(inp))
    tar = evaluate.decode(params, tf.constant(tar))
    enc_tar = evaluate.decode(params, tf.constant(enc_tar))
    enc_logits = evaluate.decode(params, tf.constant(enc_logits))
    logits = evaluate.decode(params, tf.constant(logits))

    print("inp")
    print(inp)
    print("tar")
    print(tar)
    print("enc_tar")
    print(enc_tar)
    print("enc_logits")
    print(enc_logits)
    print("logits")
    print(logits)
    print("pred_length")
    print(pred_length)

  def _create_callbacks(self, model_path, train_state_path):
    callbacks = []
    learning_rate_callback = optimizer.LearningRateCallback(self.params)
    callbacks.append(learning_rate_callback)
    ckpt_callback = CheckpointCallback(model_path, train_state_path, self.epoch_callback)
    callbacks.append(ckpt_callback)
    return callbacks

class CheckpointCallback(tf.keras.callbacks.Callback):

  def __init__(self, model_path, train_state_path, epoch_callback):
    super(CheckpointCallback, self).__init__()

    self.model_path = model_path
    self.train_state_path = train_state_path
    self.epoch_callback = epoch_callback

  def on_epoch_end(self, epoch, logs=None):
    self.model.save_weights(self.model_path, save_format='h5')

    train_state = logs
    with open(self.train_state_path, 'w') as json_file:
      json.dump(train_state, json_file, default=default)
    
    if self.epoch_callback:
      self.epoch_callback()

def default(obj):
  if isinstance(obj, np.integer):
    return int(obj)
  elif isinstance(obj, np.floating):
    return float(obj)
  elif isinstance(obj, np.ndarray):
    return obj.tolist()
