import tensorflow as tf
import tensorflow_datasets as tfds
import matplotlib.pyplot as plt
from unlabelled_transformer_v2 import unlabelled_transformer as ut
from unlabelled_transformer_v2 import data_pipeline, metrics, main
from common import model_utils
from transformer_v1 import model_params, optimizer

# a = tf.constant([2,5,2,256], dtype=tf.float32)
# print(a[:, tf.newaxis, tf.newaxis])
# b = tf.constant([2,2,2,3])
# p = tf.map_fn(lambda x: model_utils.relative_positional_encoding(tf.cast(x[0, 0], dtype=tf.int32), 512, 256), a[:, tf.newaxis, tf.newaxis])
# p = model_utils.positional_encoding(a, 512)
# b = tf.sequence_mask(a, dtype=tf.float32)
# print(model_utils.get_padding_bias(b))
# b = tf.expand_dims(b, axis=-1)

# length_mask = tf.sequence_mask(a, dtype=tf.float32)
# padding_mask = model_utils.get_padding_bias(length_mask)
# length_mask_b = tf.sequence_mask(b, dtype=tf.float32)
# padding_mask_b = model_utils.get_padding_bias(length_mask_b)
# padding_mask_b = tf.transpose(padding_mask_b, perm=[0, 1, 3, 2])
# print(padding_mask)
# print(padding_mask_b)
# print(tf.minimum(padding_mask, padding_mask_b))

# plt.pcolormesh(p[3, :, :], cmap='RdBu')
# plt.xlabel('Depth')
# plt.xlim((0, p.shape[2]))
# plt.ylabel('Position')
# plt.colorbar()
# plt.show()

language = "en"
params = model_params.BASE_PARAMS.copy()
params.update(
    language=language,
    model_name="ut_v2",
    model_dir=r"D:\models",
    data_dir=rf"D:\data\wiki_{language}_preprocessed_v2",
    vocab_path=r"D:\vocab\vocab_en_de_v3",
    with_val=False,
    single_batch_test=True,
    mask_percentage=0.25,
    drop_percentage=0.5,
    epochs=1,
    steps_per_epoch=2,
    verbose=1,
    test_batch_size=64,
    vocab_size=32517,
    batch_size=2,
    max_length=256,
    learning_rate_modifier=1.0,
    warmup_steps=4000,
    optimizer_adam_beta1=0.9,
    optimizer_adam_beta2=0.98,
    optimizer_adam_epsilon=1e-09,
)

task = main.MainTask(params, resolver=None, epoch_callback=None)
# task.train()
task.eval()

# ds = data_pipeline.get_train_ds(params).take(1)
# for ex in ds:
#   print(ex)
