import tensorflow as tf
from common import embedding_layer, attention_layer, ffn_layer
from common import dataset_utils, model_utils
from transformer_v1 import metrics

SOS_ID = dataset_utils.SOS_ID
EOS_ID = dataset_utils.EOS_ID

def create_model(params):
  inputs = tf.keras.layers.Input((None,), dtype=tf.int64)
  targets = tf.keras.layers.Input((None,), dtype=tf.int64)
  internal_model = Transformer(params)
  logits = internal_model([inputs, targets])
  model = tf.keras.Model([inputs, targets], logits)
  return model

class EncoderLayer(tf.keras.layers.Layer):
  def __init__(self, params):
    super(EncoderLayer, self).__init__()
    self.params = params

  def build(self, input_shape):
    self.self_attention = attention_layer.SelfAttention(self.params['d_model'], self.params['num_heads'], self.params['dropout_rate'])
    self.ffn = ffn_layer.FeedForwardNetwork(self.params['d_model'], self.params['dff'], self.params['dropout_rate'])

    self.layernorm1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.layernorm2 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    
    self.dropout1 = tf.keras.layers.Dropout(self.params['dropout_rate'])
    self.dropout2 = tf.keras.layers.Dropout(self.params['dropout_rate'])
    super(EncoderLayer, self).build(input_shape)
  
  def call(self, x, attention_mask, training):
    attn_output = self.self_attention(x, attention_mask, training=training)  # (batch_size, input_seq_len, d_model)
    attn_output = self.dropout1(attn_output, training=training)
    out1 = self.layernorm1(x + attn_output)  # (batch_size, input_seq_len, d_model)
    
    ffn_output = self.ffn(out1, training=training)  # (batch_size, input_seq_len, d_model)
    ffn_output = self.dropout2(ffn_output, training=training)
    out2 = self.layernorm2(out1 + ffn_output)  # (batch_size, input_seq_len, d_model)
    
    return out2

class DecoderLayer(tf.keras.layers.Layer):
  def __init__(self, params):
    super(DecoderLayer, self).__init__()
    self.params = params
    
  def build(self, input_shape):
    self.self_attention = attention_layer.SelfAttention(self.params['d_model'], self.params['num_heads'], self.params['dropout_rate'])
    self.attention = attention_layer.Attention(self.params['d_model'], self.params['num_heads'], self.params['dropout_rate'])
    self.ffn = ffn_layer.FeedForwardNetwork(self.params['d_model'], self.params['dff'], self.params['dropout_rate'])

    self.layernorm1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.layernorm2 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.layernorm3 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    
    self.dropout1 = tf.keras.layers.Dropout(self.params['dropout_rate'])
    self.dropout2 = tf.keras.layers.Dropout(self.params['dropout_rate'])
    self.dropout3 = tf.keras.layers.Dropout(self.params['dropout_rate'])
    super(DecoderLayer, self).build(input_shape)

  def call(self, x, enc_output, look_ahead_mask, attention_mask, training):
    # enc_output.shape == (batch_size, input_seq_len, d_model)
    attn1 = self.self_attention(x, look_ahead_mask, training=training)  # (batch_size, target_seq_len, d_model)
    attn1 = self.dropout1(attn1, training=training)
    out1 = self.layernorm1(attn1 + x)
    
    attn2 = self.attention(out1, enc_output, attention_mask, training=training)  # (batch_size, target_seq_len, d_model)
    attn2 = self.dropout2(attn2, training=training)
    out2 = self.layernorm2(attn2 + out1)  # (batch_size, target_seq_len, d_model)
    
    ffn_output = self.ffn(out2, training=training)  # (batch_size, target_seq_len, d_model)
    ffn_output = self.dropout3(ffn_output, training=training)
    out3 = self.layernorm3(ffn_output + out2)  # (batch_size, target_seq_len, d_model)
    
    return out3

class Encoder(tf.keras.layers.Layer):
  def __init__(self, params):
    super(Encoder, self).__init__()
    self.params = params
  
  def build(self, input_shape):
    self.enc_layers = [EncoderLayer(self.params) for _ in range(self.params['num_layers'])]
    self.dropout = tf.keras.layers.Dropout(self.params['dropout_rate'])
    self.layernorm = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    super(Encoder, self).build(input_shape)

  def call(self, x, attention_mask, training):
    seq_len = tf.shape(x)[1]
    
    pos_encoding = model_utils.get_position_encoding(
        seq_len,
        self.params['d_model'])
    x += pos_encoding

    x = self.dropout(x, training=training)
    x = self.layernorm(x)

    for i in range(self.params['num_layers']):
      x = self.enc_layers[i](x, attention_mask, training)
    
    return x  # (batch_size, input_seq_len, d_model)

class Decoder(tf.keras.layers.Layer):
  def __init__(self, params):
    super(Decoder, self).__init__()
    self.params = params

  def build(self, input_shape):
    self.dec_layers = [DecoderLayer(self.params) for _ in range(self.params['num_layers'])]
    self.dropout = tf.keras.layers.Dropout(self.params['dropout_rate'])
    self.layernorm = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    super(Decoder, self).build(input_shape)

  def call(self, x, enc_output, padding_mask, attention_mask, training):
    seq_len = tf.shape(x)[1]

    look_ahead_mask = model_utils.get_decoder_self_attention_bias(seq_len)
    self_attention_mask = tf.minimum(padding_mask, look_ahead_mask)

    pos_encoding = model_utils.get_position_encoding(
        seq_len,
        self.params['d_model'])
    x += pos_encoding
    
    x = self.dropout(x, training=training)
    x = self.layernorm(x)

    for i in range(self.params['num_layers']):
      x = self.dec_layers[i](x, enc_output, self_attention_mask, attention_mask, training)
    
    # x.shape == (batch_size, target_seq_len, d_model)
    return x

class Transformer(tf.keras.Model):
  def __init__(self, params):
    super(Transformer, self).__init__()
    self.params = params

  def build(self, input_shape):
    self.embedding_softmax_layer = embedding_layer.EmbeddingSharedWeights(
        self.params["vocab_size"], self.params["d_model"])

    self.encoder = Encoder(self.params)

    self.decoder = Decoder(self.params)

    super(Transformer, self).build(input_shape)
    
  def call(self, inputs, training=None):
    inp, tar = inputs[0], inputs[1]

    attention_mask = model_utils.get_padding_bias(inp)

    inp = self.embedding_softmax_layer(inp)  # (batch_size, target_seq_len, d_model)

    enc_output = self.encoder(inp, attention_mask, training=training)  # (batch_size, inp_seq_len, d_model)
    
    return self.decode(tar, enc_output, attention_mask, training=training)

  def decode(self, tar, enc_output, attention_mask, training):
    padding_mask = model_utils.get_padding_bias(tar)
    decoder_inputs = self.embedding_softmax_layer(tar)  # (batch_size, length, d_model)
    
    dec_output = self.decoder(
        decoder_inputs, enc_output, padding_mask, attention_mask, training=training)
    
    output = self.embedding_softmax_layer(dec_output, mode="linear")
    return output
