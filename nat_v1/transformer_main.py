import tensorflow as tf
import tensorflow_datasets as tfds
import os, json
from nat_v1 import transformer
from transformer_v1 import optimizer
from nat_v1 import data_pipeline
from transformer_v1 import evaluate
from transformer_v1 import metrics

class TransformerTask:
  def __init__(self, params, resolver=None, epoch_callback=None):
    self.params = params
    self.epoch_callback = epoch_callback

    if resolver:
      self.distribution_strategy = tf.distribute.experimental.TPUStrategy(resolver)
      self.params['static_batch'] = True
    else:
      self.distribution_strategy = tf.distribute.MirroredStrategy()
      self.params['static_batch'] = False

  def train(self):
    params = self.params

    with self.distribution_strategy.scope():
      model = transformer.create_model(params)
      opt = optimizer.create_optimizer(params)
      current_step = 0

      try:
        model.load_weights(params["model_path"])
        print("model loaded")
      except:
        print("create new model")

      try:
        with open(params["train_state_path"]) as json_file:
          train_state = json.load(json_file)
          opt.iterations = tf.Variable(train_state["steps"])
          current_step = opt.iterations.numpy()
      except:
        pass

      print(f"current train step: {current_step}")

      loss_fn = metrics.loss_function_1(params["label_smoothing"], params["vocab_size"])
      model.compile(optimizer=opt, loss=loss_fn)

    model.summary()

    train_ds = data_pipeline.get_train_ds_2(params)

    callbacks = self._create_callbacks()

    current_epoch = 0

    while current_epoch < params['epochs']:
      model.fit(train_ds,
          initial_epoch=current_epoch,
          epochs=current_epoch + 1,
          steps_per_epoch=params['steps_per_epoch'],
          callbacks=callbacks,
          verbose=params['verbose'])

      model.save_weights(params["model_path"], save_format='h5')
      train_state = {}
      train_state["steps"] = int(model.optimizer.iterations.numpy())
      with open(params["train_state_path"], 'w') as json_file:
        json.dump(train_state, json_file)

      if self.epoch_callback:
        self.epoch_callback()

      current_epoch += 1

  def eval(self):
    params = self.params

    with self.distribution_strategy.scope():
      model = transformer.create_model(params)

      try:
        model.load_weights(params["model_path"])
        print("model loaded")
      except:
        print("create new model")

      # model.compile()

    model.summary()

    ds = data_pipeline.get_test_ds_2(params)

    if params['single_batch_test']:
      ds = ds.take(1)

    # predictions = model.predict(ds)

    # print(predictions)

    for inp, tar in ds:
      ids = model(inp, training = False)
      ids = tf.argmax(ids, axis=-1, output_type=tf.int64)
      # print()
      translated_batch = evaluate.decode(params, ids)
      print(translated_batch)

    # print("computing bleu")
    # bleu_score = evaluate.compute_bleu(reference, translated)

    # return bleu_score

  def _create_callbacks(self):
    callbacks = []
    learning_rate_callback = optimizer.LearningRateCallback(self.params)
    callbacks.append(learning_rate_callback)

    # model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
    #     filepath=self.params["model_path"],
    #     save_weights_only=True)
    # callbacks.append(model_checkpoint_callback)
    return callbacks

class CheckpointCallback(tf.keras.callbacks.Callback):

  def __init__(self, ckpt_manager):
    super(CheckpointCallback, self).__init__()

    self.ckpt_manager = ckpt_manager

  def on_train_batch_end(self, batch, logs=None):
    if self.model.optimizer.iterations.numpy() < 50000:
      return
    if self.model.optimizer.iterations.numpy() % 10000 == 0:
      self.ckpt_manager.save()
      print("checkpoint saved")

