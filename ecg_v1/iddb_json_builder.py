import wfdb
from wfdb import processing
import json
import os
from os import path
from matplotlib import pyplot as plt
import numpy as np
import tensorflow as tf
from random import sample

"""
Person_74 has 20 split only
"""

append = False
filtered = False
fs = 128
data_dir = r'D:\data\ecg\ecg-id-database-1.0.0'
json_path = r'D:\data\id_label_test.json'

if append:
  with open(json_path) as json_file:
    id_labels = json.load(json_file)
else:
  id_labels = {}

with open(path.join(data_dir, 'RECORDS')) as record_list:
  for record_entry in record_list:
    record_entry = record_entry.strip()
    record_entry_splits = record_entry.split('/')

    person = record_entry_splits[0]
    record_number = record_entry_splits[1]

    if person not in id_labels:
      id_labels[person] = []
    signals = id_labels[person]

    record = wfdb.rdrecord(path.join(data_dir, record_entry))
    if filter:
      signal = record.p_signal[:, 1]
    else:
      signal = record.p_signal[:, 0]
    
    signal, _ = processing.resample_sig(signal, record.fs, fs)

    ds = tf.data.Dataset.from_tensor_slices(signal)
    ds = ds.batch(int(2 * fs), drop_remainder=True)
    for s in ds.as_numpy_iterator():
      # normalize
      s = processing.normalize_bound(s, lb=-1, ub=1)
      
      # standardize
      # m = np.mean(s)
      # d = np.std(s)
      # s = (s - m) / d
      signals.append(s.tolist())

p_74 = id_labels.pop('Person_74')
print(len(id_labels))
sampled_keys = sample(id_labels.keys(), 79)
print(sampled_keys)
train = {}
test = {
  'Person_74': p_74
}
for k,v in id_labels.items():
  if len(v) <= 40:
    samples = v
  else:
    samples = sample(v, 40)
  if k in sampled_keys:
    test[k] = samples
  else:
    train[k] = samples

# with open(r'D:\data\id_label_train.json', 'w') as json_file:
#   json.dump(train, json_file)

with open(r'D:\data\id_label_test.json', 'w') as json_file:
  json.dump(test, json_file)

