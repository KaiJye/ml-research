import tensorflow as tf
from ecg import biometric_model, data_pipeline

class MainTask:
  def __init__(self, params, resolver=None):
    self.params = params

    if resolver:
      self.distribution_strategy = tf.distribute.experimental.TPUStrategy(resolver)
      self.params['static_batch'] = True
    else:
      self.distribution_strategy = tf.distribute.MirroredStrategy()
      self.params['static_batch'] = False

  def train(self):
    params = self.params

    with self.distribution_strategy.scope():
      model = biometric_model.create_training_model(params)
      opt = tf.keras.optimizers.Adam(params['learning_rate'],
          beta_1=params['optimizer_adam_beta1'],
          beta_2=params['optimizer_adam_beta2'], 
          epsilon=params['optimizer_adam_epsilon'])

      checkpoint_path = params['model_dir']

      ckpt = tf.train.Checkpoint(model=model, optimizer=opt)

      ckpt_manager = tf.train.CheckpointManager(ckpt, checkpoint_path, max_to_keep=params['max_to_keep'])

      current_step = 0
      if ckpt_manager.latest_checkpoint:
        ckpt.restore(ckpt_manager.latest_checkpoint)
        print ('Latest checkpoint restored!!')
        current_step = opt.iterations.numpy()
      
      print('current train step', current_step)
      model.compile(optimizer=opt, loss='sparse_categorical_crossentropy', metrics=['sparse_categorical_accuracy'])

    model.summary()

    train_ds = data_pipeline.get_ds(params, 'train')
    val_ds = data_pipeline.get_ds(params, 'val')

    current_epoch = 0
    while current_epoch < params['epochs']:
      model.fit(train_ds,
          validation_data=val_ds,
          initial_epoch=current_epoch,
          epochs=current_epoch + 1,
          steps_per_epoch=params['steps_per_epoch'],
          verbose=params['verbose'])

      # ckpt_manager.save()

      current_epoch += 1

