import json
import wfdb
import os
import os.path as path

label_path = r'D:\data\ecg_labels.json'
data_dir = r'D:\data\ecg'

def gen():
  for i, dir_name in enumerate(next(os.walk(data_dir))[1]):
    if i != 6:
      continue
    print(i, dir_name)
    db = path.join(data_dir, dir_name)
    f = open(path.join(db, 'RECORDS'))
    for r in f:
      while True:
        dat_loc = path.join(db, r.strip())
        try:
          record = wfdb.rdrecord(dat_loc)
        except:
          continue
        print(f'fs: {record.fs}, sig_name: {record.sig_name}, comment: {record.comments}, dir: {dat_loc}')

        # sex
        while True:
          sex = input('sex: ')
          if sex == '' or sex == '1' or sex == '2':
            break
        if sex == '':
          sex = 0
        else:
          sex = int(sex)

        # age
        age = input('age: ')
        if age == '':
          age = -1
        else:
          age = int(age)

        # comments
        comments_in = input('comments: ')
        comments = []
        if comments_in != '':
          comments.extend([int(s) for s in comments_in.split(',')])

        with open(label_path) as json_file:
          ecg_labels = json.load(json_file)
          if dir_name not in ecg_labels:
            ecg_labels[dir_name] = {}
          branch = ecg_labels[dir_name]
          if r.strip() not in branch:
            branch[r.strip()] = {}
          record = branch[r.strip()]
          record['sex'] = sex
          record['age'] = age
          record['comments'] = comments
          json_file.close

        print(record)
        if input('confirm?') == 'y':
          break
      
      with open(label_path, 'w') as json_file:
        json.dump(ecg_labels, json_file)

    f.close()

count = 0
for _ in gen():
  count += 1
  print(count)