import wfdb
from wfdb import processing
import json
import os
from os import path
from matplotlib import pyplot as plt
import numpy as np
import tensorflow as tf
from random import sample, randint


append = False
fs = 128
sample_size = 20
data_source = [
  {
    'dir': r'D:\data\ecg\apnea-ecg-database-1.0.0',
    'prefix': 'apn',
    'channel': 0,
    'train': 50,
    'val': 5,
    'test': 15,
  },
  {
    'dir': r'D:\data\ecg\long-term-af-database-1.0.0',
    'prefix': 'laf',
    'channel': 0,
    'train': 60,
    'val': 5,
    'test': 19,
  },
  {
    'dir': r'D:\data\ecg\combined-measurement-of-ecg-breathing-and-seismocardiograms-1.0.0',
    'prefix': 'com',
    'channel': 0,
    'train': 40,
    'val': 5,
    'test': 15,
  },
  {
    'dir': r'D:\data\ecg\mit-bih-arrhythmia-database-1.0.0',
    'prefix': 'arr',
    'channel': 0,
    'train': 30,
    'val': 5,
    'test': 12,
  },
  {
    'dir': r'D:\data\ecg\mit-bih-atrial-fibrillation-database-1.0.0',
    'prefix': 'atr',
    'channel': 0,
    'train': 15,
    'val': 0,
    'test': 8,
  },
  {
    'dir': r'D:\data\ecg\mit-bih-long-term-ecg-database-1.0.0',
    'prefix': 'lt',
    'channel': 0,
    'train': 5,
    'val': 0,
    'test': 2,
  },
  {
    'dir': r'D:\data\ecg\mit-bih-malignant-ventricular-ectopy-database-1.0.0',
    'prefix': 'mal',
    'channel': 0,
    'train': 15,
    'val': 0,
    'test': 7,
  },
  {
    'dir': r'D:\data\ecg\mit-bih-normal-sinus-rhythm-database-1.0.0',
    'prefix': 'sin',
    'channel': 0,
    'train': 10,
    'val': 0,
    'test': 8,
  },
  {
    'dir': r'D:\data\ecg\mit-bih-polysomnographic-database-1.0.0',
    'prefix': 'som',
    'channel': 0,
    'train': 10,
    'val': 0,
    'test': 8,
  },
  {
    'dir': r'D:\data\ecg\mit-bih-supraventricular-arrhythmia-database-1.0.0',
    'prefix': 'sup',
    'channel': 0,
    'train': 50,
    'val': 7,
    'test': 21,
  },
  {
    'dir': r'D:\data\ecg\st-petersburg-incart-12-lead-arrhythmia-database-1.0.0',
    'prefix': 'stp',
    'channel': 0,
    'train': 50,
    'val': 7,
    'test': 18,
  },
  {
    'dir': r'D:\data\ecg\fantasia-database-1.0.0',
    'prefix': 'fan',
    'channel': 1,
    'train': 20,
    'val': 5,
    'test': 15,
  },
  {
    'dir': r'D:\data\ecg\ptb-diagnostic-ecg-database-1.0.0',
    'prefix': 'ptb',
    'channel': 0,
    'train': 400,
    'val': 30,
    'test': 70,
  },
  # {
  #   'dir': r'D:\data\ecg\ptb-xl-a-large-publicly-available-electrocardiography-dataset-1.0.1',
  #   'prefix': 'ptbx',
  #   'channel': 0,
  #   'train': 200,
  #   'val': 5,
  #   'test': 10,
  # },
]
train_json_path = r'D:\data\id_label_train.json'
val_json_path = r'D:\data\id_label_val.json'
test_json_path = r'D:\data\id_label_test.json'

def build_json():
  if append:
    with open(train_json_path) as json_file:
      id_labels_train = json.load(json_file)
    with open(val_json_path) as json_file:
      id_labels_val = json.load(json_file)
    with open(test_json_path) as json_file:
      id_labels_test = json.load(json_file)
  else:
    id_labels_train = {}
    id_labels_val = {}
    id_labels_test = {}

  for data_source_el in data_source:
    data_dir = data_source_el['dir']
    key_prefix = data_source_el['prefix']
    channel = data_source_el['channel']
    train_sample_size = data_source_el['train']
    val_sample_size = data_source_el['val']
    test_sample_size = data_source_el['test']

    with open(path.join(data_dir, 'RECORDS')) as record_file:
      records = set(record_file)

    print(f'{key_prefix}: {len(records)}')

    train_samples = sample(records, train_sample_size)
    not_train = records.difference(train_samples)
    val_samples = sample(not_train, val_sample_size)
    test_samples = sample(not_train.difference(val_samples), test_sample_size)

    # print(len(train_samples), len(val_samples), len(test_samples))
    # assert len(set(train_samples).union(val_samples, test_samples)) == len(train_samples) + len(val_samples) + len(test_samples)

    for record_entry in records:
      if record_entry in train_samples:
        id_labels = id_labels_train
      elif record_entry in val_samples:
        id_labels = id_labels_val
      elif record_entry in test_samples:
        id_labels = id_labels_test
      else:
        continue

      record_entry = record_entry.strip()

      key = f'{key_prefix}-{record_entry}'
      if key not in id_labels:
        id_labels[key] = []
      signals = id_labels[key]

      record = wfdb.rdrecord(path.join(data_dir, record_entry))

      signal = record.p_signal[:, channel]
      
      # print(record_entry)
      # print(record.__dict__)
      # # wfdb.plot_wfdb(record=record)
      # wfdb.plot_items(signal) 
      # input()

      signal = tf.gather_nd(signal, tf.where(tf.math.is_finite(signal)))

      if not tf.reduce_all(tf.math.is_finite(signal)):
        print(f'not finite: {key_prefix} - {record_entry}')
        print(tf.where(tf.math.is_finite(signal)))
        wfdb.plot_items(signal) 
      
      signal, _ = processing.resample_sig(signal.numpy(), record.fs, fs)
      
      def signal_gen(lower, upper=None):
        index = 0
        total_len = len(signal)
        while True:
          if upper is None:
            size = lower
          else:
            size = randint(lower, upper)
          end = index + size
          if end >= total_len:
            break
          yield signal[index:end] 
          index = end
      
      batched_signals = list(signal_gen(fs * 2))
      if len(batched_signals) >= sample_size:
        batched_signals = sample(batched_signals, sample_size)

      for s in batched_signals:
        # normalize
        s = processing.normalize_bound(s, lb=-1, ub=1)
        
        # standardize
        # m = np.mean(s)
        # d = np.std(s)
        # s = (s - m) / d
        # wfdb.plot_items(s) 
        signals.append(s.tolist())

  print(f'total individuals: {len(id_labels)}')

  with open(train_json_path, 'w') as json_file:
    json.dump(id_labels_train, json_file)
  with open(val_json_path, 'w') as json_file:
    json.dump(id_labels_val, json_file)
  with open(test_json_path, 'w') as json_file:
    json.dump(id_labels_test, json_file)

build_json()