# import wfdb
# from wfdb import processing
# import tensorflow as tf
from ecg import data_pipeline, model_params, main

# record = wfdb.rdrecord(r'E:\data\ecg\ecg-id-database-1.0.0\Person_07/rec_1')
# print(record.__dict__)
# annotation = wfdb.rdann(r'E:\data\ecg\ecg-id-database-1.0.0\Person_07/rec_1', 'atr')
# wfdb.plot_wfdb(record=record, annotation=annotation)

# resampled_x, resampled_t = processing.resample_sig(record.p_signal[:, 1], record.fs, 256)
# plt.plot(resampled_x)
# plt.show()

# plot_inputs = record.p_signal[:, 0]
# inputs = plot_inputs[tf.newaxis, tf.newaxis, :, tf.newaxis]
# # print(inputs)

# outputs = tf.nn.avg_pool2d(
#   inputs, [1, 1, 4, 1], [1, 1, 4, 1], 'VALID', data_format='NHWC'
# )

# outputs = tf.squeeze(outputs)

# fig, (ax1, ax2) = plt.subplots(2)
# ax1.plot(plot_inputs)
# ax2.plot(outputs)
# plt.show()

# with open(r'E:\data\id_label.json') as json_file:
#   id_labels = json.load(json_file)

# test = id_labels['Person_85'][2]
# plt.plot(test)
# plt.show()

params = model_params.BASE_PARAMS.copy()
params.update(
    model_dir=r"D:\models",
    data_dir=r"D:\data\ecg_p0_v0",
    single_batch_test=True,
    epochs=10,
    steps_per_epoch=None,
    d_model=32,
    bio_dict_size=128,
    num_heads=4,
    num_encoder_layers=4,
    num_conv_layers=4,
    kernel_size=4,
    dff=128,
    verbose=1,
    batch_size=32,
    learning_rate=1e-5,
    optimizer_adam_beta1=0.9,
    optimizer_adam_beta2=0.98,
    optimizer_adam_epsilon=1e-09,
)

# task = main.MainTask(params, resolver=None)
# task.train()
# task.eval()

ds = data_pipeline.get_ds(params, 'train')
for i, ex in ds.enumerate():
  print(i.numpy(), ex[1])
