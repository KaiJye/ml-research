import os
from os import path
import tensorflow as tf
import wfdb
from common import dataset_utils
import itertools
import json
from random import randrange
from random import sample

# total 27629

SHARDS = 10
json_path = r'D:\data\id_label.json'

def serialize(label, dat):
  feature = {
      'label': dataset_utils.int64_feature([int(label.numpy())]),
      'dat': dataset_utils.float_feature(dat.numpy()),
  }
  example_proto = tf.train.Example(features=tf.train.Features(feature=feature))
  return example_proto.SerializeToString()

def tf_serialize(label, dat):
  tf_string = tf.py_function(serialize, (label, dat), tf.string)
  return tf.reshape(tf_string, ())

def gen():
  with open(json_path) as json_file:
    id_labels = json.load(json_file)

  least = 40
  # for signals in id_labels.values():
  #   signals_size = len(signals)
  #   if (signals_size < least):
  #     least = signals_size
  # print(least)
  
  index = 1
  for i, person in enumerate(id_labels.values()):
    if len(person) < least:
      continue
    samples = sample(list(person), least)
    # print(index)
    for s in samples:
      yield index, s
    index += 1


ds = tf.data.Dataset.from_generator(
    gen, 
    (tf.int32, tf.float32), 
    (tf.TensorShape([]), tf.TensorShape([None])),
)

ds = ds.shuffle(30000)

prefix = 'test'
index = 0
for i in range(SHARDS):
  shard_ds = ds.shard(SHARDS, i)

  shard_ds = shard_ds.shuffle(3000)

  shard_ds = shard_ds.map(tf_serialize)

  if i == 1:
    prefix = 'val'
    index = 0
  elif i == 2:
    prefix = 'train'
    index = 0

  writer = tf.data.experimental.TFRecordWriter(f"D:/data/ecg_p0_v0/{prefix}-{index}.tfrecord")
  writer.write(shard_ds)

  index += 1
