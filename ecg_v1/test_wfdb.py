import wfdb
import os
import os.path as path

record = wfdb.rdrecord(r'E:\data\mit-bih-st-change-database-1.0.0\300', sampto=1000, m2s=True)
print(record.__dict__)
wfdb.plot_wfdb(record=record, title='test title') 

# ann = wfdb.rdann(r'E:\data\mitdb\213', 'atr', sampto=1000)
# print(ann.__dict__)
# wfdb.plot_wfdb(record=record, annotation=ann)

data_dir = r'E:\data'

max_loc = ''
min_loc = ''
max_fs = 0
min_fs = 100000

for dir in next(os.walk(data_dir))[1]:
  print(dir)
  db = path.join(data_dir, dir)
  f = open(path.join(db, 'RECORDS'))
  for r in f:
    dat_loc = path.join(db, r.strip())
    hea = wfdb.rdheader(dat_loc)
    if hea.fs > max_fs:
      max_fs = hea.fs
      max_loc = dat_loc
    if hea.fs < min_fs:
      min_fs = hea.fs
      min_loc = dat_loc
    
    print(f'{hea.comments}')
    # print(f'{hea.fs} ; {hea.sig_name} ; {hea.comments} {dat_loc}')
    # print(hea.__dict__)
    # break
  f.close()

print(f'max: {max_fs} ; {max_loc}')
print(f'min: {min_fs} ; {min_loc}')


# label age, sex, problem
# down sampling to 100 using pooling
# split multi channel signal (some multi are just filtered of the real)
# generate biometric classification data using both same channel and multi channel