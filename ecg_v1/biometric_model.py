import tensorflow as tf
from common import attention_layer, ffn_layer, model_utils

def create_training_model(params):
  dat = tf.keras.layers.Input((None,), dtype=tf.float32)
  internal_model = CustomModel(params)
  logits = internal_model(dat)
  model = tf.keras.Model(dat, logits)
  return model


class CustomModel(tf.keras.Model):

  def __init__(self, params):
    super(CustomModel, self).__init__()
    self.params = params

  def build(self, input_shape):
    params = self.params

    self.conv = ConvLayer(params['num_conv_layers'], params['d_model'], 
        params['kernel_size'], params['dropout_rate'])

    self.embedding = tf.keras.layers.Embedding(8, params['d_model'], mask_zero=True)

    self.encoder = Encoder(params["num_encoder_layers"], params["d_model"],
        params["num_heads"], params["dff"], params["dropout_rate"])

    self.final = tf.keras.layers.Dense(params["bio_dict_size"], activation=tf.nn.softmax)

    super(CustomModel, self).build(input_shape)

  def call(self, inputs, training=None):
    dat = inputs

    dat = self.conv(dat, training)

    pos_encoding = model_utils.positional_encoding(
        tf.shape(dat)[1],
        self.params['d_model'])

    dat += pos_encoding

    tokens = tf.repeat([[1]], repeats=[tf.shape(dat)[0]], axis=0)
    token_embeddings = self.embedding(tokens)

    clz = tf.reshape(token_embeddings[:, 0, :], [-1, 1, self.params['d_model']])

    logits = tf.concat([clz, dat], 1)

    logits = self.encoder(logits, training)

    logits = self.final(logits[:, 0, :])

    # logits = tf.reshape(logits, [-1])
    
    return logits


class ConvLayer(tf.keras.layers.Layer):
  def __init__(self, num_conv_layers, d_model, kernel_size, dropout_rate):
    super(ConvLayer, self).__init__()
    self.num_conv_layers = num_conv_layers
    self.d_model = d_model
    self.kernel_size = kernel_size
    self.dropout_rate = dropout_rate

  def build(self, input_shape):
    self.internal_layers = [{
      "conv": tf.keras.layers.Conv1D(self.d_model, self.kernel_size, padding='same'),
      "norm": tf.keras.layers.LayerNormalization(epsilon=1e-6),
    } for _ in range(self.num_conv_layers)]
    # self.max_pool = tf.keras.layers.MaxPool1D(pool_size=self.kernel_size, strides=2, padding='same')
    super(ConvLayer, self).build(input_shape)

  def call(self, inputs, training=None):
    logits = inputs[:, :, tf.newaxis]
    
    for i, layer in enumerate(self.internal_layers):
      conv = layer["conv"]
      norm = layer["norm"]

      conv_output = conv(logits)
      if i == self.num_conv_layers - 1:
        conv_output = tf.nn.relu(conv_output)
      if training:
        conv_output = tf.nn.dropout(conv_output, rate=self.dropout_rate)
      logits = norm(logits + conv_output)
    
    # logits = self.max_pool(logits)

    return logits


class Encoder(tf.keras.layers.Layer):
  def __init__(self, num_layers, d_model, num_heads, dff, dropout_rate):
    super(Encoder, self).__init__()
    self.num_layers = num_layers
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate

  def build(self, input_shape):
    self.internal_layers = [{
      "self_attention": attention_layer.SelfAttention(self.d_model, self.num_heads, self.dropout_rate),
      "attention_norm": tf.keras.layers.LayerNormalization(epsilon=1e-6),
      "ffn": ffn_layer.FeedForwardNetwork(self.d_model, self.dff, self.dropout_rate),
      "ffn_norm": tf.keras.layers.LayerNormalization(epsilon=1e-6)
    } for _ in range(self.num_layers)]
    super(Encoder, self).build(input_shape)

  def call(self, inputs, training=None):
    logits = inputs
    
    for layer in self.internal_layers:
      self_attention = layer["self_attention"]
      attention_norm = layer["attention_norm"]
      ffn = layer["ffn"]
      ffn_norm = layer["ffn_norm"]

      attn_output = self_attention(logits, training=training)
      if training:
        attn_output = tf.nn.dropout(attn_output, rate=self.dropout_rate)
      logits = attention_norm(logits + attn_output)
      
      ffn_output = ffn(logits, training=training)
      if training:
        ffn_output = tf.nn.dropout(ffn_output, rate=self.dropout_rate)
      logits = ffn_norm(logits + ffn_output)
    
    return logits

