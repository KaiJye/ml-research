from collections import defaultdict

BASE_PARAMS = defaultdict(
    lambda: None,

    static_batch=False,
    max_to_keep=1,
    data_dir='',
    model_dir='',

    epochs=1,
    steps_per_epoch=None,
    verbose=1,

    d_model=64,
    num_heads=4,
    num_encoder_layers=6,
    num_conv_layers=6,
    kernel_size=4,
    dff=2048,
    dropout_rate=0.1,
    batch_size=64,
    test_batch_size=64,
    single_batch_test=False,
    max_length=64,

    thresholds=0.5,
    num_thresholds=200,

    label_smoothing=0.1,
    learning_rate=1e-5,
    optimizer_adam_beta1=0.9,
    optimizer_adam_beta2=0.98,
    optimizer_adam_epsilon=1e-09,

    num_gpus=1,
)
