import os
from os import path
import tensorflow as tf
import wfdb
from common import dataset_utils
import itertools
import json
from random import randrange
from random import choice, sample

sample_size = 40
json_path = ''

def serialize(label, d0, d1):
  feature = {
      'label': dataset_utils.int64_feature([bool(label.numpy())]),
      'd0': dataset_utils.float_feature(d0.numpy()),
      'd1': dataset_utils.float_feature(d1.numpy()),
  }
  example_proto = tf.train.Example(features=tf.train.Features(feature=feature))
  return example_proto.SerializeToString()

def tf_serialize(label, d0, d1):
  tf_string = tf.py_function(serialize, (label, d0, d1), tf.string)
  return tf.reshape(tf_string, ())

def gen():
  with open(json_path) as json_file:
    id_labels = json.load(json_file)
  for k, v in id_labels.items():
    no_of_signals = len(v)
    if no_of_signals > sample_size:
      samples = sample(range(no_of_signals), sample_size)
    else:
      samples = range(no_of_signals)
    index_combinations = itertools.combinations(samples, 2)
    for index in index_combinations:
      yield True, v[index[0]], v[index[1]]
      while True:
        other_k = choice(list(id_labels.keys()))
        if other_k != k:
          break
      other_person = id_labels[other_k]
      yield False, choice(v), choice(other_person)

def get_ds():
  ds = tf.data.Dataset.from_generator(
      gen, 
      (tf.bool, tf.float32, tf.float32), 
      (tf.TensorShape([]), tf.TensorShape([None]), tf.TensorShape([None])),
  )
  ds = ds.take(262144).shuffle(262144)
  ds = ds.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)
  return ds

def build_train():
  SHARDS = 100
  global json_path
  global sample_size
  json_path = r'D:\data\id_label_train.json'
  folder = r'ecg_p0_v0'
  ds = get_ds()
  sample_size = 20

  prefix = 'train'
  index = 0
  for i in range(SHARDS):

    shard_ds = ds.shard(SHARDS, i)

    shard_ds = shard_ds.map(tf_serialize)

    writer = tf.data.experimental.TFRecordWriter(f"D:/data/{folder}/{prefix}-{index}.tfrecord")
    writer.write(shard_ds)

    index += 1

def build_val():
  SHARDS = 5
  global json_path
  global sample_size
  json_path = r'D:\data\id_label_val.json'
  folder = r'ecg_p0_v0'
  ds = get_ds()
  sample_size = 20

  prefix = 'val'
  index = 0
  for i in range(SHARDS):

    shard_ds = ds.shard(SHARDS, i)

    shard_ds = shard_ds.map(tf_serialize)

    writer = tf.data.experimental.TFRecordWriter(f"D:/data/{folder}/{prefix}-{index}.tfrecord")
    writer.write(shard_ds)

    index += 1

def build_test():
  SHARDS = 10
  global json_path
  global sample_size
  json_path = r'D:\data\id_label_test.json'
  folder = r'ecg_p0_v0'
  ds = get_ds()
  sample_size = 20

  prefix = 'test'
  index = 0
  for i in range(SHARDS):

    shard_ds = ds.shard(SHARDS, i)

    shard_ds = shard_ds.map(tf_serialize)

    writer = tf.data.experimental.TFRecordWriter(f"D:/data/{folder}/{prefix}-{index}.tfrecord")
    writer.write(shard_ds)

    index += 1

build_train()
build_val()
build_test()