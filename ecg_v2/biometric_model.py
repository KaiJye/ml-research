import tensorflow as tf
from common import attention_layer, ffn_layer, model_utils

def create_training_model(params):
  d0 = tf.keras.layers.Input((params['max_length'],), dtype=tf.float32)
  d1 = tf.keras.layers.Input((params['max_length'],), dtype=tf.float32)
  # label = tf.keras.layers.Input((None,), dtype=tf.int64)
  internal_model = CustomModel(params)
  logits = internal_model([d0, d1])
  model = tf.keras.Model([d0, d1], logits)
  return model


class CustomModel(tf.keras.Model):

  def __init__(self, params):
    super(CustomModel, self).__init__()
    self.params = params

  def build(self, input_shape):
    params = self.params

    self.conv = ConvLayer(params['num_conv_layers'], params['d_model'], 
        params['kernel_size'], params['dropout_rate'])

    self.embedding = tf.keras.layers.Embedding(8, params['d_model'], mask_zero=True)

    self.first_enc_norm = tf.keras.layers.LayerNormalization(epsilon=1e-6)

    self.encoder = Encoder(params["num_encoder_layers"], params["d_model"],
        params["num_heads"], params["dff"], params["dropout_rate"])

    self.ffn_0 = tf.keras.layers.Dense(params['d_model'], activation=tf.nn.relu)
    self.final_norm_0 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.ffn_1 = tf.keras.layers.Dense(params['d_model'] / 2, activation=tf.nn.relu)
    self.final_norm_1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.final = tf.keras.layers.Dense(1)

    super(CustomModel, self).build(input_shape)

  def call(self, inputs, training=None):
    d0, d1 = inputs[0], inputs[1]

    d0 = self.conv(d0, training)

    d1 = self.conv(d1, training)

    tokens = tf.repeat([[1, 2]], repeats=[tf.shape(d0)[0]], axis=0)
    token_embeddings = self.embedding(tokens)

    clz = tf.reshape(token_embeddings[:, 0, :], [-1, 1, self.params['d_model']])
    sep = tf.reshape(token_embeddings[:, 1, :], [-1, 1, self.params['d_model']])

    # pos = model_utils.get_position_encoding(tf.shape(d0)[1], self.params['d_model'])
    # d0 += pos
    # d1 += pos

    logits = tf.concat([clz, d0, sep, d1], 1)

    logits = self.first_enc_norm(logits)

    logits = self.encoder(logits, training)

    logits = logits[:, 0, :]

    ffn_out = self.ffn_0(logits)

    if training:
      ffn_out = tf.nn.dropout(ffn_out, rate=self.params["dropout_rate"])

    logits = self.final_norm_0(logits + ffn_out)

    logits = self.ffn_1(logits)

    if training:
      logits = tf.nn.dropout(logits, rate=self.params["dropout_rate"])

    logits = self.final_norm_1(logits)

    logits = self.final(logits)

    logits = tf.reshape(logits, [-1])

    if not training:
      logits = tf.sigmoid(logits)
    
    return logits


class ConvLayer(tf.keras.layers.Layer):
  def __init__(self, num_conv_layers, d_model, kernel_size, dropout_rate):
    super(ConvLayer, self).__init__()
    self.num_conv_layers = num_conv_layers
    self.d_model = d_model
    self.kernel_size = kernel_size
    self.dropout_rate = dropout_rate

  def build(self, input_shape):
    self.internal_layers = [{
      "conv": tf.keras.layers.Conv1D(self.d_model, self.kernel_size, padding='valid', 
          kernel_initializer='glorot_uniform', use_bias=True, 
          kernel_regularizer='l1', bias_regularizer='l1'),
      "pool": tf.keras.layers.MaxPooling1D(pool_size=2,
          strides=None, padding='valid'),
      "norm": tf.keras.layers.LayerNormalization(epsilon=1e-6),
    } for _ in range(self.num_conv_layers)]
    super(ConvLayer, self).build(input_shape)

  def call(self, inputs, training=None):
    logits = inputs[:, :, tf.newaxis]
    
    for i, layer in enumerate(self.internal_layers):
      conv = layer["conv"]
      pool = layer["pool"]
      norm = layer["norm"]

      internal_output = conv(logits)
      if i == self.num_conv_layers - 1:
        internal_output = tf.nn.relu(internal_output)
      # else:
      #   internal_output = tf.nn.relu(internal_output)

      internal_output = pool(internal_output)
      
      # internal_output = tf.nn.softmax(internal_output, axis=1)

      if training:
        internal_output = tf.nn.dropout(internal_output, rate=self.dropout_rate)
      # logits = norm(logits + internal_output)
      logits = norm(internal_output)
    
    return logits


class Encoder(tf.keras.layers.Layer):
  def __init__(self, num_layers, d_model, num_heads, dff, dropout_rate):
    super(Encoder, self).__init__()
    self.num_layers = num_layers
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate

  def build(self, input_shape):
    self.internal_layers = [{
      "self_attention": attention_layer.SelfAttention(self.d_model, self.num_heads, self.dropout_rate),
      "attention_norm": tf.keras.layers.LayerNormalization(epsilon=1e-6),
      "ffn": ffn_layer.FeedForwardNetwork(self.d_model, self.dff, self.dropout_rate),
      "ffn_norm": tf.keras.layers.LayerNormalization(epsilon=1e-6)
    } for _ in range(self.num_layers)]
    super(Encoder, self).build(input_shape)

  def call(self, inputs, training=None):
    logits = inputs
    
    for layer in self.internal_layers:
      self_attention = layer["self_attention"]
      attention_norm = layer["attention_norm"]
      ffn = layer["ffn"]
      ffn_norm = layer["ffn_norm"]

      attn_output = self_attention(logits, training=training)
      if training:
        attn_output = tf.nn.dropout(attn_output, rate=self.dropout_rate)
      logits = attention_norm(logits + attn_output)
      
      ffn_output = ffn(logits, training=training)
      if training:
        ffn_output = tf.nn.dropout(ffn_output, rate=self.dropout_rate)
      logits = ffn_norm(logits + ffn_output)
    
    return logits

