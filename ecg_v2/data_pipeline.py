import tensorflow as tf
import os

_READ_RECORD_BUFFER = 8 * 1000 * 1000

def get_ds(params, split):
  file_pattern = os.path.join(params["data_dir"] or "", f"*{split}*")
  ds = tf.data.Dataset.list_files(file_pattern, shuffle=True)
  options = tf.data.Options()
  options.experimental_deterministic = False
  ds = ds.interleave(
      _load_records,
      cycle_length=tf.data.experimental.AUTOTUNE,
      num_parallel_calls=tf.data.experimental.AUTOTUNE).with_options(options)
  ds = ds.map(_parse_example,
      num_parallel_calls=tf.data.experimental.AUTOTUNE)
  ds = ds.padded_batch(params['batch_size'], padded_shapes=([], [params['max_length']], [params['max_length']]))

  def ds_map(label, d0, d1):
    return (d0, d1), label

  ds = ds.map(ds_map, num_parallel_calls=tf.data.experimental.AUTOTUNE)
  ds = ds.cache()

  ds = ds.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)

  return ds

def _parse_example(serialized_example):
  """Return inputs and targets Tensors from a serialized tf.Example."""
  data_fields = {
      'label': tf.io.FixedLenFeature([], tf.int64, 0),
      'd0': tf.io.VarLenFeature(tf.float32),
      'd1': tf.io.VarLenFeature(tf.float32),
  }
  parsed = tf.io.parse_single_example(serialized_example, data_fields)
  label = parsed['label']
  d0 = tf.sparse.to_dense(parsed['d0'])
  d1 = tf.sparse.to_dense(parsed['d1'])
  return label, d0, d1

def _load_records(filename):
  """Read file and return a dataset of tf.Examples."""
  return tf.data.TFRecordDataset(filename, buffer_size=_READ_RECORD_BUFFER)
