# import wfdb
# from wfdb import processing
import tensorflow as tf
import json
from ecg_v1 import model_params
from ecg_v2 import dataset_builder_2, data_pipeline, main
from matplotlib import pyplot as plt

# record = wfdb.rdrecord(r'E:\data\ecg\ecg-id-database-1.0.0\Person_07/rec_1')
# print(record.__dict__)
# annotation = wfdb.rdann(r'E:\data\ecg\ecg-id-database-1.0.0\Person_07/rec_1', 'atr')
# wfdb.plot_wfdb(record=record, annotation=annotation)

# resampled_x, resampled_t = processing.resample_sig(record.p_signal[:, 1], record.fs, 256)
# plt.plot(resampled_x)
# plt.show()

# plot_inputs = record.p_signal[:, 0]
# inputs = plot_inputs[tf.newaxis, tf.newaxis, :, tf.newaxis]
# # print(inputs)

# outputs = tf.nn.avg_pool2d(
#   inputs, [1, 1, 4, 1], [1, 1, 4, 1], 'VALID', data_format='NHWC'
# )

# outputs = tf.squeeze(outputs)

# fig, (ax1, ax2) = plt.subplots(2)
# ax1.plot(plot_inputs)
# ax2.plot(outputs)
# plt.show()

# with open(r'E:\data\id_label.json') as json_file:
#   id_labels = json.load(json_file)

# test = id_labels['Person_85'][2]
# plt.plot(test)
# plt.show()

params = model_params.BASE_PARAMS.copy()
params.update(
    model_dir=r"D:\models\test",
    data_dir=r"D:\data\273x2x64",
    single_batch_test=True,
    epochs=10,
    steps_per_epoch=None,
    d_model=128,
    num_heads=4,
    num_encoder_layers=3,
    num_conv_layers=1,
    kernel_size=5,
    dff=256,
    verbose=1,
    max_length=128,
    batch_size=64,
    learning_rate=1e-4,
    optimizer_adam_beta1=0.9,
    optimizer_adam_beta2=0.999,
    optimizer_adam_epsilon=1e-07,
)

task = main.MainTask(params, resolver=None)
task.train()

def test():
  test_ds = data_pipeline.get_ds(params, 'test')
  result = task.eval(test_ds)

  # print(result)
  tprs = []
  fprs = []
  for ex in result:
      tpr = ex['tp'] / (ex['tp'] + ex['fn'])
      fpr = ex['fp'] / (ex['fp'] + ex['tn'])
      fnr = ex['fn'] / (ex['fn'] + ex['tp'])
      print(fpr, fnr)
      tprs.append(tpr)
      fprs.append(fpr)
  plt.plot(tprs, fprs)
  plt.show()

test()

# ds = data_pipeline.get_ds(params, 'val')
# for i, ex in ds.enumerate():
#   print(i)
#   a = tf.reduce_all(tf.math.is_finite(ex[0][0]))
#   b = tf.reduce_all(tf.math.is_finite(ex[0][1]))

#   if not a:
#     print(a.numpy())
#     print(ex[0][0])

#   if not b.numpy(): 
#     print(ex[0][1])

# with open(r'D:\data\id_label_train.json') as json_file:
#   id_labels = json.load(json_file)
# print(len(id_labels))

# for k,v in id_labels.items():
#   print(k, len(v))

# dataset_builder_2.build('train', r'D:\data\755x2x128_4\json/id_label_train.json', r'D:\data\755x2x128_4', 100)
# dataset_builder_2.build('val', r'D:\data\755x2x128_4\json/id_label_val.json', r'D:\data\755x2x128_4', 5)
# dataset_builder_2.build('test', r'D:\data\755x2x128_4\json/id_label_test.json', r'D:\data\755x2x128_4', 10)
# dataset_builder_2.build('test_id', r'D:\data\755x2x128_4\json_test/id_label_test.json', r'D:\data\755x2x128_4', 10)
