import os
from os import path
import tensorflow as tf
import wfdb
from common import dataset_utils
import itertools
import json
from random import randrange
from random import choice, sample

def serialize(label, d0, d1):
  feature = {
      'label': dataset_utils.int64_feature([bool(label.numpy())]),
      'd0': dataset_utils.float_feature(d0.numpy()),
      'd1': dataset_utils.float_feature(d1.numpy()),
  }
  example_proto = tf.train.Example(features=tf.train.Features(feature=feature))
  return example_proto.SerializeToString()

def tf_serialize(label, d0, d1):
  tf_string = tf.py_function(serialize, (label, d0, d1), tf.string)
  return tf.reshape(tf_string, ())

def gen(json_path, sample_size):
  with open(json_path) as json_file:
    id_labels = json.load(json_file)
  for k, v in id_labels.items():
    no_of_signals = len(v)
    if no_of_signals > sample_size:
      samples = sample(range(no_of_signals), sample_size)
    else:
      samples = range(no_of_signals)
    index_combinations = itertools.combinations(samples, 2)
    for index in index_combinations:
      yield 1, v[index[0]], v[index[1]]
      while True:
        other_k = choice(list(id_labels.keys()))
        if other_k != k:
          break
      other_person = id_labels[other_k]
      yield 0, choice(v), choice(other_person)

def get_ds(json_path, sample_size):  
  ds = tf.data.Dataset.from_generator(
      gen, 
      (tf.int64, tf.float32, tf.float32), 
      (tf.TensorShape([]), tf.TensorShape([None]), tf.TensorShape([None])),
      (tf.constant(json_path, tf.string), tf.constant(sample_size, tf.int64))
  )
  ds = ds.take(524288).shuffle(524288)
  ds = ds.cache()
  return ds

def build(prefix, json_path, output_path, shards=100, sample_size=20):  
  ds = get_ds(json_path, sample_size)

  index = 0
  for i in range(shards):

    shard_ds = ds.shard(shards, i)

    shard_ds = shard_ds.map(tf_serialize)

    writer = tf.data.experimental.TFRecordWriter(f"{output_path}/{prefix}-{index}.tfrecord")
    writer.write(shard_ds)

    index += 1

