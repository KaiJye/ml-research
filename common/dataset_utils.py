import tensorflow as tf
import re

PAD_ID = 1
SOS_ID = 2
EOS_ID = 3
POS_ID = 4
EMP_ID = 5
MAS_ID = 6
SEP_ID = 7
CLS_ID = 8
CU9_ID = 9
CU10_ID = 10
CU11_ID = 11
CU12_ID = 12
CU13_ID = 13
CU14_ID = 14
CU15_ID = 15
CU16_ID = 16
PUNC_PATTERN = r"[\u2000-\u206F\u2E00-\u2E7F\#*+\/<=>\[\]^_`{|}~]"

def byte_feature(value):
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=value))

def int64_feature(value):
  return tf.train.Feature(int64_list=tf.train.Int64List(value=value))

def float_feature(value):
  return tf.train.Feature(float_list=tf.train.FloatList(value=value))

def filter_fn(de, en):
  if tf.strings.length(de) < 20 or tf.strings.length(en) < 20 or tf.strings.regex_full_match(de, ".*the.*"):
    return False
  else:
    return True

def replace_punctuation_fn(de, en):
  de = de.numpy().decode('utf-8')
  de = re.sub(PUNC_PATTERN, '', de)
  de = re.sub(r"([,\.!\?\'\"$%&()\-:;@])\1+", r"\1", de)
  de = re.sub(r"([,\.!\?\'\"$%&()\-:;@])", r" \1 ", de)
  de = re.sub(r"\s+", ' ', de)

  en = en.numpy().decode('utf-8')
  en = re.sub(PUNC_PATTERN, '', en)
  en = re.sub(r"([,\.!\?\'\"$%&()\-:;@])\1+", r"\1", en)
  en = re.sub(r"([,\.!\?\'\"$%&()\-:;@])", r" \1 ", en)
  en = re.sub(r"\s+", ' ', en)
  return (de, en)

def tf_replace_punctuation_fn(de, en):
  return tf.py_function(replace_punctuation_fn, (de, en), [tf.string, tf.string])
