import tensorflow as tf

def _log_prob_from_logits(logits):
  return logits - tf.reduce_logsumexp(logits, axis=2, keepdims=True)

def _length_normalization(alpha, length, dtype=tf.float32):
  """Return length normalization factor."""
  return tf.pow(((5. + tf.cast(length, dtype)) / 6.), alpha)

def _gather_beams(nested, beam_indices, batch_size, new_beam_size):
  """Gather beams from nested structure of tensors.
  Each tensor in nested represents a batch of beams, where beam refers to a
  single search state (beam search involves searching through multiple states
  in parallel).
  This function is used to gather the top beams, specified by
  beam_indices, from the nested tensors.
  Args:
    nested: Nested structure (tensor, list, tuple or dict) containing tensors
      with shape [batch_size, beam_size, ...].
    beam_indices: int32 tensor with shape [batch_size, new_beam_size]. Each
     value in beam_indices must be between [0, beam_size), and are not
     necessarily unique.
    batch_size: int size of batch
    new_beam_size: int number of beams to be pulled from the nested tensors.
  Returns:
    Nested structure containing tensors with shape
      [batch_size, new_beam_size, ...]
  """
  # Computes the i'th coodinate that contains the batch index for gather_nd.
  # Batch pos is a tensor like [[0,0,0,0,],[1,1,1,1],..].
  batch_pos = tf.range(batch_size)
  batch_pos = tf.expand_dims(batch_pos, axis=1)
  batch_pos = tf.repeat(batch_pos, repeats=new_beam_size, axis=1)

  # Create coordinates to be passed to tf.gather_nd. Stacking creates a tensor
  # with shape [batch_size, beam_size, 2], where the last dimension contains
  # the (i, j) gathering coordinates.
  coordinates = tf.stack([batch_pos, beam_indices], axis=2)

  return tf.nest.map_structure(
      lambda state: tf.gather_nd(state, coordinates), nested)

def _gather_topk_beams(nested, score_or_log_prob, batch_size, beam_size):
  """Gather top beams from nested structure."""
  _, topk_indexes = tf.nn.top_k(score_or_log_prob, k=beam_size)
  return _gather_beams(nested, topk_indexes, batch_size, beam_size)

class _StateKeys:
  CUR_INDEX = "CUR_INDEX"
  ALIVE_SEQ = "ALIVE_SEQ"
  ALIVE_LOG_PROBS = "ALIVE_LOG_PROBS"
  FINISHED_SEQ = "FINISHED_SEQ"
  FINISHED_SCORES = "FINISHED_SCORES"
  FINISHED_FLAGS = "FINISHED_FLAGS"

class BeamSearch:

  def __init__(self,
               vocab_size,
               beam_size,
               alpha,
               extra_length,
               sos_id,
               eos_id
               ):
    self.vocab_size = vocab_size
    self.beam_size = beam_size
    self.alpha = alpha
    self.extra_length = extra_length
    self.sos_id = sos_id
    self.eos_id = eos_id

  def search(self, symbols_to_logits_fn, batch_size, max_decode_length):
    self.symbols_to_logits_fn = symbols_to_logits_fn
    self.batch_size = batch_size
    self.max_decode_length = max_decode_length

    cur_index = tf.constant(0)

    alive_seq = tf.ones([self.batch_size, self.beam_size, 1], dtype=tf.int64) * self.sos_id
    alive_log_probs = tf.constant([[0.] + [-float("inf")] * (self.beam_size - 1)], dtype=tf.float32)
    finished_seq = finished_seq = tf.zeros(tf.shape(alive_seq), tf.int64)
    finished_scores = tf.ones([self.batch_size, self.beam_size],
                              dtype=tf.float32) * -1e7
    finished_flags = tf.zeros([self.batch_size, self.beam_size], tf.bool)
    tar = {
        _StateKeys.CUR_INDEX: cur_index,
        _StateKeys.ALIVE_SEQ: alive_seq,
        _StateKeys.ALIVE_LOG_PROBS: alive_log_probs,
        _StateKeys.FINISHED_SEQ: finished_seq,
        _StateKeys.FINISHED_SCORES: finished_scores,
        _StateKeys.FINISHED_FLAGS: finished_flags
    }
    tar_shape = {
        _StateKeys.CUR_INDEX: tf.TensorShape([]),
        _StateKeys.ALIVE_SEQ: tf.TensorShape([self.batch_size, self.beam_size, None]),
        _StateKeys.ALIVE_LOG_PROBS: tf.TensorShape([self.batch_size, self.beam_size]),
        _StateKeys.FINISHED_SEQ: tf.TensorShape([self.batch_size, self.beam_size, None]),
        _StateKeys.FINISHED_SCORES: tf.TensorShape([self.batch_size, self.beam_size]),
        _StateKeys.FINISHED_FLAGS: tf.TensorShape([self.batch_size, self.beam_size])
    }

    (finished_state,) = tf.nest.map_structure(tf.stop_gradient, 
        tf.while_loop(self._continue_search, self._search_step, 
            loop_vars=(tar,),
            shape_invariants=(tar_shape,)))

    alive_seq = finished_state[_StateKeys.ALIVE_SEQ]
    alive_log_probs = finished_state[_StateKeys.ALIVE_LOG_PROBS]
    finished_seq = finished_state[_StateKeys.FINISHED_SEQ]
    finished_scores = finished_state[_StateKeys.FINISHED_SCORES]
    finished_flags = finished_state[_StateKeys.FINISHED_FLAGS]

    finished_seq = tf.where(
        tf.expand_dims(finished_flags, axis=2), finished_seq, alive_seq)
    finished_scores = tf.where(finished_flags, finished_scores, alive_log_probs)
    return finished_seq, finished_scores

  def _continue_search(self, state):
    """Return whether to continue the search loop.
    The loops should terminate when
      1) when decode length has been reached, or
      2) when the worst score in the finished sequences is better than the best
         score in the alive sequences (i.e. the finished sequences are provably
         unchanging)
    Args:
      state: A dictionary with the current loop state.
    Returns:
      Bool tensor with value True if loop should continue, False if loop should
      terminate.
    """
    i = state[_StateKeys.CUR_INDEX]
    alive_log_probs = state[_StateKeys.ALIVE_LOG_PROBS]
    finished_scores = state[_StateKeys.FINISHED_SCORES]
    finished_flags = state[_StateKeys.FINISHED_FLAGS]

    not_at_max_decode_length = tf.less(i, self.max_decode_length)

    # Calculate largest length penalty (the larger penalty, the better score).
    max_length_norm = _length_normalization(self.alpha, self.max_decode_length,
                                            dtype=tf.float32)
    # Get the best possible scores from alive sequences.
    best_alive_scores = alive_log_probs[:, 0] / max_length_norm

    # Compute worst score in finished sequences for each batch element
    finished_scores *= tf.cast(finished_flags,
                               tf.float32)  # set filler scores to zero
    lowest_finished_scores = tf.reduce_min(finished_scores, axis=1)

    # If there are no finished sequences in a batch element, then set the lowest
    # finished score to -INF for that element.
    finished_batches = tf.reduce_any(finished_flags, 1)
    lowest_finished_scores += ((1.0 -
                                tf.cast(finished_batches, tf.float32)) *
                               -1e7)

    worst_finished_score_better_than_best_alive_score = tf.reduce_all(
        tf.greater(lowest_finished_scores, best_alive_scores)
    )

    return tf.logical_and(
        not_at_max_decode_length,
        tf.logical_not(worst_finished_score_better_than_best_alive_score)
    )

  def _search_step(self, state):
    """Beam search loop body.
    Grow alive sequences by a single ID. Sequences that have reached the EOS
    token are marked as finished. The alive and finished sequences with the
    highest log probabilities and scores are returned.
    A sequence's finished score is calculating by dividing the log probability
    by the length normalization factor. Without length normalization, the
    search is more likely to return shorter sequences.
    Args:
      state: A dictionary with the current loop state.
    Returns:
      new state dictionary.
    """
    # Grow alive sequences by one token.
    new_seq, new_log_probs, topk_ids = self._grow_alive_seq(state)
    new_finished_flags = tf.equal(topk_ids, self.eos_id)
    # Collect top beam_size alive sequences
    alive_state = self._get_new_alive_state(new_seq, new_log_probs,
                                            new_finished_flags)

    # Combine newly finished sequences with existing finished sequences, and
    # collect the top k scoring sequences.
    finished_state = self._get_new_finished_state(state, new_seq, new_log_probs,
                                                  new_finished_flags)

    # Increment loop index and create new state dictionary
    new_state = {_StateKeys.CUR_INDEX: state[_StateKeys.CUR_INDEX] + 1}
    new_state.update(alive_state)
    new_state.update(finished_state)
    return [new_state]

  def _get_new_alive_state(self, new_seq, new_log_probs, new_finished_flags):
    """Gather the top k sequences that are still alive.
    Args:
      new_seq: New sequences generated by growing the current alive sequences
        int32 tensor with shape [batch_size, 2 * beam_size, cur_index + 1]
      new_log_probs: Log probabilities of new sequences float32 tensor with
        shape [batch_size, beam_size]
      new_finished_flags: A boolean Tensor indicates which sequences are live
        inside the beam.
      new_cache: Dict of cached values for each sequence.
    Returns:
      Dictionary with alive keys from _StateKeys:
        {Top beam_size sequences that are still alive (don't end with eos_id)
         Log probabilities of top alive sequences
         Dict cache storing decoder states for top alive sequences}
    """
    # To prevent finished sequences from being considered, set log probs to -inf
    new_log_probs += tf.cast(new_finished_flags, dtype=tf.float32) * -1e7

    top_alive_seq, top_alive_log_probs = _gather_topk_beams(
        [new_seq, new_log_probs], new_log_probs, self.batch_size,
        self.beam_size)

    return {
        _StateKeys.ALIVE_SEQ: top_alive_seq,
        _StateKeys.ALIVE_LOG_PROBS: top_alive_log_probs,
    }

  def _get_new_finished_state(self, state, new_seq, new_log_probs,
                              new_finished_flags):
    """Combine new and old finished sequences, and gather the top k sequences.
    Args:
      state: A dictionary with the current loop state.
      new_seq: New sequences generated by growing the current alive sequences
        int32 tensor with shape [batch_size, beam_size, i + 1]
      new_log_probs: Log probabilities of new sequences float32 tensor with
        shape [batch_size, beam_size]
      new_finished_flags: A boolean Tensor indicates which sequences are live
        inside the beam.
    Returns:
      Dictionary with finished keys from _StateKeys:
        {Top beam_size finished sequences based on score,
         Scores of finished sequences,
         Finished flags of finished sequences}
    """
    i = state[_StateKeys.CUR_INDEX]
    finished_seq = state[_StateKeys.FINISHED_SEQ]
    finished_scores = state[_StateKeys.FINISHED_SCORES]
    finished_flags = state[_StateKeys.FINISHED_FLAGS]

    # First append a column of 0-ids to finished_seq to increment the length.
    # New shape of finished_seq: [batch_size, beam_size, i + 1]
    finished_seq = tf.concat([
        finished_seq,
        tf.zeros([self.batch_size, self.beam_size, 1], tf.int64)], 
        axis=2)

    # Calculate new seq scores from log probabilities.
    length_norm = _length_normalization(self.alpha, i + 1, dtype=tf.float32)
    new_scores = new_log_probs / length_norm

    # Set the scores of the still-alive seq in new_seq to large negative values.
    new_scores += ((1. - tf.cast(new_finished_flags, tf.float32)) * -1e7)

    # Combine sequences, scores, and flags.
    finished_seq = tf.concat([finished_seq, new_seq], axis=1)
    finished_scores = tf.concat([finished_scores, new_scores], axis=1)
    finished_flags = tf.concat([finished_flags, new_finished_flags], axis=1)

    # Return the finished sequences with the best scores.
    top_finished_seq, top_finished_scores, top_finished_flags =_gather_topk_beams(
          [finished_seq, finished_scores, finished_flags],
          finished_scores, self.batch_size, self.beam_size)

    return {
        _StateKeys.FINISHED_SEQ: top_finished_seq,
        _StateKeys.FINISHED_SCORES: top_finished_scores,
        _StateKeys.FINISHED_FLAGS: top_finished_flags
    }

  def _grow_alive_seq(self, state):

    alive_seq = state[_StateKeys.ALIVE_SEQ] # [batch_size, beam_size, i+1]
    alive_log_probs = state[_StateKeys.ALIVE_LOG_PROBS] # [batch_size, beam_size]

    tar = tf.reshape(alive_seq, [-1, tf.shape(alive_seq)[-1]])
    logits = self.symbols_to_logits_fn(tar)
    last = logits[:, -1:, :]
    log_prob = _log_prob_from_logits(last) # [batch_size*beam_size, vocab_size]
    log_prob = tf.reshape(log_prob, [self.batch_size, self.beam_size, self.vocab_size])
    log_prob = log_prob + tf.expand_dims(alive_log_probs, axis=2) # [batch_size, beam_size, vocab_size]
    log_prob = tf.reshape(log_prob, [self.batch_size, self.beam_size * self.vocab_size]) # [batch_size, beam_size*vocab_size]

    twice_beam_size = self.beam_size * 2
    log_prob, indices = tf.math.top_k(log_prob, k=twice_beam_size) # [batch_size, self.beam_size*2]

    beam_indices = indices // self.vocab_size

    (top_seqs,) = _gather_beams((alive_seq,), beam_indices, self.batch_size, twice_beam_size)

    top_ids = indices % self.vocab_size # [batch_size, self.beam_size*2]
    top_ids = tf.cast(top_ids, dtype=tf.int64)

    top_seqs = tf.concat([top_seqs, tf.expand_dims(top_ids, axis=2)], axis=2) #[batch_size, beam_size, n]
    
    return top_seqs, log_prob, top_ids