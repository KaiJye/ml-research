import sys
import tensorflow as tf
import tensorflow_datasets as tfds
from common import dataset_utils

NUM_THREAD = 4
SHARDS = 5
FILE_PREFIX = "validation"
SPLIT = tfds.Split.VALIDATION
VOCAB_PATH = r"D:/vocab/vocab_en_de_v3"

vocab = tfds.features.text.SubwordTextEncoder.load_from_file(VOCAB_PATH)

def tf_encode_fn(vocab):

  def encode(en, de):
    inp = vocab.encode(en.numpy())
    tar = vocab.encode(de.numpy())
    return inp, tar
  
  def py_fn(de, en):
    return tf.py_function(encode, (en, de), [tf.int64, tf.int64])
  
  return py_fn

def serialize(inp, tar):
  feature = {
      'inp': dataset_utils.int64_feature(inp),
      'tar': dataset_utils.int64_feature(tar),
  }
  example_proto = tf.train.Example(features=tf.train.Features(feature=feature))
  return example_proto.SerializeToString()

def tf_serialize(inp, tar):
  tf_string = tf.py_function(serialize, (inp, tar), tf.string)
  return tf.reshape(tf_string, ())

raw_ds = tfds.load(
      name="wmt14_translate/de-en", 
      data_dir="D:/data", 
      split=SPLIT,
      shuffle_files=False,
      as_supervised=True)

raw_ds = raw_ds.filter(dataset_utils.filter_fn)

raw_ds = raw_ds.map(dataset_utils.tf_replace_punctuation_fn, num_parallel_calls=tf.data.experimental.AUTOTUNE)

if SPLIT == tfds.Split.TRAIN:
  raw_ds = raw_ds.cache(r"D:\data\cache\en_de_filter_clean_punc")

# if SPLIT == tfds.Split.TRAIN:
#   raw_ds = raw_ds.cache(r"D:\data\cache\en_de_encode_serialize")
# else:
#   raw_ds = raw_ds.cache(r"D:\data\cache\temp")

for i in range(SHARDS):
  # if i % NUM_THREAD - int(sys.argv[1]) != 0:
  #   continue
  print("building shard:", i)
  shard_ds = raw_ds.shard(SHARDS, i)

  shard_ds = shard_ds.map(tf_encode_fn(vocab), num_parallel_calls=tf.data.experimental.AUTOTUNE)

  shard_ds = shard_ds.map(tf_serialize, num_parallel_calls=tf.data.experimental.AUTOTUNE)

  writer = tf.data.experimental.TFRecordWriter(f"D:/data/wmt14_preprocessed_v3/{FILE_PREFIX}-{i}.tfrecord")
  writer.write(shard_ds)
