import tensorflow as tf
import math

def get_special_angles_2(p, length, i, d_model):
  angle_rates = 1 / tf.pow(tf.cast(1000 * (p + 1), tf.float32), (i - d_model) / (4 * d_model))
  return (length - 0.5) * angle_rates

def left_right_encoding_2(position, length, d_model, left=True):
  if left:
    rand_pool_range = 16
  else:
    rand_pool_range = 8
  rand_pool = tf.range(0, tf.cast(rand_pool_range, tf.float32), dtype=tf.float32)
  rand_pool = tf.random.shuffle(rand_pool)
  angle_rads = get_special_angles_2(
      rand_pool[:position, tf.newaxis, tf.newaxis],
      tf.range(0, tf.cast(length, tf.float32), dtype=tf.float32)[tf.newaxis, :, tf.newaxis],
      tf.range(0, tf.cast(d_model, tf.float32), dtype=tf.float32)[tf.newaxis, tf.newaxis, :],
      d_model)
  
  if left:
    encoding = tf.sin(angle_rads)
  else:
    encoding = tf.cos(angle_rads)
  
  return encoding

def get_special_angles(length, i, d_model, p=0):
  angle_rates = 1 / tf.pow(tf.cast(1000 * (p + 1), tf.float32), (i - d_model) / (4 * d_model))
  return (length - 0.5) * angle_rates

def left_right_encoding(length, d_model, left=True, position=0):
  angle_rads = get_special_angles(
      tf.range(0, tf.cast(length, tf.float32), dtype=tf.float32)[:, tf.newaxis],
      tf.range(0, tf.cast(d_model, tf.float32), dtype=tf.float32)[tf.newaxis, :],
      d_model, position)
  
  if left:
    encoding = tf.sin(angle_rads)
  else:
    encoding = tf.cos(angle_rads)
  
  return encoding

def get_angles(length, i, d_model, position=0):
  angle_rates = 1 / tf.pow(tf.cast(10000 * (position + 1), tf.float32), (2 * (i//2)) / d_model)
  return length * angle_rates

def positional_encoding(length, d_model):
  angle_rads = get_angles(
      tf.range(0, tf.cast(length, tf.float32), dtype=tf.float32)[:, tf.newaxis],
      tf.range(0, tf.cast(d_model, tf.float32), dtype=tf.float32)[tf.newaxis, :],
      d_model)
        
  pos_encoding = tf.reshape(
      tf.stack(
          [tf.sin(angle_rads[:, 0::2]), tf.cos(angle_rads[:, 1::2])], axis=-1), 
          [length, d_model])
  
  return pos_encoding

def relative_positional_encoding(length, d_model, max_length):
  angle_rads = get_angles(
      tf.linspace(0., 512., length)[:, tf.newaxis],
      tf.range(0, tf.cast(d_model, tf.float32), dtype=tf.float32)[tf.newaxis, :],
      d_model)
        
  pos_encoding = tf.reshape(
      tf.stack(
          [tf.sin(angle_rads[:, 0::2]), tf.cos(angle_rads[:, 1::2])], axis=-1), 
          [length, d_model])
  
  return tf.pad(pos_encoding, [[0, tf.cast(max_length, dtype=tf.int32) - length], [0, 0]])

def relative_positional_encoding_dict(params, drop_size=0):
  length = tf.range(1, params["max_length"] - drop_size + 1, dtype=tf.float32)
  pos_encoding = tf.map_fn(
        lambda x: relative_positional_encoding(tf.cast(x[0, 0], dtype=tf.int32), params["d_model"], params["max_length"] - drop_size), 
        length[:, tf.newaxis, tf.newaxis])
  return tf.pad(pos_encoding, [[1, 0], [0, 0], [0, 0]])

def relative_pos_ecg(mask, d_model, min_val=0.0, max_val=512, padding_value=0, dtype=tf.float32):
  mask = tf.cast(tf.not_equal(mask, padding_value), tf.int32)
  max_length = tf.shape(mask)[-1]
  base = tf.linspace(tf.cast(min_val, dtype), tf.cast(max_val, dtype), max_length)
  length = tf.math.reduce_sum(mask, axis=-1) - 1
  pos = base / tf.gather(base, length)[:, tf.newaxis] * max_val

  angle_rads = get_angles(
      pos[:, :, tf.newaxis],
      tf.range(0, d_model, dtype=dtype)[tf.newaxis, tf.newaxis, :],
      d_model)
        
  pos_encoding = tf.reshape(
      tf.stack(
          [tf.sin(angle_rads[:, :, 0::2]), tf.cos(angle_rads[:, :, 1::2])], axis=-1), 
          [-1, max_length, d_model])
  
  return pos_encoding


def gather_relative_positional_encoding(rped, x, offset=0):
  length = tf.reduce_sum(tf.cast(tf.not_equal(x, 0), dtype=tf.int32), axis=-1)
  pos_encoding = tf.gather(rped, length - offset)
  if offset > 0:
    pos_encoding = tf.pad(pos_encoding, [[0, 0], [offset, 0], [0, 0]])[:, :-offset, :]
  return pos_encoding

def get_position_encoding(
    length, d_model, min_timescale=1.0, max_timescale=1.0e4):
  """Return positional encoding.
  Calculates the position encoding as a mix of sine and cosine functions with
  geometrically increasing wavelengths.
  Defined and formulized in Attention is All You Need, section 3.5.
  Args:
    length: Sequence length.
    d_model: Size of the
    min_timescale: Minimum scale that will be applied at each position
    max_timescale: Maximum scale that will be applied at each position
  Returns:
    Tensor with shape [length, d_model]
  """
  # We compute the positional encoding in float32 even if the model uses
  # float16, as many of the ops used, like log and exp, are numerically unstable
  # in float16.
  position = tf.cast(tf.range(length), tf.float32)
  num_timescales = d_model // 2
  log_timescale_increment = (
      math.log(float(max_timescale) / float(min_timescale)) /
      (tf.cast(num_timescales, tf.float32) - 1))
  inv_timescales = min_timescale * tf.exp(
      tf.cast(tf.range(num_timescales), tf.float32) * -log_timescale_increment)
  scaled_time = tf.expand_dims(position, 1) * tf.expand_dims(inv_timescales, 0)
  signal = tf.concat([tf.sin(scaled_time), tf.cos(scaled_time)], axis=1)
  return signal

def create_padding_mask(seq):
  seq = tf.cast(tf.math.equal(seq, 0), tf.float32)
  
  return seq[:, tf.newaxis, tf.newaxis, :]  # (batch_size, 1, 1, seq_len)

def create_look_ahead_mask(size):
  mask = 1 - tf.linalg.band_part(tf.ones((size, size)), -1, 0)
  return mask  # (seq_len, seq_len)

def create_combined_mask(seq):  
  look_ahead_mask = create_look_ahead_mask(tf.shape(seq)[1])
  dec_target_padding_mask = create_padding_mask(seq)
  combined_mask = tf.maximum(dec_target_padding_mask, look_ahead_mask)
  
  return combined_mask

def get_decoder_self_attention_bias(length, dtype=tf.float32):
  """Calculate bias for decoder that maintains model's autoregressive property.
  Creates a tensor that masks out locations that correspond to illegal
  connections, so prediction at position i cannot draw information from future
  positions.
  Args:
    length: int length of sequences in batch.
    dtype: The dtype of the return value.
  Returns:
    float tensor of shape [1, 1, length, length]
  """
  with tf.name_scope("decoder_self_attention_bias"):
    valid_locs = tf.linalg.band_part(tf.ones([length, length], dtype=dtype),
                                     -1, 0)
    valid_locs = tf.reshape(valid_locs, [1, 1, length, length])
    decoder_bias = -1e9 * (1.0 - valid_locs)
  return decoder_bias

def get_padding_bias(x, padding_value=0, dtype=tf.float32):
  """Calculate bias tensor from padding values in tensor.
  Bias tensor that is added to the pre-softmax multi-headed attention logits,
  which has shape [batch_size, num_heads, length, length]. The tensor is zero at
  non-padding locations, and -1e9 (negative infinity) at padding locations.
  Args:
    x: int tensor with shape [batch_size, length]
    padding_value: int which represents padded values in input
    dtype: The dtype of the return value
  Returns:
    Attention bias tensor of shape [batch_size, 1, 1, length].
  """
  with tf.name_scope("attention_bias"):
    padding = tf.cast(tf.equal(x, padding_value), dtype)
    attention_bias = padding * -1e9
    attention_bias = tf.expand_dims(
        tf.expand_dims(attention_bias, axis=1), axis=1)
  return attention_bias