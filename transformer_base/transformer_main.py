"""Train and evaluate the Transformer model.
See README for description of setting the training schedule and evaluating the
BLEU score.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import tempfile
import time

import tensorflow as tf

import transformer
import data_pipeline
import metrics
import optimizer
import tokenizer
import translate
import compute_bleu
import model_params

INF = int(1e9)
BLEU_DIR = "bleu"
_SINGLE_SAMPLE = 1

def translate_and_compute_bleu(model,
                               params,
                               subtokenizer,
                               bleu_source,
                               bleu_ref,
                               distribution_strategy=None):
  """Translate file and report the cased and uncased bleu scores.
  Args:
    model: A Keras model, used to generate the translations.
    params: A dictionary, containing the translation related parameters.
    subtokenizer: A subtokenizer object, used for encoding and decoding source
      and translated lines.
    bleu_source: A file containing source sentences for translation.
    bleu_ref: A file containing the reference for the translated sentences.
    distribution_strategy: A platform distribution strategy, used for TPU based
      translation.
  Returns:
    uncased_score: A float, the case insensitive BLEU score.
    cased_score: A float, the case sensitive BLEU score.
  """
  # Create temporary file to store translation.
  tmp = tempfile.NamedTemporaryFile(delete=False)
  tmp_filename = tmp.name

  translate.translate_file(
      model,
      params,
      subtokenizer,
      bleu_source,
      output_file=tmp_filename,
      print_all_translations=False,
      distribution_strategy=None)

  # Compute uncased and cased bleu scores.
  uncased_score = compute_bleu.bleu_wrapper(bleu_ref, tmp_filename, False)
  cased_score = compute_bleu.bleu_wrapper(bleu_ref, tmp_filename, True)
  os.remove(tmp_filename)
  return uncased_score, cased_score


def evaluate_and_log_bleu(model,
                          params,
                          bleu_source,
                          bleu_ref,
                          vocab_file,
                          distribution_strategy=None):
  """Calculate and record the BLEU score.
  Args:
    model: A Keras model, used to generate the translations.
    params: A dictionary, containing the translation related parameters.
    bleu_source: A file containing source sentences for translation.
    bleu_ref: A file containing the reference for the translated sentences.
    vocab_file: A file containing the vocabulary for translation.
    distribution_strategy: A platform distribution strategy, used for TPU based
      translation.
  Returns:
    uncased_score: A float, the case insensitive BLEU score.
    cased_score: A float, the case sensitive BLEU score.
  """
  subtokenizer = tokenizer.Subtokenizer(vocab_file)

  uncased_score, cased_score = translate_and_compute_bleu(
      model, params, subtokenizer, bleu_source, bleu_ref, distribution_strategy)

  print("Bleu score (uncased): ", uncased_score)
  print("Bleu score (cased): ", cased_score)
  return uncased_score, cased_score


class TransformerTask(object):
  """Main entry of Transformer model."""

  def __init__(self, flags_obj):
    """Init function of TransformerMain.
    Args:
      flags_obj: Object containing parsed flag values, i.e., FLAGS.
    Raises:
      ValueError: if not using static batch for input data on TPU.
    """
    self.flags_obj = flags_obj
    self.predict_model = None

    # Add flag-defined parameters to params object
    num_gpus = get_num_gpus()
    self.params = params = get_model_params(flags_obj['param_type'])

    params["num_gpus"] = num_gpus
    params["use_ctl"] = flags_obj['use_ctl']
    params["data_dir"] = flags_obj['data_dir']
    params["model_dir"] = flags_obj['model_dir']
    params["static_batch"] = flags_obj['static_batch']
    params["max_length"] = flags_obj['max_length']
    params["decode_batch_size"] = flags_obj['decode_batch_size']
    params["decode_max_length"] = flags_obj['decode_max_length']
    params["beam_size"] = flags_obj['beam_size']
    params["padded_decode"] = flags_obj['padded_decode']
    params["num_parallel_calls"] = flags_obj['num_parallel_calls']

    params["use_synthetic_data"] = flags_obj['use_synthetic_data']
    params["batch_size"] = flags_obj['batch_size'] or params["default_batch_size"]
    params["repeat_dataset"] = None
    params["dtype"] = flags_obj['dtype']
    params["enable_tensorboard"] = flags_obj['enable_tensorboard']
    params["enable_metrics_in_training"] = flags_obj['enable_metrics_in_training']
    params["steps_between_evals"] = flags_obj['steps_between_evals']

    self.distribution_strategy = tf.distribute.MirroredStrategy()
    # self.distribution_strategy = None
    print("Running transformer with num_gpus ", num_gpus)

    if self.distribution_strategy:
      print("For training, using distribution strategy: %s",
                   self.distribution_strategy)
    else:
      print("Not using any distribution strategy.")

  @property
  def use_tpu(self):
    if self.distribution_strategy:
      return isinstance(self.distribution_strategy,
                        tf.distribute.experimental.TPUStrategy)
    return False

  def train(self):
    """Trains the model."""
    params = self.params
    flags_obj = self.flags_obj

    _ensure_dir(flags_obj['model_dir'])
    with get_strategy_scope(self.distribution_strategy):
      model = transformer.create_model(params, is_train=True)
      opt = self._create_optimizer()

      current_step = 0
      checkpoint = tf.train.Checkpoint(model=model, optimizer=opt)
      latest_checkpoint = tf.train.latest_checkpoint(flags_obj['model_dir'])
      if latest_checkpoint:
        checkpoint.restore(latest_checkpoint)
        print("Loaded checkpoint", latest_checkpoint)
        current_step = opt.iterations.numpy()

      model.compile(opt)

    model.summary()

    train_ds = data_pipeline.train_input_fn(params)
    map_data_fn = data_pipeline.map_data_for_transformer_fn
    train_ds = train_ds.map(
        map_data_fn, num_parallel_calls=params["num_parallel_calls"])

    callbacks = self._create_callbacks(0, params)

    cased_score, uncased_score = None, None
    cased_score_history, uncased_score_history = [], []
    stats = None
    while current_step < flags_obj['train_steps']:
      remaining_steps = flags_obj['train_steps'] - current_step
      train_steps_per_eval = (
          remaining_steps if remaining_steps < flags_obj['steps_between_evals']
          else flags_obj['steps_between_evals'])
      current_iteration = current_step // flags_obj['steps_between_evals']

      print("Start train iteration at global step:{}".format(current_step))
      history = None
      history = model.fit(
            train_ds,
            initial_epoch=current_iteration,
            epochs=current_iteration + 1,
            steps_per_epoch=train_steps_per_eval,
            callbacks=callbacks,
            verbose=flags_obj['verbose'])
      current_step += train_steps_per_eval
      # print("Train history: {}".format(history.history))
        
      print("End train iteration at global step:{}".format(current_step))

      if (flags_obj['bleu_source'] and flags_obj['bleu_ref']):
        uncased_score, cased_score = self.eval()
        cased_score_history.append([current_iteration + 1, cased_score])
        uncased_score_history.append([current_iteration + 1, uncased_score])

    if history:
      stats = (build_stats(history, callbacks))
      if uncased_score and cased_score:
        stats["bleu_uncased"] = uncased_score
        stats["bleu_cased"] = cased_score
        stats["bleu_uncased_history"] = uncased_score_history
        stats["bleu_cased_history"] = cased_score_history
    print("model training done")
    return stats

  def eval(self):
    """Evaluates the model."""
    distribution_strategy = self.distribution_strategy

    # We only want to create the model under DS scope for TPU case.
    # When 'distribution_strategy' is None, a no-op DummyContextManager will
    # be used.
    with get_strategy_scope(distribution_strategy):
      if not self.predict_model:
        self.predict_model = transformer.create_model(self.params, False)
      self._load_weights_if_possible(
          self.predict_model,
          tf.train.latest_checkpoint(self.flags_obj['model_dir']))
      self.predict_model.summary()
    return evaluate_and_log_bleu(
        self.predict_model, self.params, self.flags_obj['bleu_source'],
        self.flags_obj['bleu_ref'], self.flags_obj['vocab_file'],
        distribution_strategy)

  def predict(self):
    """Predicts result from the model."""
    params = self.params
    flags_obj = self.flags_obj

    with tf.name_scope("model"):
      model = transformer.create_model(params, is_train=False)
      self._load_weights_if_possible(model)
      model.summary()
    subtokenizer = tokenizer.Subtokenizer(flags_obj['vocab_file'])

    ds = data_pipeline.eval_input_fn(params)
    ds = ds.map(lambda x, y: x).take(_SINGLE_SAMPLE)
    ret = model.predict(ds)
    val_outputs, _ = ret
    length = len(val_outputs)
    for i in range(length):
      translate.translate_from_input(val_outputs[i], subtokenizer)

  def _create_callbacks(self, init_steps, params):
    """Creates a list of callbacks."""
    flags_obj = self.flags_obj
    callbacks = []

    if flags_obj['enable_time_history']:
      time_callback = TimeHistory(flags_obj['batch_size'], flags_obj['log_steps'])
      callbacks.append(time_callback)

    if flags_obj['enable_tensorboard']:
      tensorboard_callback = tf.keras.callbacks.TensorBoard(
          log_dir=flags_obj['model_dir'])
      callbacks.append(tensorboard_callback)

    sfunc = optimizer.LearningRateFn(params["learning_rate"],
                                     params["hidden_size"],
                                     params["learning_rate_warmup_steps"])
    scheduler_callback = optimizer.LearningRateScheduler(sfunc, init_steps)
    callbacks.append(scheduler_callback)

    ckpt_full_path = os.path.join(flags_obj['model_dir'], "cp.ckpt")
    callbacks.append(
        tf.keras.callbacks.ModelCheckpoint(
            ckpt_full_path, save_weights_only=True))

    return callbacks

  def _load_weights_if_possible(self, model, init_weight_path=None):
    """Loads model weights when it is provided."""
    if init_weight_path:
      model.load_weights(init_weight_path)
      print('Latest checkpoint restored!!')
    else:
      print("Weights not loaded from path:{}".format(init_weight_path))

  def _create_optimizer(self):
    """Creates optimizer."""
    params = self.params
    lr_schedule = optimizer.LearningRateSchedule(
        params["learning_rate"], params["hidden_size"],
        params["learning_rate_warmup_steps"])
    opt = tf.keras.optimizers.Adam(
        # lr_schedule,
        lr_schedule if self.use_tpu else params["learning_rate"],
        params["optimizer_adam_beta1"],
        params["optimizer_adam_beta2"],
        epsilon=params["optimizer_adam_epsilon"])

    return opt

def _ensure_dir(log_dir):
  """Makes log dir if not existed."""
  if not tf.io.gfile.exists(log_dir):
    tf.io.gfile.makedirs(log_dir)

def get_num_gpus():
  return len(tf.config.experimental.list_physical_devices('GPU'))

def get_model_params(param_type):
  if param_type == 'base':
    return model_params.BASE_PARAMS.copy()
  elif param_type == 'tiny':
    return model_params.TINY_PARAMS.copy()
#   return model_params.TINY_PARAMS.copy()
#   return model_params.BIG_PARAMS.copy()

def build_stats(history, callbacks):
  """Normalizes and returns dictionary of stats.
  Args:
    history: Results of the training step.
    callbacks: a list of callbacks which might include a time history callback
      used during keras.fit.
  Returns:
    Dictionary of normalized results.
  """
  stats = {}

  if history and history.history:
    train_hist = history.history
    # Gets final loss from training.
    stats['loss'] = train_hist['loss'][-1].item()

  if not callbacks:
    return stats

  # Look for the time history callback which was used during keras.fit
  for callback in callbacks:
    if isinstance(callback, TimeHistory):
      timestamp_log = callback.timestamp_log
      stats['step_timestamp_log'] = timestamp_log
      stats['train_finish_time'] = callback.train_finish_time
      if len(timestamp_log) > 1:
        stats['avg_exp_per_second'] = (
            callback.batch_size * callback.log_steps *
            (len(callback.timestamp_log)-1) /
            (timestamp_log[-1].timestamp - timestamp_log[0].timestamp))
  return stats

def get_strategy_scope(strategy):
  if strategy:
    strategy_scope = strategy.scope()
  else:
    strategy_scope = DummyContextManager()

  return strategy_scope


class DummyContextManager(object):

  def __enter__(self):
    pass

  def __exit__(self, *args):
    pass

class BatchTimestamp(object):
  """A structure to store batch time stamp."""

  def __init__(self, batch_index, timestamp):
    self.batch_index = batch_index
    self.timestamp = timestamp

  def __repr__(self):
    return "'BatchTimestamp<batch_index: {}, timestamp: {}>'".format(
        self.batch_index, self.timestamp)

class TimeHistory(tf.keras.callbacks.Callback):
  """Callback for Keras models."""

  def __init__(self, batch_size, log_steps):
    """Callback for logging performance.
    Args:
      batch_size: Total batch size.
      log_steps: Interval of steps between logging of batch level stats.
    """
    self.batch_size = batch_size
    super(TimeHistory, self).__init__()
    self.log_steps = log_steps
    self.global_steps = 0

    # Logs start of step 1 then end of each step based on log_steps interval.
    self.timestamp_log = []

    # Records the time each epoch takes to run from start to finish of epoch.
    self.epoch_runtime_log = []

  def on_train_end(self, logs=None):
    self.train_finish_time = time.time()

  def on_epoch_begin(self, epoch, logs=None):
    self.epoch_start = time.time()

  def on_batch_begin(self, batch, logs=None):
    self.global_steps += 1
    if self.global_steps == 1:
      self.start_time = time.time()
      self.timestamp_log.append(BatchTimestamp(self.global_steps,
                                               self.start_time))

  def on_batch_end(self, batch, logs=None):
    """Records elapse time of the batch and calculates examples per second."""
    if self.global_steps % self.log_steps == 0:
      timestamp = time.time()
      elapsed_time = timestamp - self.start_time
      examples_per_second = (self.batch_size * self.log_steps) / elapsed_time
      self.timestamp_log.append(BatchTimestamp(self.global_steps, timestamp))
      print(
          "BenchmarkMetric: {'global step':%d, 'time_taken': %f,"
          "'examples_per_second': %f}",
          self.global_steps, elapsed_time, examples_per_second)
      self.start_time = timestamp

  def on_epoch_end(self, epoch, logs=None):
    epoch_run_time = time.time() - self.epoch_start
    self.epoch_runtime_log.append(epoch_run_time)
    print(
        "BenchmarkMetric: {'epoch':%d, 'time_taken': %f}",
        epoch, epoch_run_time)
