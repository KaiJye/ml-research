import importlib
import tensorflow as tf
import flags
import model_params
import transformer
import transformer_main

importlib.reload(flags)
importlib.reload(model_params)
importlib.reload(transformer)
importlib.reload(transformer_main)

tf.keras.backend.clear_session()

flags_obj = flags.DEFAULT_FLAGS.copy()
flags_obj.update(
    param_type = 'base',
    checkpoints_to_keep=1,
    epochs=1,
    steps_per_epoch=5000,
    decode_max_length=64,
    data_dir = "D:/data",
    model_dir = "D:/models",
    vocab_file = "D:/data/vocab.ende.32768",
    bleu_source = "D:/data/newstest2014.en",
    bleu_ref = "D:/data/newstest2014.de",
    beam_size=2,
    verbose=1,
    # bleu_source=None,
    # bleu_ref=None,
)
task = transformer_main.TransformerTask(flags_obj)
task.train_2()
# task.eval()