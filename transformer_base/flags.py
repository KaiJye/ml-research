import tensorflow as tf
from collections import defaultdict

DEFAULT_FLAGS = defaultdict(
    lambda: None,

    param_type='base',
    use_ctl = False,
    data_dir = "/content/gdrive/My Drive/colab-workspace/data/wmt14_preprocessed_ori",
    model_dir = "/content/gdrive/My Drive/colab-workspace/models",
    vocab_file = "/content/gdrive/My Drive/colab-workspace/vocab/vocab.ende.32768",
    bleu_source = "/content/gdrive/My Drive/colab-workspace/data/wmt14_preprocessed_ori/newstest2014.en",
    bleu_ref = "/content/gdrive/My Drive/colab-workspace/data/wmt14_preprocessed_ori/newstest2014.de",
    static_batch = False,
    max_length = 256,
    decode_batch_size = 32,
    decode_max_length = 128,
    padded_decode = False,
    num_parallel_calls = tf.data.experimental.AUTOTUNE,
    use_synthetic_data = False,
    batch_size = 1024,
    dtype = tf.float32,
    enable_time_history = False,
    enable_tensorboard = False,
    enable_metrics_in_training = False,
    steps_between_evals = 5000,
    train_steps = 100000,
    verbose=2,
    beam_size=4,
    log_steps = 100,
)
