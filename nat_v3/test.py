import tensorflow as tf
import tensorflow_datasets as tfds
import matplotlib.pyplot as plt
from nat_v3 import main
from common import model_utils
from transformer_v1 import model_params

params = model_params.BASE_PARAMS.copy()
params.update(
    model_name="nat_v3",
    ut_model_name="ut_v2",
    model_dir=r"D:\models",
    data_dir=r"D:\data\wmt14_preprocessed_v3",
    vocab_path=r"D:\vocab\vocab_en_de_v3",
    with_val=False,
    single_batch_test=True,
    num_fresh_layer=6,
    epochs=1,
    steps_per_epoch=10,
    verbose=1,
    test_batch_size=64,
    vocab_size=32517,
    batch_size=8,
    max_length=256,
    learning_rate_modifier=1.0,
    warmup_steps=4000,
    optimizer_adam_beta1=0.9,
    optimizer_adam_beta2=0.98,
    optimizer_adam_epsilon=1e-09,
)

task = main.MainTask(params, resolver=None, epoch_callback=None)
# task.train()
task.eval()
