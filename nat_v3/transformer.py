import tensorflow as tf
from common import embedding_layer, attention_layer, ffn_layer, model_utils
from unlabelled_transformer_v2 import unlabelled_transformer as ut

def create_training_model(params):
  en_model, en_embedding, encoder, _, _, _ = ut.create_training_model(params)
  de_model, de_embedding, _, length_predictor, decoder_initializer, decoder = ut.create_training_model(params)

  en_embedding.trainable = False
  encoder.trainable = False
  de_embedding.trainable = False
  decoder_initializer.trainable = False
  decoder.trainable = False

  fresh_layers = Encoder(params["num_fresh_layer"], params["d_model"], params["num_heads"], params["dff"], params["dropout_rate"])

  inp = tf.keras.layers.Input((None,), dtype=tf.int64)
  length = tf.keras.layers.Input([], dtype=tf.int64)

  logits, attention_mask = en_embedding(inp, mode="embedding", training=False)

  enc_output = encoder([logits, attention_mask], training=False)

  fresh_enc_output = fresh_layers([enc_output, attention_mask])

  pred_length, pass_on_length = length_predictor([fresh_enc_output, attention_mask, length])

  dec_inputs, padding_mask, _ = decoder_initializer(
      [fresh_enc_output[:, 1:, :], pass_on_length, attention_mask[:, :, :, 1:]],
      training=False)

  dec_output = decoder(
        [dec_inputs, fresh_enc_output[:, 1:, :], padding_mask, attention_mask[:, :, :, 1:]], training=False)

  logits = de_embedding(dec_output, mode="linear", training=False)

  logits = tf.keras.layers.Lambda(lambda x: x, 
      name="output",
      dtype=tf.float32)(logits)

  pred_length = tf.keras.layers.Lambda(lambda x: x, 
      name="length",
      dtype=tf.int64)(pred_length)

  model = tf.keras.Model([inp, length], [logits, pred_length])

  return en_model, de_model, model

def create_predict_model(params):
  en_embedding = ut.Embedding(params["d_model"], params["vocab_size"])

  de_embedding = ut.Embedding(params["d_model"], params["vocab_size"])

  encoder = ut.Encoder(params["num_layers"], params["d_model"],
      params["num_heads"], params["dff"], params["dropout_rate"])

  length_predictor = ut.LengthPredictorLayer(params["d_model"],
      params["num_heads"], params["dropout_rate"])

  decoder_initializer = ut.DecoderInitializerLayer(params["max_length"], params["d_model"],
      params["num_heads"], params["dff"], params["dropout_rate"], params["static_batch"])

  decoder = ut.Decoder(params["num_layers"], params["d_model"],
      params["num_heads"], params["dff"], params["dropout_rate"])

  fresh_layers = Encoder(params["num_fresh_layer"], params["d_model"], params["num_heads"], params["dff"], params["dropout_rate"])

  inp = tf.keras.layers.Input((None,), dtype=tf.int64)
  tar = tf.keras.layers.Input((None,), dtype=tf.int64)

  logits, attention_mask = en_embedding(inp, mode="embedding")

  enc_output = encoder([logits, attention_mask])

  fresh_enc_output = fresh_layers([enc_output, attention_mask])

  pred_length, _ = length_predictor([fresh_enc_output, attention_mask])

  dec_inputs, padding_mask, length_mask = decoder_initializer(
      [fresh_enc_output[:, 1:, :], pred_length, attention_mask[:, :, :, 1:]])

  dec_output = decoder(
        [dec_inputs, fresh_enc_output[:, 1:, :], padding_mask, attention_mask[:, :, :, 1:]])

  logits = de_embedding(dec_output, mode="linear")

  logits = tf.keras.layers.Lambda(lambda x: tf.argmax(x[0], axis=-1) * tf.cast(tf.squeeze(x[1]), dtype=tf.int64),
      name="output",
      dtype=tf.float32)([logits, length_mask])

  model = tf.keras.Model([inp, tar], [inp, tar, logits])

  return model

class EncoderLayer(tf.keras.layers.Layer):
  def __init__(self, d_model, num_heads, dff, dropout_rate):
    super(EncoderLayer, self).__init__()
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate

  def build(self, input_shape):
    self.self_attention = attention_layer.SelfAttention(self.d_model, self.num_heads, self.dropout_rate)
    self.ffn = ffn_layer.FeedForwardNetwork(self.d_model, self.dff, self.dropout_rate)

    self.layernorm1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.layernorm2 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    
    self.dropout1 = tf.keras.layers.Dropout(self.dropout_rate)
    self.dropout2 = tf.keras.layers.Dropout(self.dropout_rate)
    super(EncoderLayer, self).build(input_shape)
  
  def call(self, inputs, training=None):
    x, attention_mask = inputs[0], inputs[1]

    attn_output = self.self_attention(x, attention_mask, training=training)  # (batch_size, input_seq_len, d_model)
    attn_output = self.dropout1(attn_output, training=training)
    out1 = self.layernorm1(x + attn_output)  # (batch_size, input_seq_len, d_model)
    
    ffn_output = self.ffn(out1, training=training)  # (batch_size, input_seq_len, d_model)
    ffn_output = self.dropout2(ffn_output, training=training)
    out2 = self.layernorm2(out1 + ffn_output)  # (batch_size, input_seq_len, d_model)
    
    return out2

class Encoder(tf.keras.Model):
  def __init__(self, num_layers, d_model, num_heads, dff, dropout_rate):
    super(Encoder, self).__init__()
    self.num_layers = num_layers
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate
  
  def build(self, input_shape):
    self.enc_layers = [EncoderLayer(self.d_model, self.num_heads, self.dff, self.dropout_rate)
        for _ in range(self.num_layers)]
    self.dropout = tf.keras.layers.Dropout(self.dropout_rate)
    self.layernorm = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    super(Encoder, self).build(input_shape)

  def call(self, inputs, training=None):
    x, attention_mask = inputs[0], inputs[1]

    seq_len = tf.shape(x)[1]
    
    pos_encoding = model_utils.positional_encoding(
        seq_len,
        self.d_model)
    x += pos_encoding

    x = self.dropout(x, training=training)
    x = self.layernorm(x)

    for layer in self.enc_layers:
      x = layer([x, attention_mask], training)
    
    return x  # (batch_size, input_seq_len, d_model)
