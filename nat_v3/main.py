import tensorflow as tf
import tensorflow_datasets as tfds
import os, json
import numpy as np
from transformer_v1 import optimizer
from transformer_v1 import evaluate as eval_v1
from unlabelled_transformer_v2 import metrics
from nat_v2 import data_pipeline
from common import evaluate
from nat_v3 import transformer

class MainTask:
  def __init__(self, params, resolver=None, epoch_callback=None):
    self.params = params
    self.epoch_callback = epoch_callback

    if resolver:
      self.distribution_strategy = tf.distribute.experimental.TPUStrategy(resolver)
      self.params['static_batch'] = True
    else:
      self.distribution_strategy = tf.distribute.MirroredStrategy()
      self.params['static_batch'] = False

  def train(self):
    params = self.params

    model_dir = params["model_dir"]
    model_name = params["model_name"]
    ut_model_name = params["ut_model_name"]

    model_path = os.path.join(model_dir, f"{model_name}_model.h5")
    train_state_path = os.path.join(model_dir, f"{model_name}_train_state.json")

    en_model_path = os.path.join(model_dir, f"{ut_model_name}_en_model.h5")
    de_model_path = os.path.join(model_dir, f"{ut_model_name}_de_model.h5")

    with self.distribution_strategy.scope():
      en_model, de_model, model = transformer.create_training_model(params)

      try:
        model.load_weights(model_path)
        print("model loaded")
      except:
        print("create new model")
        en_model.load_weights(en_model_path)
        de_model.load_weights(de_model_path)

      opt = optimizer.create_optimizer(params)
      current_step = 0

      try:
        with open(train_state_path) as json_file:
          train_state = json.load(json_file)
        opt.iterations = tf.Variable(train_state["steps"])
        current_step = opt.iterations.numpy()
      except:
        print("no train state found")
      
      print(f"current train step: {current_step}")

      loss_fn = metrics.loss_function
      accuracy_function = metrics.accuracy_function
      model.compile(optimizer=opt, 
          loss={ "output": loss_fn, "length": tf.keras.losses.MAE }, 
          metrics={ "output": [accuracy_function], "length": [] },
          loss_weights={ "output": 1, "length": 1 })

    model.summary()

    train_ds = data_pipeline.get_train_ds(params)
    if params["with_val"]:
      validation_ds = data_pipeline.get_validation_ds(params)
    else:
      validation_ds = None

    callbacks = self._create_callbacks(model_path, train_state_path)

    history = model.fit(train_ds,
        validation_data=validation_ds,
        epochs=params['epochs'],
        steps_per_epoch=params['steps_per_epoch'],
        callbacks=callbacks,
        verbose=params['verbose'])

    print(history.history)

  def eval(self):
    params = self.params

    model_dir = params["model_dir"]
    model_name = params["model_name"]

    model_path = os.path.join(model_dir, f"{model_name}_model.h5")

    with self.distribution_strategy.scope():
      model = transformer.create_predict_model(params)
      
      model.load_weights(model_path)
      print("model loaded")

    model.summary()

    test_ds = data_pipeline.get_test_ds(params)

    if params['single_batch_test']:
      test_ds = test_ds.take(1)

    test_ds = test_ds.map(lambda x, y: ((x[0], y[0]),))

    inp, tar, logits = model.predict(test_ds)

    inp = evaluate.decode(params, tf.constant(inp))
    tar = evaluate.decode(params, tf.constant(tar))
    logits = evaluate.decode(params, tf.constant(logits))

    print(inp)
    print(tar)
    print(logits)

  def _create_callbacks(self, model_path, train_state_path):
    callbacks = []
    learning_rate_callback = optimizer.LearningRateCallback(self.params)
    callbacks.append(learning_rate_callback)
    ckpt_callback = CheckpointCallback(model_path, train_state_path, self.epoch_callback)
    callbacks.append(ckpt_callback)
    return callbacks

class CheckpointCallback(tf.keras.callbacks.Callback):

  def __init__(self, model_path, train_state_path, epoch_callback):
    super(CheckpointCallback, self).__init__()

    self.model_path = model_path
    self.train_state_path = train_state_path
    self.epoch_callback = epoch_callback

  def on_epoch_end(self, epoch, logs=None):
    self.model.save_weights(self.model_path, save_format='h5')

    train_state = logs
    with open(self.train_state_path, 'w') as json_file:
      json.dump(train_state, json_file, default=default)
    
    if self.epoch_callback:
      self.epoch_callback()

def default(obj):
  if isinstance(obj, np.integer):
    return int(obj)
  elif isinstance(obj, np.floating):
    return float(obj)
  elif isinstance(obj, np.ndarray):
    return obj.tolist()
