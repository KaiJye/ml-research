import wfdb
from wfdb import processing
import json
import os
from os import path
from matplotlib import pyplot as plt
import numpy as np
import tensorflow as tf
from random import sample, randint

"""
Person_74 has 20 split only
"""

append = False
filtered = False
fs = 128
key_prefix = 'idd'
sample_size = 100
min_sample_size = 15
data_dir = r'D:\data\ecg\ecg-id-database-1.0.0'
json_path = r'D:\data\id_label_test.json'

if append:
  with open(json_path) as json_file:
    id_labels = json.load(json_file)
else:
  id_labels = {}

with open(path.join(data_dir, 'RECORDS')) as record_list:
  for record_entry in record_list:
    record_entry = record_entry.strip()
    record_entry_splits = record_entry.split('/')

    person = record_entry_splits[0]
    record_number = record_entry_splits[1]

    record = wfdb.rdrecord(path.join(data_dir, record_entry))
    if filtered:
      signal = record.p_signal[:, 1]
    else:
      signal = record.p_signal[:, 0]
    
    signal, _ = processing.resample_sig(signal, record.fs, fs)

    def signal_gen(lower, upper=None):
      index = 0
      total_len = len(signal)
      while True:
        if upper is None:
          size = lower
        else:
          size = randint(lower, upper)
        end = index + size
        if end >= total_len:
          break
        yield signal[index:end] 
        index = end
    
    batched_signals = list(signal_gen(fs * 1.5, fs * 2))

    key = f'{key_prefix}-{person}'
    if person not in id_labels:
      id_labels[person] = []
    signals = id_labels[person]

    for s in batched_signals:
      # normalize
      s = processing.normalize_bound(s, lb=-1, ub=1)
      
      # standardize
      # m = np.mean(s)
      # d = np.std(s)
      # s = (s - m) / d
      # wfdb.plot_items(s) 
      signals.append(s.tolist())

for key in list(id_labels.keys()):
  if len(id_labels[key]) < min_sample_size:
    del id_labels[key]
  elif len(id_labels[key]) >= sample_size:
    batched_signals = sample(id_labels[key], sample_size)
    id_labels[key] = batched_signals

print(len(id_labels))

# p_74 = id_labels.pop('Person_74')
# print(len(id_labels))
# sampled_keys = sample(id_labels.keys(), 79)
# print(sampled_keys)
# train = {}
# test = {
#   'Person_74': p_74
# }
# for k,v in id_labels.items():
#   if len(v) <= 40:
#     samples = v
#   else:
#     samples = sample(v, 40)
#   if k in sampled_keys:
#     test[k] = samples
#   else:
#     train[k] = samples

# with open(r'D:\data\id_label_train.json', 'w') as json_file:
#   json.dump(train, json_file)

# with open(r'D:\data\id_label_test.json', 'w') as json_file:
#   json.dump(test, json_file)

with open(r'D:\data\id_label_test.json', 'w') as json_file:
  json.dump(id_labels, json_file)

