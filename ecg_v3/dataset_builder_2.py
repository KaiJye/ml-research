import os, json
from random import sample
from common import dataset_utils
import tensorflow as tf



def equal_gen(json_path, num_left, num_right):
  with open(json_path) as json_file:
    id_labels = json.load(json_file)
  while True:
    keys = sample(id_labels.keys(), 2)
    k0, k1 = keys[0], keys[1]
    v0 = id_labels[k0]
    v1 = id_labels[k1]
    positive_samples = sample(v0, num_left + num_right)
    yield 1, tuple(positive_samples[:num_left]), tuple(positive_samples[num_left:])
    yield 0, tuple(sample(v0, num_left)), tuple(sample(v1, num_right))


def get_ds(split, json_dir, num_left, num_right, max_length, batch_size):
  json_path = os.path.join(json_dir, f"id_label_{split}.json")

  output_types = (tf.int32, 
      tuple(tf.float32 for _ in range(num_left)), 
      tuple(tf.float32 for _ in range(num_right)))

  output_shapes = ([], 
      tuple([None] for _ in range(num_left)), 
      tuple([None] for _ in range(num_right)))

  ds = tf.data.Dataset.from_generator(
      equal_gen, 
      output_types, 
      output_shapes=output_shapes,
      args=(tf.constant(json_path, tf.string), tf.constant(num_left, tf.int32), tf.constant(num_right, tf.int32))
  )

  ds = ds.shuffle(2**12)

  padded_shapes = ([], tuple([max_length] for _ in range(num_left)),
      tuple([max_length] for _ in range(num_right)))
  padding_values = (0, tuple(tf.constant(-1e9, tf.float32) for _ in range(num_left)),
      tuple(tf.constant(-1e9, tf.float32) for _ in range(num_right)))
  ds = ds.padded_batch(batch_size, padded_shapes=padded_shapes, padding_values=padding_values)

  def ds_map(label, d0, d1):
    d0 = tf.stack(d0, axis=1)
    d1 = tf.stack(d1, axis=1)
    return label, d0, d1

  ds = ds.map(ds_map, num_parallel_calls=tf.data.experimental.AUTOTUNE)
  
  ds = ds.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)

  return ds

def serialize(label, d0, d1):
  feature = {
      'label': dataset_utils.int64_feature(label.numpy()),
      'd0': dataset_utils.byte_feature([tf.io.serialize_tensor(d0).numpy()]),
      'd1': dataset_utils.byte_feature([tf.io.serialize_tensor(d1).numpy()]),
  }
  example_proto = tf.train.Example(features=tf.train.Features(feature=feature))
  return example_proto.SerializeToString()

def tf_serialize(label, d0, d1):
  tf_string = tf.py_function(serialize, (label, d0, d1), tf.string)
  return tf.reshape(tf_string, ())

def build(output_path, split, split_name, json_dir, num_left=5, num_right=2, max_length=384, batch_size=512, limit=512, shards=8):
  ds = get_ds(split, json_dir, num_left, num_right, max_length, batch_size)
  ds = ds.take(limit)
  ds = ds.cache()

  index = 0
  for i in range(shards):

    shard_ds = ds.shard(shards, i)

    shard_ds = shard_ds.map(tf_serialize)

    with tf.io.TFRecordWriter(
        os.path.join(output_path, f"{split_name}-{index}.tfrecord"), 
        tf.io.TFRecordOptions(compression_type='GZIP')) as writer:
      for ex in shard_ds.as_numpy_iterator():
        writer.write(ex)

    # writer = tf.data.experimental.TFRecordWriter(f"{output_path}/{split}-{index}.tfrecord")
    # writer.write(shard_ds)

    index += 1
  
