import tensorflow as tf
import math
from common import attention_layer, ffn_layer, model_utils

def create_training_model(params):
  d0 = tf.keras.layers.Input((None, params['max_length']), dtype=tf.float32)
  d1 = tf.keras.layers.Input((None, params['max_length']), dtype=tf.float32)
  internal_model = CustomModel(params)
  logits = internal_model([d0, d1])
  model = tf.keras.Model([d0, d1], logits)
  return model


class CustomModel(tf.keras.Model):

  def __init__(self, params):
    super(CustomModel, self).__init__()
    self.params = params

  def build(self, input_shape):
    params = self.params

    self.conv = ConvLayer(params['num_conv_layers'], params['filter_size'], 
        params['kernel_size'], params['conv_strides'], params['pool_size'], params['dropout_rate'])

    self.converter = tf.keras.layers.Dense(params['d_model'], use_bias=False)

    self.embedding = tf.keras.layers.Embedding(4, params['d_model'], 
        embeddings_initializer='glorot_uniform', mask_zero=True)

    self.first_enc_norm = tf.keras.layers.LayerNormalization(epsilon=1e-6)

    self.encoder = Encoder(params["num_encoder_layers"], params["d_model"],
        params["num_heads"], params["dff"], params["dropout_rate"])

    # self.ffn_0 = tf.keras.layers.Dense(params['d_model'], activation=tf.nn.relu)
    # self.final_norm_0 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.ffn_1 = tf.keras.layers.Dense(params['d_model'] / 2, activation=tf.nn.relu)
    self.final_norm_1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.final = tf.keras.layers.Dense(1)

    super(CustomModel, self).build(input_shape)

  def call(self, inputs, training=None):
    batch_size = tf.shape(inputs[0])[0]
    num_left = tf.shape(inputs[0])[1]
    num_right = tf.shape(inputs[1])[1]
    tokens = tf.repeat([[1]], repeats=[batch_size], axis=0)
    token_embeddings = self.embedding(tokens)

    clz = tf.reshape(token_embeddings[:, 0, :], [-1, 1, self.params['d_model']])

    left = inputs[0]
    right = inputs[1]
    
    conv_inputs = tf.concat([left, right], 1)

    conv_out, conv_mask = self.conv(conv_inputs)
    conv_out = self.converter(conv_out)

    encoding_left = model_utils.left_right_encoding_2(num_left, tf.shape(conv_out)[2], self.params['d_model'], left=True)
    encoding_right = model_utils.left_right_encoding_2(num_right, tf.shape(conv_out)[2], self.params['d_model'], left=False)
    encoding = tf.concat([encoding_left, encoding_right], 0)

    conv_out += encoding

    conv_out = tf.where(tf.equal(conv_mask, 0), conv_out, tf.constant(0, tf.float32))

    logits = tf.reshape(conv_out, [batch_size, -1, self.params['d_model']])

    mask = tf.reshape(conv_mask, [batch_size, -1])

    logits = tf.concat([clz, logits], 1)
    mask = tf.pad(mask, [[0, 0], [1, 0]], constant_values=0)

    mask = model_utils.get_padding_bias(mask, padding_value=1)

    if training:
      logits = tf.nn.dropout(logits, rate=self.params["dropout_rate"])

    logits = self.first_enc_norm(logits)

    logits = self.encoder([logits, mask], training)

    logits = logits[:, 0, :]

    # ffn_out = self.ffn_0(logits)

    # if training:
    #   ffn_out = tf.nn.dropout(ffn_out, rate=self.params["dropout_rate"])

    # logits = self.final_norm_0(logits + ffn_out)

    logits = self.ffn_1(logits)

    if training:
      logits = tf.nn.dropout(logits, rate=self.params["dropout_rate"])

    logits = self.final_norm_1(logits)

    logits = self.final(logits)

    logits = tf.reshape(logits, [-1])

    if not training:
      logits = tf.sigmoid(logits)
    
    return logits


class ConvLayer(tf.keras.layers.Layer):
  def __init__(self, num_conv_layers, filter_size, kernel_size, conv_strides, pool_size, dropout_rate):
    super(ConvLayer, self).__init__()
    self.num_conv_layers = num_conv_layers
    self.filter_size = filter_size
    self.kernel_size = kernel_size
    self.conv_strides = conv_strides
    self.pool_size = pool_size
    self.dropout_rate = dropout_rate

    k = kernel_size - 1
    self.kernel_reduce = int(k)
    self.lower_kernel_reduce = int(math.floor(float(k/2)))
    self.upper_kernel_reduce = int(math.ceil(float(k/2)))

  def build(self, input_shape):
    self.mask_conv = tf.keras.layers.Conv2D(1, (1, self.kernel_size),
        padding='valid', strides=(1, self.conv_strides),
        kernel_initializer=tf.keras.initializers.Constant(1), use_bias=False)
    self.mask_conv.trainable = False
    
    self.logit_pool = tf.keras.layers.MaxPooling2D(pool_size=(1, self.pool_size),
        strides=None, padding='valid')

    self.internal_layers = [{
      "conv": tf.keras.layers.Conv2D(self.filter_size, (1, self.kernel_size),
          padding='same', strides=(1, self.conv_strides),
          kernel_initializer='glorot_uniform', use_bias=True, 
          kernel_regularizer='l1_l2', bias_regularizer='l1_l2'),
      "norm": tf.keras.layers.LayerNormalization(epsilon=1e-6),
    } for _ in range(self.num_conv_layers)]
    super(ConvLayer, self).build(input_shape)

  def call(self, inputs, training=None):
    logits = inputs[:, :, :, tf.newaxis]

    mask = tf.cast(tf.less(logits, -1e7), tf.float32)
    
    for i, layer in enumerate(self.internal_layers):
      conv = layer["conv"]
      norm = layer["norm"]

      internal_output = conv(logits)
      if i == self.num_conv_layers - 1:
        internal_output = tf.nn.relu(internal_output)
      else:
        internal_output = tf.nn.relu(internal_output)

      if training:
        internal_output = tf.nn.dropout(internal_output, rate=self.dropout_rate)
      logits = norm(logits[:, :, ::self.conv_strides, :] + internal_output)
      # logits = norm(internal_output)

      logits = logits[:, :, self.lower_kernel_reduce:-1*self.upper_kernel_reduce, :]
      # mask = mask[:, self.kernel_reduce:, :]

      mask = self.mask_conv(mask)
      mask = tf.cast(tf.not_equal(mask, 0), tf.float32)
    
    logits = self.logit_pool(logits)

    mask = self.logit_pool(mask)
    mask = tf.cast(tf.not_equal(mask, 0), tf.int64)

    return logits, mask


class Encoder(tf.keras.layers.Layer):
  def __init__(self, num_layers, d_model, num_heads, dff, dropout_rate):
    super(Encoder, self).__init__()
    self.num_layers = num_layers
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate

  def build(self, input_shape):
    self.internal_layers = [{
      "self_attention": attention_layer.SelfAttention(self.d_model, self.num_heads, self.dropout_rate),
      "attention_norm": tf.keras.layers.LayerNormalization(epsilon=1e-6),
      "ffn": ffn_layer.FeedForwardNetwork(self.d_model, self.dff, self.dropout_rate),
      "ffn_norm": tf.keras.layers.LayerNormalization(epsilon=1e-6)
    } for _ in range(self.num_layers)]
    super(Encoder, self).build(input_shape)

  def call(self, inputs, training=None):
    logits, mask = inputs[0], inputs[1]
    
    for layer in self.internal_layers:
      self_attention = layer["self_attention"]
      attention_norm = layer["attention_norm"]
      ffn = layer["ffn"]
      ffn_norm = layer["ffn_norm"]

      attn_output = self_attention(logits, bias=mask, training=training)
      if training:
        attn_output = tf.nn.dropout(attn_output, rate=self.dropout_rate)
      logits = attention_norm(logits + attn_output)
      
      ffn_output = ffn(logits, training=training)
      if training:
        ffn_output = tf.nn.dropout(ffn_output, rate=self.dropout_rate)
      logits = ffn_norm(logits + ffn_output)
    
    return logits

