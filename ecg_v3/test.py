import os
import tensorflow as tf
import json
from common import model_utils
from ecg_v1 import model_params
from ecg_v3 import main, data_pipeline, dataset_builder, dataset_builder_2
from matplotlib import pyplot as plt

# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

params = model_params.BASE_PARAMS.copy()
params.update(
    model_dir=r"D:\models\test",
    data_dir=r"D:\data\832x1-2x128_512_512",
    num_left=5,
    num_right=2,
    single_batch_test=True,
    model_checkpoint=False,
    epochs=10,
    steps_per_epoch=10,
    d_model=128,
    num_heads=4,
    num_encoder_layers=1,
    num_conv_layers=1,
    kernel_size=5,
    pool_size=2,
    dff=256,
    verbose=1,
    max_length=256,
    batch_size=512,
    learning_rate=1e-4,
    optimizer_adam_beta1=0.9,
    optimizer_adam_beta2=0.999,
    optimizer_adam_epsilon=1e-07,
)

# task = main.MainTask(params, resolver=None)
# task.train()

# def test():
#   test_ds = data_pipeline.get_ds(params, 'test')
#   result = task.eval(test_ds)

#   # print(result)
#   tprs = []
#   fprs = []
#   for ex in result:
#       tpr = ex['tp'] / (ex['tp'] + ex['fn'])
#       fpr = ex['fp'] / (ex['fp'] + ex['tn'])
#       fnr = ex['fn'] / (ex['fn'] + ex['tp'])
#       print(fpr, fnr)
#       tprs.append(tpr)
#       fprs.append(fpr)
#   plt.plot(tprs, fprs)
#   plt.show()

# test()

# for i, ex in ds.enumerate():
#   print(i)

# ds = data_pipeline.get_ds(params, 'test')
# for ex in ds.take(1):
#   print(ex[0])

# dataset_builder.build_json_to_tfrecord(r'D:/data/755x2-3x128/json/id_label_train.json', r'D:/data/755x2-3x128', 'train')
# dataset_builder.build_json_to_tfrecord(r'D:/data/755x2-3x128/json/id_label_val.json', r'D:/data/755x2-3x128', 'val')
# dataset_builder.build_json_to_tfrecord(r'D:/data/755x2-3x128/json/id_label_test.json', r'D:/data/755x2-3x128', 'test')

# ds = data_pipeline.get_ds(params, 'val')

# for i, ex in ds.take(1).enumerate():
#   print(ex)

# conv0 = tf.keras.layers.Conv1D(3, 4, padding='valid', use_bias=False, kernel_initializer=tf.keras.initializers.Constant(-0.5))
# conv1 = tf.keras.layers.Conv1D(3, 4, padding='same', use_bias=False, kernel_initializer=tf.keras.initializers.Constant(-0.5))
# pool0 = tf.keras.layers.MaxPool1D(padding='valid')
# pool1 = tf.keras.layers.MaxPool1D(padding='valid')

# a = tf.ones([1, 30, 1])

# b = conv0(a)
# b = pool0(b)
# print(b)
# c = conv0(b)
# c = pool0(c)
# print(c)

# a = tf.pad(a, [[0, 0], [0, 9], [0, 0]], constant_values=-1e9)

# k = tf.constant(conv1.kernel_size[0]-1, tf.float32)
# l = tf.cast(tf.math.floor(k/2), tf.int64)
# u = tf.cast(tf.math.ceil(k/2), tf.int64)
# m = tf.cast(tf.less(a, -1), tf.int64)

# b = conv1(a)
# b = b[:, l:u*-1, :]
# m = m[:, l+u:, :]
# b = pool1(b)
# m = pool1(m)
# b = tf.where(tf.equal(m, 1), -1e9, b)
# print(b)
# print(tf.reshape(m, [1, -1]))

# c = conv1(b)
# c = c[:, l:u*-1, :]
# m = m[:, l+u:, :]
# c = pool1(c)
# m = pool1(m)
# c = tf.where(tf.equal(m, 1), -1e9, c)
# print(c)
# print(m)


# dataset_builder_2.build(r'D:/data/8-4_1_5-2', 'train', 'train', r'D:/data/raw/832x1_5-2x128_3', 
#     num_left=8, num_right=4, max_length=256, batch_size=512, limit=1024, shards=128)
# dataset_builder_2.build(r'D:/data/8-4_1_5-2', 'val', 'val', r'D:/data/raw/832x1_5-2x128_3', 
#     num_left=8, num_right=4, max_length=256, batch_size=512, limit=16, shards=8)
# dataset_builder_2.build(r'D:/data/8-4_1_5-2', 'test', 'test', r'D:/data/raw/832x1_5-2x128_id_2', 
#     num_left=8, num_right=4, max_length=256, batch_size=512, limit=64, shards=16)
# dataset_builder_2.build(r'D:/data/8-4_1_5-2', 'test', 'test_2', r'D:/data/raw/832x1_5-2x128_3', 
#     num_left=8, num_right=4, max_length=256, batch_size=512, limit=16, shards=8)

# with open(r'D:/data/raw/955x1-2x128/id_label_train.json') as json_file:
# 	id_labels = json.load(json_file)

# for k, v in id_labels.items():
# 	if len(v) < 10:
# 		print(len(v))
# 		exit()

a = tf.range(0, 27, dtype=tf.int32)
a = tf.reshape(a, [3, 3, 3])
print(a)
a = tf.transpose(a, [1, 0, 2])
a = tf.random.shuffle(a)
print(a)
a = tf.transpose(a, [1, 0, 2])
print(a)
