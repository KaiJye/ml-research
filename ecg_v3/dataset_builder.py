import os
from os import path
import tensorflow as tf
import wfdb
from common import dataset_utils
import itertools
import json
from random import randrange
from random import choice, sample
from ecg_v3 import data_pipeline


def gen(json_path, num_left, num_right):
  with open(json_path) as json_file:
    id_labels = json.load(json_file)
  while True:
    keys = sample(id_labels.keys(), 2)
    k0, k1 = keys[0], keys[1]
    v0 = id_labels[k0]
    v1 = id_labels[k1]
    positive_samples = sample(v0, num_left + num_right)
    yield 1, tuple(positive_samples[:num_left]), tuple(positive_samples[num_left:])
    yield 0, tuple(sample(v0, num_left)), tuple(sample(v1, num_right))

def get_ds(params, split):
  json_path = os.path.join(params['data_dir'] or "", f"id_label_{split}.json")

  output_types = (tf.int64, 
      tuple(tf.float32 for _ in range(params['num_left'])), 
      tuple(tf.float32 for _ in range(params['num_right'])))

  output_shapes = ([], 
      tuple([None] for _ in range(params['num_left'])), 
      tuple([None] for _ in range(params['num_right'])))

  ds = tf.data.Dataset.from_generator(
      gen, 
      output_types, 
      output_shapes=output_shapes,
      args=(tf.constant(json_path, tf.string), tf.constant(params['num_left'], tf.int64), tf.constant(params['num_right'], tf.int64))
  )

  return ds

def build(params, split, json_path, output_path, limit=1024, shards=5):  
  ds = get_ds(params, split)
  
  ds = ds.shuffle(2**10)
  ds = ds.cache()

  index = 0
  for i in range(shards):

    shard_ds = ds.shard(shards, i)

    shard_ds = shard_ds.map(tf_serialize)

    writer = tf.data.experimental.TFRecordWriter(f"{output_path}/{split}-{index}.tfrecord")
    writer.write(shard_ds)

    index += 1

def serialize(label, d0, d1):
  feature = {
      'label': dataset_utils.int64_feature([label.numpy()]),
      'd0': dataset_utils.float_feature(d0.numpy()),
      'd1': dataset_utils.float_feature(d1.numpy()),
  }
  example_proto = tf.train.Example(features=tf.train.Features(feature=feature))
  return example_proto.SerializeToString()

def tf_serialize(label, d0, d1):
  tf_string = tf.py_function(serialize, (label, d0, d1), tf.string)
  return tf.reshape(tf_string, ())

def build_json_to_tfrecord(json_path, output_path, split):
  with open(json_path) as json_file:
    id_labels = json.load(json_file)

  serialized = tf.io.serialize_tensor([tf.io.serialize_tensor([tf.io.serialize_tensor(value) for value in values]) for _, values in id_labels.items()])
  
  with tf.io.TFRecordWriter(f"{output_path}/{split}.tfrecord") as writer:
    writer.write(serialized.numpy())


