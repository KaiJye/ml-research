import tensorflow as tf
import math
from common import attention_layer, ffn_layer, model_utils


def create_training_model(params, threshold=0.5, return_logit=False):
  d0 = tf.keras.layers.Input([None, None], dtype=tf.float32)
  d1 = tf.keras.layers.Input([None, None], dtype=tf.float32)
  internal_model = CustomModel(params, threshold, return_logit)
  logits = internal_model([d0, d1])
  model = tf.keras.Model([d0, d1], logits)
  return model, internal_model


class CustomModel(tf.keras.Model):

  def __init__(self, params, threshold=0.5, return_logit=False):
    super(CustomModel, self).__init__()
    self.params = params
    self.threshold = threshold
    self.return_logit = return_logit
    self.pos = model_utils.positional_encoding(512, self.params['d_model'])

  def build(self, input_shape):
    params = self.params

    self.conv = ConvLayer(params['num_conv_layers'], params['filter_size'], 
        params['kernel_size'], params['conv_strides'], params['pool_size'], params['dropout_rate'])

    self.converter = tf.keras.layers.Dense(params['d_model'], use_bias=False)
    self.converter_norm = tf.keras.layers.LayerNormalization(epsilon=1e-6)

    self.clz = self.add_weight(
        "clz",
        shape=[params["d_model"]],
        initializer='glorot_uniform')
    
    self.group_encoding = tf.keras.layers.Dense(params['d_model'])
    self.segment_encoding = tf.keras.layers.Dense(params['d_model'])

    self.first_enc_norm = tf.keras.layers.LayerNormalization(epsilon=1e-6)

    self.encoder = Encoder(params["num_encoder_layers"], params["d_model"],
        params["num_heads"], params["dff"], params["dropout_rate"])

    # self.ffn_0 = tf.keras.layers.Dense(params['d_model'], activation=tf.nn.relu)
    # self.final_norm_0 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.ffn_1 = tf.keras.layers.Dense(params['d_model'] / 2, activation=tf.nn.relu)
    self.final_norm_1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.final = tf.keras.layers.Dense(1)

    super(CustomModel, self).build(input_shape)

  def call(self, inputs, training=None):
    batch_size = tf.shape(inputs[0])[0]
    num_left = tf.shape(inputs[0])[1]
    num_right = tf.shape(inputs[1])[1]
    d_model = self.params['d_model']

    padding_value = tf.constant(-1e9, dtype=tf.float32)
    left = tf.where(tf.math.is_finite(inputs[0]), inputs[0], padding_value)
    right = tf.where(tf.math.is_finite(inputs[1]), inputs[1], padding_value)
    
    conv_inputs = tf.concat([left, right], 1)

    conv_out, conv_mask = self.conv(conv_inputs)
    conv_out = self.converter(conv_out)
    conv_out = self.converter_norm(conv_out)

    segment_length = tf.shape(conv_out)[2]

    group_pos = self.group_encoding(self.pos[:3, :])
    expanded_group_pos = tf.repeat(group_pos, [0, num_left*segment_length, num_right*segment_length], axis=0)
    
    segment_pos_left = self.segment_encoding(self.pos[:num_left, :])
    expanded_segment_pos_left = tf.repeat(segment_pos_left, segment_length, axis=0)

    segment_pos_right = self.segment_encoding(self.pos[:num_right, :])
    expanded_segment_pos_right = tf.repeat(segment_pos_right, segment_length, axis=0)

    encoding = expanded_group_pos + tf.concat([expanded_segment_pos_left, expanded_segment_pos_right], 0)

    encoding = tf.reshape(encoding, [num_left+num_right, segment_length, d_model])

    conv_out += self.pos[:segment_length, :]

    conv_out += encoding

    conv_out = tf.where(tf.equal(conv_mask, 0), conv_out, tf.constant(0, tf.float32))

    logits = tf.reshape(conv_out, [batch_size, -1, d_model])

    conv_mask = tf.reshape(conv_mask, [batch_size, -1])

    clz = self.clz[tf.newaxis, tf.newaxis, :]
    clz += group_pos[tf.newaxis, 0, :]
    clz = tf.repeat(clz, batch_size, axis=0)
    logits = tf.concat([clz, logits], 1)
    conv_mask = tf.pad(conv_mask, [[0, 0], [1, 0]], constant_values=0)

    mask = model_utils.get_padding_bias(conv_mask, padding_value=1)

    if training:
      logits = tf.nn.dropout(logits, rate=self.params["dropout_rate"])

    logits = self.first_enc_norm(logits)

    eye = tf.eye(tf.shape(logits)[1]) * -1e9
    logits = self.encoder([logits, tf.minimum(mask, eye)], training)

    logits = logits[:, 0, :]

    if self.return_logit:
      return tf.reshape(logits, [batch_size, -1])

    # ffn_out = self.ffn_0(logits)

    # if training:
    #   ffn_out = tf.nn.dropout(ffn_out, rate=self.params["dropout_rate"])

    # logits = self.final_norm_0(logits + ffn_out)

    logits = self.ffn_1(logits)

    if training:
      logits = tf.nn.dropout(logits, rate=self.params["dropout_rate"])

    logits = self.final_norm_1(logits)

    logits = self.final(logits)

    if training:
      # logits = tf.reshape(logits, [batch_size, -1])

      # reduce_mask = tf.cast(conv_mask, tf.float32) * -1e9
      # logits += reduce_mask
      # logits = tf.math.reduce_max(logits, axis=-1)

      logits = tf.reshape(logits, [batch_size])
    else:
      # logits = tf.reshape(logits, [batch_size, -1])

      # reduce_mask = tf.cast(tf.logical_not(tf.cast(conv_mask, tf.bool)), tf.float32)
      # logits *= reduce_mask
      # logits = tf.reduce_sum(logits, axis=-1) / tf.reduce_sum(reduce_mask, axis=-1)

      # reduce_mask = tf.cast(conv_mask, tf.float32) * -1e9
      # logits += reduce_mask
      # logits = tf.math.reduce_max(logits, axis=-1)

      logits = tf.reshape(logits, [batch_size])

      logits = tf.sigmoid(logits)
    
    return logits


class ConvLayer(tf.keras.layers.Layer):
  def __init__(self, num_conv_layers, filter_size, kernel_size, conv_strides, pool_size, dropout_rate):
    super(ConvLayer, self).__init__()
    self.num_conv_layers = num_conv_layers
    self.filter_size = filter_size
    self.kernel_size = kernel_size
    self.conv_strides = conv_strides
    self.pool_size = pool_size
    self.dropout_rate = dropout_rate

    k = kernel_size - 1
    self.kernel_reduce = int(k)
    self.lower_kernel_reduce = int(math.floor(float(k/2)))
    self.upper_kernel_reduce = int(math.ceil(float(k/2)))

  def build(self, input_shape):    
    self.logit_pool = tf.keras.layers.MaxPooling2D(pool_size=(1, self.pool_size),
        strides=None, padding='valid')

    self.internal_layers = [{
      "conv": tf.keras.layers.Conv2D(self.filter_size * pow(2, i), (1, self.kernel_size),
          padding='same', strides=(1, self.conv_strides),
          kernel_initializer='glorot_uniform', use_bias=True, 
          kernel_regularizer='l1_l2', bias_regularizer='l1_l2'),
      "norm": tf.keras.layers.LayerNormalization(epsilon=1e-6),
      "mask_conv": tf.keras.layers.Conv2D(1, (1, self.kernel_size),
          padding='valid', strides=(1, self.conv_strides),
          kernel_initializer=tf.keras.initializers.Constant(1), use_bias=False,
          trainable=False)
    } for i in range(self.num_conv_layers)]
    super(ConvLayer, self).build(input_shape)

  def call(self, inputs, training=None):
    logits = inputs[:, :, :, tf.newaxis]

    mask = tf.cast(tf.less(logits, -1e7), tf.float32)
    
    for i, layer in enumerate(self.internal_layers):
      conv = layer["conv"]
      norm = layer["norm"]
      mask_conv = layer["mask_conv"]

      internal_output = conv(logits)
      if i == self.num_conv_layers - 1:
        internal_output = tf.nn.relu(internal_output)
      else:
        internal_output = tf.nn.relu(internal_output)

      if training:
        internal_output = tf.nn.dropout(internal_output, rate=self.dropout_rate)
      # logits = norm(logits[:, :, ::self.conv_strides, :] + internal_output)
      logits = norm(internal_output)

      mask = mask_conv(mask)
      mask = tf.cast(tf.not_equal(mask, 0), tf.float32)

      d_length = tf.cast(tf.shape(logits)[2] - tf.shape(mask)[2], tf.float32)
      lower_kernel_reduce = tf.cast(tf.math.floor(d_length / 2), tf.int32)
      upper_kernel_reduce = tf.cast(tf.math.ceil(d_length / 2), tf.int32)
      logits = logits[:, :, lower_kernel_reduce:-1*upper_kernel_reduce, :]
    
      logits = self.logit_pool(logits)

      mask = self.logit_pool(mask)
    mask = tf.cast(tf.not_equal(mask, 0), tf.int64)

    return logits, mask


class Encoder(tf.keras.layers.Layer):
  def __init__(self, num_layers, d_model, num_heads, dff, dropout_rate):
    super(Encoder, self).__init__()
    self.num_layers = num_layers
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate

  def build(self, input_shape):
    self.internal_layers = [{
      "self_attention": attention_layer.SelfAttention(self.d_model, self.num_heads, self.dropout_rate),
      "attention_norm": tf.keras.layers.LayerNormalization(epsilon=1e-6),
      "ffn": ffn_layer.FeedForwardNetwork(self.d_model, self.dff, self.dropout_rate),
      "ffn_norm": tf.keras.layers.LayerNormalization(epsilon=1e-6)
    } for _ in range(self.num_layers)]
    super(Encoder, self).build(input_shape)

  def call(self, inputs, training=None):
    logits, mask = inputs[0], inputs[1]
    
    for layer in self.internal_layers:
      self_attention = layer["self_attention"]
      attention_norm = layer["attention_norm"]
      ffn = layer["ffn"]
      ffn_norm = layer["ffn_norm"]

      attn_output = self_attention(logits, bias=mask, training=training)
      if training:
        attn_output = tf.nn.dropout(attn_output, rate=self.dropout_rate)
      logits = attention_norm(logits + attn_output)
      
      ffn_output = ffn(logits, training=training)
      if training:
        ffn_output = tf.nn.dropout(ffn_output, rate=self.dropout_rate)
      logits = ffn_norm(logits + ffn_output)
    
    return logits

