import tensorflow as tf
from common import dataset_utils

def confidence_loss_function_passthrough(y_true, y_pred):
  return y_pred

def confidence_truth(logits, tar):
  mask = tf.cast(tf.not_equal(tar, 0), dtype=tf.float32)

  y_pred = tf.argmax(logits, axis=-1)
  y_true = tf.cast(tf.equal(tar, y_pred), dtype=tf.float32) * mask
  return y_true, mask

def confidence_accuracy_function(y_true, y_pred, mask):
  y_true_positive = tf.cast(y_true, dtype=tf.bool)
  y_true_negative = tf.logical_and(tf.logical_not(y_true_positive), tf.cast(mask, dtype=tf.bool))

  positive_accuracy = tf.cast(tf.equal(y_true_positive, tf.greater_equal(tf.squeeze(y_pred), 0.5)), dtype=tf.float32)
  positive_accuracy *= tf.cast(y_true_positive, dtype=tf.float32)
  positive_accuracy = tf.reduce_sum(positive_accuracy) / tf.reduce_sum(tf.cast(y_true_positive, dtype=tf.float32))

  negative_accuracy = tf.cast(tf.equal(y_true_negative, tf.less(tf.squeeze(y_pred), 0.5)), dtype=tf.float32)
  negative_accuracy *= tf.cast(y_true_negative, dtype=tf.float32)
  negative_accuracy = tf.reduce_sum(negative_accuracy) / tf.reduce_sum(tf.cast(y_true_negative, dtype=tf.float32))

  return positive_accuracy, negative_accuracy

def confidence_loss_function(y_true, y_pred, mask):
  y_true = tf.expand_dims(y_true, axis=-1)
  loss = tf.keras.losses.binary_crossentropy(y_true, y_pred, 
      from_logits=True, label_smoothing=0.1)

  loss *= mask
  loss = tf.reduce_sum(loss) / tf.reduce_sum(mask)

  return loss

def loss_function(y_true, y_pred):
  xentropy = tf.keras.losses.sparse_categorical_crossentropy(
      y_true, y_pred, from_logits=True)

  mask = tf.cast(tf.not_equal(y_true, 0), dtype=tf.float32)
  xentropy *= mask
  loss = tf.reduce_sum(xentropy) / tf.reduce_sum(mask)

  return loss

def accuracy_function(y_true, y_pred):
  accuracy = tf.keras.metrics.sparse_categorical_accuracy(y_true, y_pred)

  mask = tf.cast(tf.not_equal(y_true, 0), dtype=tf.float32)
  accuracy *= mask
  accuracy = tf.reduce_sum(accuracy) / tf.reduce_sum(mask)

  return accuracy

class MaskedAccuracy(tf.keras.metrics.SparseCategoricalAccuracy):

  def __init__(self):
    super(MaskedAccuracy, self).__init__()

  def update_state(self, y_true, y_pred, sample_weight=None):
    sample_weight = tf.cast(tf.not_equal(y_true, 0), dtype=tf.float32)
    super(MaskedAccuracy, self).update_state(y_true, y_pred, sample_weight)