import os
import tensorflow as tf
import tensorflow_datasets as tfds
# import matplotlib.pyplot as plt
from unlabelled_transformer_v3 import unlabelled_transformer as ut
from unlabelled_transformer_v3 import data_pipeline, metrics, main
# from common import model_utils
from transformer_v1 import model_params, optimizer


language = "en"
params = model_params.BASE_PARAMS.copy()
params.update(
    language=language,
    model_name="ut_v3",
    model_dir=r"D:\models",
    data_dir=rf"D:\data\wiki_{language}_preprocessed_v2",
    vocab_path=r"D:\vocab\vocab_en_de_v3",
    with_val=False,
    single_batch_test=True,
    mask_percentage=0.1,
    drop_percentage=0.1,
    epochs=1,
    steps_per_epoch=100,
    verbose=1,
    test_batch_size=64,
    vocab_size=32517,
    batch_size=2,
    max_length=64,
    internal_d_model=256,
    d_model=512,
    num_dec_init_layers=3,
    num_layers=6,
    confidence_threshold=0.5,
    learning_rate=1e-5,
    learning_rate_modifier=1.0,
    warmup_steps=4000,
    optimizer_adam_beta1=0.9,
    optimizer_adam_beta2=0.98,
    optimizer_adam_epsilon=1e-09,
)

task = main.MainTask(params, resolver=None, epoch_callback=None)
# task.train()
task.eval()

# ds = data_pipeline.get_train_ds(params)
# for ex in ds.take(20):
#   print(ex)

# file_pattern = os.path.join(params["data_dir"] or "", f"*train*")

# ds = tf.data.Dataset.list_files(file_pattern, shuffle=True)

# options = tf.data.Options()
# options.experimental_deterministic = False
# ds = ds.interleave(
#     data_pipeline._load_records,
#     cycle_length=tf.data.experimental.AUTOTUNE,
#     num_parallel_calls=tf.data.experimental.AUTOTUNE).with_options(options)

# ds = ds.map(lambda x: tf.io.parse_tensor(x, tf.int64),
#     num_parallel_calls=tf.data.experimental.AUTOTUNE)

# # ds = ds.take(200)
# count = 0
# length = {
#   "32": 0,
#   "64": 0,
#   "128": 0,
#   "256": 0,
#   "++": 0
# }
# for ex in ds:
#   count = count + 1
#   l = ex.shape[0]
#   if l <= 32:
#     length["32"] = length["32"] + 1
#   elif l <= 64:
#     length["64"] = length["64"] + 1
#   elif l <= 128:
#     length["128"] = length["128"] + 1
#   elif l <= 256:
#     length["256"] = length["256"] + 1
#   else:
#     length["++"] = length["++"] + 1
#   if count % 100000 == 0:
#     print(count)
#     print(length)
# print(count)
# print(length)
