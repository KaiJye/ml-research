import tensorflow as tf
from unlabelled_transformer_v3 import metrics
from common import embedding_layer, attention_layer, ffn_layer, model_utils, dataset_utils

POS_ID = dataset_utils.POS_ID
MAS_ID = dataset_utils.MAS_ID

def create_predict_model(params):
  enc_inp = tf.keras.layers.Input((None,), dtype=tf.int64)
  tar = tf.keras.layers.Input((None,), dtype=tf.int64)
  length = tf.keras.layers.Input([], dtype=tf.int64)

  internal_model = Transformer(params)

  out = internal_model([enc_inp])

  model = tf.keras.Model([enc_inp, tar, length], [out, enc_inp, tar, length])

  return model

def create_training_model(params):
  enc_inp = tf.keras.layers.Input((None,), dtype=tf.int64)
  dec_inp = tf.keras.layers.Input((None,), dtype=tf.int64)
  dec_init_tar = tf.keras.layers.Input((None,), dtype=tf.int64)
  dec_tar = tf.keras.layers.Input((None,), dtype=tf.int64)
  length = tf.keras.layers.Input([], dtype=tf.int64)

  internal_model = Transformer(params)

  dec_init_logits, dec_logits, dec_confidence, pred_length = internal_model(
      [enc_inp, dec_inp, length])

  model = tf.keras.Model([enc_inp, dec_inp, dec_init_tar, dec_tar, length], [])

  # decoder initializer loss
  dec_init_loss = tf.keras.layers.Lambda(
      lambda x: metrics.loss_function(x[0], x[1]),
      name="dec_init_loss")([dec_init_tar, dec_init_logits])
  model.add_loss(dec_init_loss)

  # decoder initializer metric
  dec_init_accuracy = tf.keras.layers.Lambda(
      lambda x: metrics.accuracy_function(x[0], x[1]))([dec_init_tar, dec_init_logits])
  model.add_metric(dec_init_accuracy, aggregation="mean", name="dec_init_accuracy")

  # decoder loss
  dec_loss = tf.keras.layers.Lambda(
      lambda x: metrics.loss_function(x[0], x[1]),
      name="dec_loss")([dec_tar, dec_logits])
  model.add_loss(dec_loss)

  # decoder metric
  dec_accuracy = tf.keras.layers.Lambda(
      lambda x: metrics.accuracy_function(x[0], x[1]))([dec_tar, dec_logits])
  model.add_metric(dec_accuracy, aggregation="mean", name="dec_accuracy")

  # decoder confidence loss
  dec_conf_truth, dec_conf_mask = tf.keras.layers.Lambda(
      lambda x: metrics.confidence_truth(x[0], x[1])
      )([dec_logits, dec_tar])
  dec_conf_loss = tf.keras.layers.Lambda(
      lambda x: metrics.confidence_loss_function(x[0], x[1], x[2]),
      name="dec_conf_loss")([dec_conf_truth, dec_confidence, dec_conf_mask])
  model.add_loss(dec_conf_loss)

  # decoder confidence metric
  dec_conf_positive_accuracy, dec_conf_negative_accuracy = tf.keras.layers.Lambda(
      lambda x: metrics.confidence_accuracy_function(x[0], x[1], x[2])
      )([dec_conf_truth, dec_confidence, dec_conf_mask])
  model.add_metric(dec_conf_positive_accuracy, aggregation="mean", name="dec_conf_positive_accuracy")
  model.add_metric(dec_conf_negative_accuracy, aggregation="mean", name="dec_conf_negative_accuracy")

  # length loss
  length_loss = tf.keras.layers.Lambda(
      lambda x: tf.keras.losses.MAE(x[0], x[1]),
      name="length_loss")([length, pred_length])
  model.add_loss(length_loss)
  model.add_metric(length_loss, aggregation="mean", name="length_error")

  return model

class Transformer(tf.keras.Model):

  def __init__(self, params):
    super(Transformer, self).__init__()
    self.params = params

  def build(self, input_shape):
    params = self.params
    self.enc_inp_layernorm = tf.keras.layers.LayerNormalization(epsilon=1e-6)

    self.dec_inp_layernorm = tf.keras.layers.LayerNormalization(epsilon=1e-6)

    self.embedding = Embedding(params["d_model"], params["internal_d_model"], 
        params["vocab_size"], params["dropout_rate"], params["rped"])

    self.encoder = Encoder(params["num_layers"], params["d_model"],
        params["num_heads"], params["dff"], params["dropout_rate"])

    self.decoder = Decoder(params["num_layers"], params["d_model"],
        params["num_heads"], params["dff"], params["dropout_rate"])

    self.length_predictor = LengthPredictorLayer(params["d_model"], params["dropout_rate"])

    self.confidence_predictor = ConfidencePredictorLayer(params["d_model"],
        params["num_heads"], params["dropout_rate"])

    self.decoder_initializer = DecoderInitializerLayer(params["num_dec_init_layers"], 
        params["max_length"], params["d_model"], params["num_heads"], params["dff"], 
        params["dropout_rate"], params["static_batch"], params["rped"])

    super(Transformer, self).build(input_shape)

  def call(self, inputs, training=None):
    if len(inputs) == 3:
      enc_inp, dec_inp, length = inputs[0], inputs[1], inputs[2]
    else:
      enc_inp, dec_inp, length = inputs[0], None, None

    enc_padding_mask = model_utils.get_padding_bias(enc_inp)
    enc_pos_encoding = model_utils.gather_relative_positional_encoding(
        self.params["rped"], enc_inp, offset=1)
    logits = self.embedding(enc_inp, mode="embedding")

    logits += enc_pos_encoding
    if training:
      logits = tf.nn.dropout(logits, rate=self.params["dropout_rate"])
    logits = self.enc_inp_layernorm(logits)

    enc_output = self.encoder([logits, enc_padding_mask])

    pred_length = self.length_predictor(enc_output[:, 0, :])

    if dec_inp is None:
      # pred_length = tf.fill([tf.shape(enc_inp)[0]], 64)
      return self.predict(
          pred_length, enc_output, enc_padding_mask, training)
    else:
      dec_init_logits, dec_logits, dec_confidence = self.decode(
          dec_inp, length, enc_output, enc_padding_mask, training)
      return dec_init_logits, dec_logits, dec_confidence, pred_length

  def predict(self, length, enc_output, enc_padding_mask, training):
    dec_init, dec_padding_mask, dec_pos_encoding = self.decoder_initializer(
        [enc_output[:, 1:, :], length, enc_padding_mask[:, :, :, 1:]])
    
    dec_init = self.embedding(dec_init, mode="linear")

    dec_init = tf.argmax(dec_init, axis=-1)
    
    logits = self.embedding(dec_init, mode="embedding")

    logits = tf.reshape(logits, [tf.shape(enc_output)[0], -1, self.params["d_model"]])
    logits += dec_pos_encoding
    if training:
      logits = tf.nn.dropout(logits, rate=self.params["dropout_rate"])
    logits = self.dec_inp_layernorm(logits)

    logits = self.decoder([logits, enc_output[:, 1:, :], dec_padding_mask, enc_padding_mask[:, :, :, 1:]])

    logits, _ = self.confidence_filter(logits, mask=dec_padding_mask, 
        threshold=self.params["confidence_threshold"])

    logits = self.embedding(logits, mode="embedding")

    logits = tf.reshape(logits, [tf.shape(enc_output)[0], -1, self.params["d_model"]])
    logits += dec_pos_encoding
    if training:
      logits = tf.nn.dropout(logits, rate=self.params["dropout_rate"])
    logits = self.dec_inp_layernorm(logits)

    logits = self.decoder([logits, enc_output[:, 1:, :], dec_padding_mask, enc_padding_mask[:, :, :, 1:]])

    logits, _ = self.confidence_filter(logits, mask=dec_padding_mask, 
        threshold=self.params["confidence_threshold"])

    logits = self.embedding(logits, mode="embedding")

    logits = tf.reshape(logits, [tf.shape(enc_output)[0], -1, self.params["d_model"]])
    logits += dec_pos_encoding
    if training:
      logits = tf.nn.dropout(logits, rate=self.params["dropout_rate"])
    logits = self.dec_inp_layernorm(logits)

    logits = self.decoder([logits, enc_output[:, 1:, :], dec_padding_mask, enc_padding_mask[:, :, :, 1:]])

    logits, _ = self.confidence_filter(logits, mask=dec_padding_mask, 
        threshold=self.params["confidence_threshold"], filter=True)

    return logits
  
  def decode(self, dec_inp, length, enc_output, enc_padding_mask, training):
    dec_init, _, _ = self.decoder_initializer(
        [enc_output[:, 1:, :], length, enc_padding_mask[:, :, :, 1:]])

    dec_init_logits = self.embedding(dec_init, mode="linear")

    dec_padding_mask = model_utils.get_padding_bias(dec_inp)
    dec_pos_encoding = model_utils.gather_relative_positional_encoding(
        self.params["rped"], dec_inp)

    logits = self.embedding(dec_inp, mode="embedding")

    logits += dec_pos_encoding
    if training:
      logits = tf.nn.dropout(logits, rate=self.params["dropout_rate"])
    logits = self.dec_inp_layernorm(logits)

    logits = self.decoder([logits, enc_output[:, 1:, :], dec_padding_mask, enc_padding_mask[:, :, :, 1:]])

    dec_confidence = self.confidence_predictor([logits, dec_padding_mask])

    dec_logits = self.embedding(logits, mode="linear")

    return dec_init_logits, dec_logits, dec_confidence

  def confidence_filter(self, logits, mask=None, threshold=0.5, filter=True):
    confidence = self.confidence_predictor([logits, mask])
    confidence = tf.greater_equal(tf.squeeze(confidence), threshold)

    filtered = self.embedding(logits, mode="linear")
    filtered = tf.argmax(filtered, axis=-1)
    if filter:
      filtered = tf.where(confidence, filtered, tf.constant(MAS_ID, dtype=tf.int64))
    return filtered, confidence

class Embedding(tf.keras.layers.Layer):
  
  def __init__(self, d_model, internal_d_model, vocab_size, dropout_rate, rped):
    super(Embedding, self).__init__()
    self.d_model = d_model
    self.internal_d_model = internal_d_model
    self.vocab_size = vocab_size
    self.dropout_rate = dropout_rate
    self.rped = rped

  def build(self, input_shape):
    self.embedding_softmax_layer = embedding_layer.EmbeddingSharedWeights(
        self.vocab_size, self.internal_d_model)
    self.weight0 = self.add_weight(name="weight0", 
        shape=[self.internal_d_model, self.d_model])
    super(Embedding, self).build(input_shape)

  def call(self, x, mode="embedding", training=None):
    if mode == "embedding":
      x = self.embedding_softmax_layer(x, mode=mode)
      x = tf.matmul(x, self.weight0)
      return x
    elif mode == "linear":
      x = tf.matmul(x, self.weight0, transpose_b=True)
      x = self.embedding_softmax_layer(x, mode=mode)
      return x

class ConfidencePredictorLayer(tf.keras.layers.Layer):
  def __init__(self, d_model, num_heads, dropout_rate):
    super(ConfidencePredictorLayer, self).__init__()
    self.d_model = d_model
    self.num_heads = num_heads
    self.dropout_rate = dropout_rate

  def build(self, input_shape):
    self.self_attention = attention_layer.SelfAttention(self.d_model, self.num_heads, self.dropout_rate)
    self.layernorm = tf.keras.layers.LayerNormalization(epsilon=1e-6)

    self.dense_0 = tf.keras.layers.Dense(self.d_model, activation=tf.nn.relu)
    self.dense_1 = tf.keras.layers.Dense(1, activation=None)
    super(ConfidencePredictorLayer, self).build(input_shape)

  def call(self, inputs, training=None):
    x, attention_mask = inputs[0], inputs[1]

    logits = self.self_attention(x, attention_mask, training=training)
    if training:
      logits = tf.nn.dropout(logits, rate=self.dropout_rate)
    logits = self.layernorm(x + logits)

    logits = self.dense_0(logits)
    if training:
      logits = tf.nn.dropout(logits, rate=self.dropout_rate)
    logits = self.dense_1(logits)
    return logits

class DecoderInitializerLayer(tf.keras.layers.Layer):
  def __init__(self, num_dec_init_layers, max_length, d_model,
      num_heads, dff, dropout_rate, static_batch, rped):
    super(DecoderInitializerLayer, self).__init__()
    self.num_dec_init_layers = num_dec_init_layers
    self.max_length = max_length
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate
    self.static_batch = static_batch
    self.rped = rped

  def build(self, input_shape):
    self.internal_layers = [{
      "position_attention": attention_layer.Attention(self.d_model, self.num_heads, self.dropout_rate),
      "cross_attention": attention_layer.Attention(self.d_model, self.num_heads, self.dropout_rate),
      "attention_norm": tf.keras.layers.LayerNormalization(epsilon=1e-6),
      "ffn": ffn_layer.FeedForwardNetwork(self.d_model, self.dff, self.dropout_rate),
      "ffn_norm": tf.keras.layers.LayerNormalization(epsilon=1e-6)
    } for _ in range(self.num_dec_init_layers)]
    super(DecoderInitializerLayer, self).build(input_shape)
  
  def call(self, inputs, training=None):
    encoder_output, length, attention_mask = inputs[0], tf.cast(inputs[1], dtype=tf.int32), inputs[2]

    length_mask = tf.sequence_mask(length, maxlen=self.max_length, dtype=tf.int64)
    length = tf.clip_by_value(length, 0, self.max_length)
    pos_encoding = tf.gather(self.rped, length)

    padding_mask = model_utils.get_padding_bias(length_mask)

    logits = pos_encoding

    composite_mask = tf.minimum(tf.transpose(padding_mask, perm=[0, 1, 3, 2]), attention_mask)

    for layer in self.internal_layers:
      position_attention = layer["position_attention"]
      cross_attention = layer["cross_attention"]
      attention_norm = layer["attention_norm"]
      ffn = layer["ffn"]
      ffn_norm = layer["ffn_norm"]

      pos_out = position_attention(logits, pos_encoding, 
          padding_mask, training=training)
      if training:
        pos_out = tf.nn.dropout(pos_out, rate=self.dropout_rate)
      cross_out = cross_attention(logits, encoder_output, composite_mask, 
          training=training)
      if training:
        cross_out = tf.nn.dropout(cross_out, rate=self.dropout_rate)
      logits = attention_norm(pos_out + cross_out + logits)

      ffn_out = ffn(logits, training=training)
      if training:
        ffn_out = tf.nn.dropout(ffn_out, rate=self.dropout_rate)
      logits = ffn_norm(ffn_out + logits)

    return logits, padding_mask, pos_encoding

class LengthPredictorLayer(tf.keras.layers.Layer):
  def __init__(self, d_model, dropout_rate):
    super(LengthPredictorLayer, self).__init__()
    self.d_model = d_model
    self.dropout_rate = dropout_rate

  def build(self, input_shape):
    self.dense_0 = tf.keras.layers.Dense(self.d_model, activation=tf.nn.relu)
    self.dense_1 = tf.keras.layers.Dense(1, activation=None)
    self.relu = tf.keras.layers.ReLU()
    super(LengthPredictorLayer, self).build(input_shape)
  
  def call(self, encoder_output_length, training=None):
    out = self.dense_0(encoder_output_length)
    if training:
      out = tf.nn.dropout(out, rate=self.dropout_rate)
    out = self.dense_1(out)
    out = self.relu(out)
    out = tf.reshape(out, [-1])
    return out

class Encoder(tf.keras.layers.Layer):
  def __init__(self, num_layers, d_model, num_heads, dff, dropout_rate):
    super(Encoder, self).__init__()
    self.num_layers = num_layers
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate

  def build(self, input_shape):
    self.internal_layers = [{
      "self_attention": attention_layer.SelfAttention(self.d_model, self.num_heads, self.dropout_rate),
      "attention_norm": tf.keras.layers.LayerNormalization(epsilon=1e-6),
      "ffn": ffn_layer.FeedForwardNetwork(self.d_model, self.dff, self.dropout_rate),
      "ffn_norm": tf.keras.layers.LayerNormalization(epsilon=1e-6)
    } for _ in range(self.num_layers)]
    super(Encoder, self).build(input_shape)

  def call(self, inputs, training=None):
    logits, attention_mask = inputs[0], inputs[1]
    
    for layer in self.internal_layers:
      self_attention = layer["self_attention"]
      attention_norm = layer["attention_norm"]
      ffn = layer["ffn"]
      ffn_norm = layer["ffn_norm"]

      attn_output = self_attention(logits, attention_mask, training=training)
      if training:
        attn_output = tf.nn.dropout(attn_output, rate=self.dropout_rate)
      logits = attention_norm(logits + attn_output)
      
      ffn_output = ffn(logits, training=training)
      if training:
        ffn_output = tf.nn.dropout(ffn_output, rate=self.dropout_rate)
      logits = ffn_norm(logits + ffn_output)
    
    return logits

class Decoder(tf.keras.layers.Layer):
  def __init__(self, num_layers, d_model, num_heads, dff, dropout_rate):
    super(Decoder, self).__init__()
    self.num_layers = num_layers
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate

  def build(self, input_shape):
    self.internal_layers = [{
      "self_attention": attention_layer.SelfAttention(self.d_model, self.num_heads, self.dropout_rate),
      "self_norm": tf.keras.layers.LayerNormalization(epsilon=1e-6),
      "cross_attention": attention_layer.Attention(self.d_model, self.num_heads, self.dropout_rate),
      "cross_norm": tf.keras.layers.LayerNormalization(epsilon=1e-6),
      "ffn": ffn_layer.FeedForwardNetwork(self.d_model, self.dff, self.dropout_rate),
      "ffn_norm": tf.keras.layers.LayerNormalization(epsilon=1e-6)
    } for _ in range(self.num_layers)]
    super(Decoder, self).build(input_shape)

  def call(self, inputs, training=None):
    logits, enc_output, padding_mask, attention_mask = inputs[0], inputs[1], inputs[2], inputs[3]

    composite_mask = tf.minimum(tf.transpose(padding_mask, perm=[0, 1, 3, 2]), attention_mask)

    for layer in self.internal_layers:
      self_attention = layer["self_attention"]
      self_norm = layer["self_norm"]
      cross_attention = layer["cross_attention"]
      cross_norm = layer["cross_norm"]
      ffn = layer["ffn"]
      ffn_norm = layer["ffn_norm"]
      
      logits = tf.reshape(logits, [tf.shape(enc_output)[0], -1, self.d_model])
      attn_output = self_attention(logits, padding_mask, training=training)
      if training:
        attn_output = tf.nn.dropout(attn_output, rate=self.dropout_rate)
      logits = self_norm(logits + attn_output)

      cross_output = cross_attention(logits, enc_output, 
          composite_mask, 
          training=training)
      if training:
        cross_output = tf.nn.dropout(cross_output, rate=self.dropout_rate)
      logits = cross_norm(logits + cross_output)
      
      ffn_output = ffn(logits, training=training)
      if training:
        ffn_output = tf.nn.dropout(ffn_output, rate=self.dropout_rate)
      logits = ffn_norm(logits + ffn_output)
    
    return logits
