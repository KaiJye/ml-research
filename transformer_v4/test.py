import importlib
import tensorflow as tf
import tensorflow_datasets as tfds
from common import embedding_layer, attention_layer, ffn_layer, dataset_utils
from common import model_utils
from common import beam_search
from transformer_v1 import data_pipeline, model_params
import re

a = tf.constant([[[1,1,1], [2,2,2]], [[3,3,3], [4,4,4]], [[5,5,5], [6,6,6]]], dtype=tf.float32)
b = tf.constant([[[7,7,7], [8,8,8]], [[9,9,9], [10,10,10]], [[11,11,11], [12,12,12]]], dtype=tf.float32)

s = []
s += [a]
s += [b]
s = tf.stack(s, axis=-1)

l0 = tf.keras.layers.Dense(2)
l1 = tf.keras.layers.Dense(1)

o = l0(s)
print(o)
o = l1(o)
print(o)
o = tf.reshape(o, tf.shape(o)[:-1])
