import tensorflow as tf
from common import embedding_layer, attention_layer, ffn_layer
from common import dataset_utils, model_utils, beam_search
from transformer_v1 import metrics

SOS_ID = dataset_utils.SOS_ID
EOS_ID = dataset_utils.EOS_ID

def create_training_model(params):
  inputs = tf.keras.layers.Input((None,), dtype=tf.int64)
  targets = tf.keras.layers.Input((None,), dtype=tf.int64)
  internal_model = Transformer(params, mode="standard_decode")
  logits = internal_model([inputs, targets])
  model = tf.keras.Model([inputs, targets], logits)
  return model

def create_eval_model(params):
  inputs = tf.keras.layers.Input((None,), dtype=tf.int64)
  internal_model = Transformer(params, mode="standard_predict")
  logits = internal_model([inputs])
  model = tf.keras.Model([inputs], logits)
  return model

class EncoderLayer(tf.keras.layers.Layer):
  def __init__(self, params):
    super(EncoderLayer, self).__init__()
    self.params = params

  def build(self, input_shape):
    self.self_attention = attention_layer.SelfAttention(self.params['d_model'], self.params['num_heads'], self.params['dropout_rate'])
    self.ffn = ffn_layer.FeedForwardNetwork(self.params['d_model'], self.params['dff'], self.params['dropout_rate'])

    self.layernorm1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.layernorm2 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    
    self.dropout1 = tf.keras.layers.Dropout(self.params['dropout_rate'])
    self.dropout2 = tf.keras.layers.Dropout(self.params['dropout_rate'])
    super(EncoderLayer, self).build(input_shape)
  
  def call(self, x, attention_mask, training):
    attn_output = self.self_attention(x, attention_mask, training=training)  # (batch_size, input_seq_len, d_model)
    attn_output = self.dropout1(attn_output, training=training)
    out1 = self.layernorm1(x + attn_output)  # (batch_size, input_seq_len, d_model)
    
    ffn_output = self.ffn(out1, training=training)  # (batch_size, input_seq_len, d_model)
    ffn_output = self.dropout2(ffn_output, training=training)
    out2 = self.layernorm2(out1 + ffn_output)  # (batch_size, input_seq_len, d_model)
    
    return out2

class DecoderLayer(tf.keras.layers.Layer):
  def __init__(self, params):
    super(DecoderLayer, self).__init__()
    self.params = params
    
  def build(self, input_shape):
    self.self_attention = attention_layer.SelfAttention(self.params['d_model'], self.params['num_heads'], self.params['dropout_rate'])
    self.attention = attention_layer.Attention(self.params['d_model'], self.params['num_heads'], self.params['dropout_rate'])
    self.ffn = ffn_layer.FeedForwardNetwork(self.params['d_model'], self.params['dff'], self.params['dropout_rate'])
    self.encoder_selection = tf.keras.layers.Dense(self.params['num_layers'], activation=tf.nn.relu)
    self.encoder_reducer = tf.keras.layers.Dense(1)

    self.layernorm1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.layernorm2 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.layernorm3 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    
    self.dropout1 = tf.keras.layers.Dropout(self.params['dropout_rate'])
    self.dropout2 = tf.keras.layers.Dropout(self.params['dropout_rate'])
    self.dropout3 = tf.keras.layers.Dropout(self.params['dropout_rate'])
    super(DecoderLayer, self).build(input_shape)

  def call(self, x, enc_output, look_ahead_mask, attention_mask, training):
    # enc_output.shape == (batch_size, input_seq_len, d_model)
    attn1 = self.self_attention(x, look_ahead_mask, training=training)  # (batch_size, target_seq_len, d_model)
    attn1 = self.dropout1(attn1, training=training)
    out1 = self.layernorm1(attn1 + x)
    
    enc_output = self.encoder_selection(enc_output)
    if training:
      tf.nn.dropout(enc_output, rate=self.params['dropout_rate'])
    enc_output = self.encoder_reducer(enc_output)
    enc_output = tf.reshape(enc_output, tf.shape(enc_output)[:3])
    attn2 = self.attention(out1, enc_output, attention_mask, training=training)  # (batch_size, target_seq_len, d_model)
    attn2 = self.dropout2(attn2, training=training)
    out2 = self.layernorm2(attn2 + out1)  # (batch_size, target_seq_len, d_model)
    
    ffn_output = self.ffn(out2, training=training)  # (batch_size, target_seq_len, d_model)
    ffn_output = self.dropout3(ffn_output, training=training)
    out3 = self.layernorm3(ffn_output + out2)  # (batch_size, target_seq_len, d_model)
    
    return out3

class Encoder(tf.keras.layers.Layer):
  def __init__(self, params):
    super(Encoder, self).__init__()
    self.params = params
  
  def build(self, input_shape):
    self.enc_layers = [EncoderLayer(self.params) for _ in range(self.params['num_layers'])]
    self.dropout = tf.keras.layers.Dropout(self.params['dropout_rate'])
    self.layernorm = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    super(Encoder, self).build(input_shape)

  def call(self, x, attention_mask, training):
    seq_len = tf.shape(x)[1]
    
    pos_encoding = model_utils.get_position_encoding(
        seq_len,
        self.params['d_model'])
    x += pos_encoding

    x = self.dropout(x, training=training)
    x = self.layernorm(x)

    enc_output_stack = []
    for i in range(self.params['num_layers']):
      x = self.enc_layers[i](x, attention_mask, training)
      enc_output_stack += [x]
    
    return tf.stack(enc_output_stack, axis=-1)

class Decoder(tf.keras.layers.Layer):
  def __init__(self, params):
    super(Decoder, self).__init__()
    self.params = params

  def build(self, input_shape):
    self.dec_layers = [DecoderLayer(self.params) for _ in range(self.params['num_layers'])]
    self.dropout = tf.keras.layers.Dropout(self.params['dropout_rate'])
    self.layernorm = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    super(Decoder, self).build(input_shape)

  def call(self, x, enc_output, padding_mask, attention_mask, training):
    seq_len = tf.shape(x)[1]

    look_ahead_mask = model_utils.get_decoder_self_attention_bias(seq_len)
    self_attention_mask = tf.minimum(padding_mask, look_ahead_mask)

    pos_encoding = model_utils.get_position_encoding(
        seq_len,
        self.params['d_model'])
    x += pos_encoding
    
    x = self.dropout(x, training=training)
    x = self.layernorm(x)

    for i in range(self.params['num_layers']):
      x = self.dec_layers[i](x, enc_output, self_attention_mask, attention_mask, training)
    
    return x

class Transformer(tf.keras.Model):
  def __init__(self, params, mode):
    super(Transformer, self).__init__()
    self.mode = mode
    self.params = params
    if self.mode == "standard_predict":
      self.bs = beam_search.BeamSearch(params["vocab_size"], params["beam_size"], params["alpha"],
          params["extra_length"], SOS_ID, EOS_ID)

  def build(self, input_shape):
    self.embedding_softmax_layer = embedding_layer.EmbeddingSharedWeights(
        self.params["vocab_size"], self.params["d_model"])

    self.encoder = Encoder(self.params)

    self.decoder = Decoder(self.params)

    super(Transformer, self).build(input_shape)
    
  def call(self, inputs, training=None):
    if self.mode == "standard_decode":
      inp, tar = inputs[0], inputs[1]
    elif self.mode == "standard_predict":
      inp, tar = inputs[0], None

    attention_mask = model_utils.get_padding_bias(inp)

    inp = self.embedding_softmax_layer(inp)  # (batch_size, target_seq_len, d_model)

    enc_output = self.encoder(inp, attention_mask, training=training)  # (batch_size, inp_seq_len, d_model)
    
    if self.mode == "standard_decode":
      return self.decode(tar, enc_output, attention_mask, training=training)
    elif self.mode == "standard_predict":
      return self.predict_2(enc_output, attention_mask, training=training)

  def _get_symbols_to_logits_fn(self, enc_output, attention_mask, training):

    def symbols_to_logits_fn(tar):
      padding_mask = model_utils.get_padding_bias(tar)
      decoder_inputs = self.embedding_softmax_layer(tar)  # (batch_size, length, d_model)
      
      # dec_output.shape == (batch_size, tar_seq_len, d_model)
      dec_output = self.decoder(
          decoder_inputs, enc_output, padding_mask, attention_mask, training=training)
      
      output = self.embedding_softmax_layer(dec_output, mode="linear")
      return output

    return symbols_to_logits_fn

  def predict_2(self, enc_output, attention_mask, training):
    enc_output_shape = enc_output.shape
    batch_size = enc_output_shape[0] if enc_output_shape[0] else 1
    input_length = enc_output_shape[1] if enc_output_shape[1] else 1
    max_decode_length = input_length + self.params["extra_length"]

    enc_output = tf.expand_dims(enc_output, axis=1)
    enc_output = tf.repeat(enc_output, repeats=self.params["beam_size"], axis=1)
    enc_output = tf.reshape(enc_output, [-1, input_length, self.params["d_model"], self.params["num_layers"]])

    attention_mask = tf.expand_dims(attention_mask, axis=1)
    attention_mask = tf.repeat(attention_mask, repeats=self.params["beam_size"], axis=1)
    attention_mask = tf.reshape(attention_mask, [-1, 1, 1, input_length])

    symbols_to_logits_fn = self._get_symbols_to_logits_fn(enc_output, attention_mask, training)
    seqs, scores = self.bs.search(symbols_to_logits_fn, batch_size, max_decode_length)
    return seqs[:, 0, :]
    
  def predict(self, enc_output, attention_mask, training):
    tar = tf.ones([tf.shape(enc_output)[0], 1], tf.int64) * SOS_ID

    def _condition(tar):
      output = tar
      isEos = tf.equal(output, EOS_ID)
      eosPos = tf.where(isEos)  # get all EOS positions
      batchWithEos = tf.gather(eosPos, 0, axis=1) # gather batch id
      p, _ = tf.unique(batchWithEos)  # remove duplicate
      not_all_eos = tf.not_equal(tf.shape(p)[0], tf.shape(output)[0])
      not_max_length = tf.not_equal(tf.shape(output)[1], tf.constant(self.params["max_length"]))
      return tf.logical_and(not_all_eos, not_max_length)

    def _body(tar):
      padding_mask = model_utils.get_padding_bias(tar)
      decoder_inputs = self.embedding_softmax_layer(tar)

      # dec_output.shape == (batch_size, tar_seq_len, d_model)
      dec_output = self.decoder(
          decoder_inputs, enc_output, padding_mask, attention_mask, training=training)
      
      # [batch_size, length, vocab_size]
      output = self.embedding_softmax_layer(dec_output, mode="linear")
      last = tf.cast(tf.argmax(output[:, -1:, :], axis=-1), tf.int64)
      tar = tf.concat([tar, last], axis=-1)
      
      return (tar,)
    
    (ret,) = tf.nest.map_structure(tf.stop_gradient, 
        tf.while_loop(_condition, _body, 
            loop_vars=(tar,),
            shape_invariants=(tf.TensorShape([tar.shape[0], None]),)))

    return ret

  def decode(self, tar, enc_output, attention_mask, training):
    padding_mask = model_utils.get_padding_bias(tar)
    decoder_inputs = self.embedding_softmax_layer(tar)  # (batch_size, length, d_model)
    
    # shift right
    # decoder_inputs = tf.pad(decoder_inputs,
    #     [[0, 0], [1, 0], [0, 0]])[:, :-1, :]

    # dec_output.shape == (batch_size, tar_seq_len, d_model)
    dec_output = self.decoder(
        decoder_inputs, enc_output, padding_mask, attention_mask, training=training)
    
    output = self.embedding_softmax_layer(dec_output, mode="linear")
    return output
