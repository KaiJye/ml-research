import tensorflow as tf
import numpy as np
from ecg_data import data_pipeline as test_dp
from ecg_v3 import biometric_model, data_pipeline

class MainTask:
  def __init__(self, params, opt=None, resolver=None):
    self.params = params
    self.opt = opt

    if resolver:
      self.distribution_strategy = tf.distribute.experimental.TPUStrategy(resolver)
      self.params['static_batch'] = True
    else:
      self.distribution_strategy = tf.distribute.MirroredStrategy()
      self.params['static_batch'] = False

  def train(self, steps_per_epoch=512, repeat=True):
    params = self.params

    with self.distribution_strategy.scope():
      model, _ = biometric_model.create_training_model(params)
      if self.opt is None:
        opt = tf.keras.optimizers.Adam(params['learning_rate'],
            beta_1=params['optimizer_adam_beta1'],
            beta_2=params['optimizer_adam_beta2'], 
            epsilon=params['optimizer_adam_epsilon'])
      else:
        opt = self.opt

      loss = tf.keras.losses.BinaryCrossentropy(from_logits=True, label_smoothing=params['label_smoothing'])
      # auc_roc = tf.keras.metrics.AUC(curve='ROC', num_thresholds=params['num_thresholds'], name='auc_roc')
      # auc_pr = tf.keras.metrics.AUC(curve='PR', name='auc_pr')
      binary_accuracy = tf.keras.metrics.BinaryAccuracy(name='ba')
      binary_crossentropy = tf.keras.metrics.BinaryCrossentropy(
          from_logits=False, label_smoothing=params['label_smoothing'], name='bc'
      )
      # true_positive = tf.keras.metrics.TruePositives(thresholds=params['thresholds'])
      # true_negative = tf.keras.metrics.TrueNegatives(thresholds=params['thresholds'])
      # false_positive = tf.keras.metrics.FalsePositives(thresholds=params['thresholds'])
      # false_negative = tf.keras.metrics.FalseNegatives(thresholds=params['thresholds'])
      model.compile(optimizer=opt, loss=loss, metrics=[
        binary_crossentropy, binary_accuracy
      ])

    model.summary()

    callbacks = self._create_callbacks()

    train_ds: tf.data.Dataset = data_pipeline.get_ds(params, 'train')
    train_ds = train_ds.cache()
    if steps_per_epoch and repeat:
      train_ds = train_ds.repeat()
      train_ds = train_ds.shuffle(steps_per_epoch)
    elif steps_per_epoch and not repeat:
      train_ds = train_ds.take(steps_per_epoch)
      train_ds = train_ds.repeat()
      train_ds = train_ds.shuffle(steps_per_epoch)
    else:
      train_ds = train_ds.shuffle(1024)

    val_ds = data_pipeline.get_ds(params, 'val')
    val_ds = val_ds.cache()

    model.fit(train_ds,
        validation_data=val_ds,
        epochs=params['epochs'],
        steps_per_epoch=steps_per_epoch,
        callbacks=callbacks,
        verbose=params['verbose'])

    model.save_weights(self.params['model_dir'])

    return model

  def _create_callbacks(self):
    callbacks = []

    # if self.params['model_checkpoint']:
    #   ckpt_callback = tf.keras.callbacks.ModelCheckpoint(
    #       self.params['model_dir'], 
    #       save_best_only=True, save_weights_only=True,
    #       monitor='val_bc', mode='min')
    #   callbacks.append(ckpt_callback)

    def scheduler(epoch, lr):
      return 0.000012 * tf.math.exp(2 - (0.03 * epoch)) + 0.00004
      # if epoch == 0:
      #   return self.params['learning_rate'] * 2.0
      # elif epoch < 10:
      #   return self.params['learning_rate']
      # elif epoch < 20:
      #   return self.params['learning_rate'] * 0.5
      # else:
      #   return self.params['learning_rate'] * 0.25
    lr_callback = tf.keras.callbacks.LearningRateScheduler(scheduler)
    callbacks.append(lr_callback)
    
    early_stop_callback = tf.keras.callbacks.EarlyStopping(
      monitor='val_bc', min_delta=0, patience=self.params['patience'], verbose=0, mode='min',
      baseline=None, restore_best_weights=True
    )
    callbacks.append(early_stop_callback)
    
    return callbacks

  def eval(self, split, left, right, model=None):
    params = self.params

    with self.distribution_strategy.scope():
      if model is None:
        model, _ = biometric_model.create_training_model(params)
        model.load_weights(params['model_dir'])

      binary_accuracy = tf.keras.metrics.BinaryAccuracy(name='ba')
      auc_roc = tf.keras.metrics.AUC(num_thresholds=params['num_thresholds'], curve='ROC', name='ar')
      auc_pr = tf.keras.metrics.AUC(num_thresholds=params['num_thresholds'], curve='PR', name='ap')
      binary_crossentropy = tf.keras.metrics.BinaryCrossentropy(
          from_logits=False, label_smoothing=params['label_smoothing'], name='bc'
      )
      model.compile(loss='binary_crossentropy', metrics=[
        auc_roc, auc_pr, binary_accuracy, binary_crossentropy
      ])

    model.summary()

    model.reset_metrics()

    test_ds = data_pipeline.get_ds(params, split)
    model.evaluate(test_ds, verbose=params['verbose'])
    
    model.reset_metrics()

    test_ds = data_pipeline.get_ds(params, split)

    y_true = tf.constant([], tf.int64)
    y_pred_logits = tf.constant([])
    for ex in test_ds:
      predicted = model.predict_on_batch(ex[0])
      y_true = tf.concat([y_true, ex[1]], -1)
      y_pred_logits = tf.concat([y_pred_logits, predicted], -1)

    tp = tf.constant([])
    tn = tf.constant([])
    fp = tf.constant([])
    fn = tf.constant([])
    tpm = tf.keras.metrics.TruePositives()
    tnm = tf.keras.metrics.TrueNegatives()
    fpm = tf.keras.metrics.FalsePositives()
    fnm = tf.keras.metrics.FalseNegatives()
    for threshold in np.linspace(0, 1, params['num_thresholds']):
      y_pred = tf.greater(y_pred_logits, threshold)

      tpm.update_state(y_true, y_pred)
      tnm.update_state(y_true, y_pred)
      fpm.update_state(y_true, y_pred)
      fnm.update_state(y_true, y_pred)
      
      tp = tf.concat([tp, tf.expand_dims(tpm.result(), 0)], -1)
      tn = tf.concat([tn, tf.expand_dims(tnm.result(), 0)], -1)
      fp = tf.concat([fp, tf.expand_dims(fpm.result(), 0)], -1)
      fn = tf.concat([fn, tf.expand_dims(fnm.result(), 0)], -1)

      tpm.reset_states()
      tnm.reset_states()
      fpm.reset_states()
      fnm.reset_states()
    
    return tp, tn, fp, fn
