import tensorflow as tf
import math
from common import attention_layer, ffn_layer, model_utils


def create_training_model(params):
  d0 = tf.keras.layers.Input([None], dtype=tf.float32)
  d1 = tf.keras.layers.Input([None, None], dtype=tf.float32)
  internal_model = CustomModel(params)
  logits = internal_model([d0, d1])
  model = tf.keras.Model([d0, d1], logits)
  return model, internal_model


class CustomModel(tf.keras.Model):

  def __init__(self, params):
    super(CustomModel, self).__init__()
    self.params = params
    

  def build(self, input_shape):
    params = self.params

    self.left_conv = ConvLayer(params['num_conv_layers'], params['filter_size'], 
        params['kernel_size'], params['conv_strides'], params['pool_size'], params['dropout_rate'])

    self.left_encoder = EcgEncoder(params["num_encoder_layers"], params["d_model"],
        params["num_heads"], params["dff"], params["dropout_rate"])

    self.right_conv = ConvLayer(params['num_conv_layers'], params['filter_size'], 
        params['kernel_size'], params['conv_strides'], params['pool_size'], params['dropout_rate'])

    self.right_encoder = EcgEncoder(params["num_encoder_layers"], params["d_model"],
        params["num_heads"], params["dff"], params["dropout_rate"])

    self.id_encoder = IdEncoder(params["num_encoder_layers"], params["d_model"],
        params["num_heads"], params["dff"], params["dropout_rate"])

    self.ffn_1 = tf.keras.layers.Dense(params['d_model'] / 2, activation=tf.nn.relu)
    self.final_norm_1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.final = tf.keras.layers.Dense(1)

    super(CustomModel, self).build(input_shape)

  def call(self, inputs, training=None):
    batch_size = tf.shape(inputs[0])[0]
#     num_left = tf.shape(inputs[0])[1]
    num_right = tf.shape(inputs[1])[1]
    d_model = self.params['d_model']
    
    left = inputs[0]
    left_conv_out, left_conv_mask = self.left_conv(left)
    left_conv_mask = tf.reshape(left_conv_mask, [batch_size, -1])
    left_logits = self.left_encoder([left_conv_out, left_conv_mask], training)
    
    right = tf.reshape(inputs[1], [batch_size*num_right, -1])
    right_conv_out, right_conv_mask = self.right_conv(right)
    right_conv_mask = tf.reshape(right_conv_mask, [batch_size, -1])
    right_logits = self.right_encoder([right_conv_out, right_conv_mask], training)
    right_logits = tf.reshape(right_logits, [batch_size, num_right, d_model])
    
    id_logits = tf.concat([left_logits, right_logits], 1)
    logits = self.id_encoder([id_logits, None], training)
    
    logits = self.ffn_1(logits)
    if training:
      logits = tf.nn.dropout(logits, rate=self.params["dropout_rate"])
    logits = self.final_norm_1(logits)
    logits = self.final(logits)
    
    if training:
      logits = tf.reshape(logits, [batch_size, -1])
    else:
      logits = tf.reshape(logits, [batch_size, -1])
      logits = tf.nn.softmax(logits)
    
    return logits
    
class ConvLayer(tf.keras.layers.Layer):
  def __init__(self, num_conv_layers, filter_size, kernel_size, conv_strides, pool_size, dropout_rate):
    super(ConvLayer, self).__init__()
    self.num_conv_layers = num_conv_layers
    self.filter_size = filter_size
    self.kernel_size = kernel_size
    self.conv_strides = conv_strides
    self.pool_size = pool_size
    self.dropout_rate = dropout_rate

  def build(self, input_shape):    
    self.logit_pool = tf.keras.layers.MaxPooling1D(pool_size=self.pool_size,
        strides=None, padding='valid')
    self.mask_conv = tf.keras.layers.Conv1D(1, self.kernel_size,
        padding='valid', strides=self.conv_strides,
        kernel_initializer=tf.keras.initializers.Constant(1), use_bias=False,
        trainable=False)
    
    self.internal_layers = [{
      "conv": tf.keras.layers.Conv1D(self.filter_size * pow(2, i), self.kernel_size,
          padding='valid', strides=self.conv_strides,
          kernel_initializer='glorot_uniform', use_bias=True, 
          kernel_regularizer='l1_l2', bias_regularizer='l1_l2'),
      "norm": tf.keras.layers.LayerNormalization(epsilon=1e-6),
    } for i in range(self.num_conv_layers)]
    super(ConvLayer, self).build(input_shape)

  def call(self, inputs, training=None):
    logits = inputs[:, :, tf.newaxis]

    mask = tf.cast(tf.less(logits, -1e7), tf.float32)
    
    for i, layer in enumerate(self.internal_layers):
      conv = layer["conv"]
      norm = layer["norm"]

      internal_output = conv(logits)
      if i == self.num_conv_layers - 1:
        internal_output = tf.nn.relu(internal_output)
      else:
        internal_output = tf.nn.relu(internal_output)

      if training:
        internal_output = tf.nn.dropout(internal_output, rate=self.dropout_rate)
      # logits = norm(logits[:, ::self.conv_strides, :] + internal_output)
      logits = norm(internal_output)

      mask = self.mask_conv(mask)
      mask = tf.cast(tf.not_equal(mask, 0), tf.float32)

      # d_length = tf.cast(tf.shape(logits)[2] - tf.shape(mask)[2], tf.float32)
      # lower_kernel_reduce = tf.cast(tf.math.floor(d_length / 2), tf.int32)
      # upper_kernel_reduce = tf.cast(tf.math.ceil(d_length / 2), tf.int32)
      # logits = logits[:, lower_kernel_reduce:-1*upper_kernel_reduce, :]
    
      logits = self.logit_pool(logits)

      mask = self.logit_pool(mask)
    mask = tf.cast(tf.not_equal(mask, 0), tf.int64)

    return logits, mask


class EcgEncoder(tf.keras.layers.Layer):
  def __init__(self, num_layers, d_model, num_heads, dff, dropout_rate):
    super(EcgEncoder, self).__init__()
    self.num_layers = num_layers
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate
    
    self.pos = model_utils.positional_encoding(512, d_model)

  def build(self, input_shape):
    self.clz = self.add_weight(
        "clz",
        shape=[self.d_model],
        initializer='glorot_uniform')

    self.internal_layers = [{
        "self_attention": attention_layer.SelfAttention(self.d_model, self.num_heads, self.dropout_rate),
        "attention_norm": tf.keras.layers.LayerNormalization(epsilon=1e-6),
        "ffn": ffn_layer.FeedForwardNetwork(self.d_model, self.dff, self.dropout_rate),
        "ffn_norm": tf.keras.layers.LayerNormalization(epsilon=1e-6)
    } for _ in range(self.num_layers)]
    super(EcgEncoder, self).build(input_shape)

  def call(self, inputs, training=None):
    logits, mask = inputs[0], inputs[1]
    
    clz = self.clz[tf.newaxis, tf.newaxis, :]
    clz = tf.repeat(clz, tf.shape(logits)[0], axis=0)
    logits = tf.concat([clz, logits], 1)
    logits += self.pos[:tf.shape(logits)[1], :]
    
    mask = tf.pad(mask, [[0, 0], [1, 0]], constant_values=0)
    mask = model_utils.get_padding_bias(mask, padding_value=1)
    eye = tf.eye(tf.shape(logits)[1]) * -1e9
    
    for i, layer in enumerate(self.internal_layers):
      self_attention = layer["self_attention"]
      attention_norm = layer["attention_norm"]
      ffn = layer["ffn"]
      ffn_norm = layer["ffn_norm"]

      attn_output = self_attention(logits, bias=tf.minimum(mask, eye), training=training)
      if training:
        attn_output = tf.nn.dropout(attn_output, rate=self.dropout_rate)
      logits = attention_norm(logits + attn_output)
    
      if i == self.num_layers - 1:
        logits = logits[:, 0:1, :]
      
      ffn_output = ffn(logits, training=training)
      if training:
        ffn_output = tf.nn.dropout(ffn_output, rate=self.dropout_rate)
      logits = ffn_norm(logits + ffn_output)
    
    return logits


class IdEncoder(tf.keras.layers.Layer):
  def __init__(self, num_layers, d_model, num_heads, dff, dropout_rate):
    super(IdEncoder, self).__init__()
    self.num_layers = num_layers
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate

  def build(self, input_shape):
    
    self.internal_layers = [{
      "self_attention": attention_layer.SelfAttention(self.d_model, self.num_heads, self.dropout_rate),
      "attention_norm": tf.keras.layers.LayerNormalization(epsilon=1e-6),
      "ffn": ffn_layer.FeedForwardNetwork(self.d_model, self.dff, self.dropout_rate),
      "ffn_norm": tf.keras.layers.LayerNormalization(epsilon=1e-6)
    } for _ in range(self.num_layers)]
    super(IdEncoder, self).build(input_shape)
    
  def call(self, inputs, training=None):
    logits, mask = inputs[0], inputs[1]
        
    for i, layer in enumerate(self.internal_layers):
      self_attention = layer["self_attention"]
      attention_norm = layer["attention_norm"]
      ffn = layer["ffn"]
      ffn_norm = layer["ffn_norm"]

      attn_output = self_attention(logits, bias=mask, training=training)
      if training:
        attn_output = tf.nn.dropout(attn_output, rate=self.dropout_rate)
      logits = attention_norm(logits + attn_output)
          
      ffn_output = ffn(logits, training=training)
      if training:
        ffn_output = tf.nn.dropout(ffn_output, rate=self.dropout_rate)
      logits = ffn_norm(logits + ffn_output)
    
    return logits