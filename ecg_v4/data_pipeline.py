import tensorflow as tf
import os, json
from random import sample, choice

_READ_RECORD_BUFFER = 32 * 1024 * 1024


def get_ds(params, split):
  file_pattern = os.path.join(params['data_dir'] or "", f"*{split}*")
  ds = tf.data.Dataset.list_files(file_pattern, shuffle=True)
  options = tf.data.Options()
  options.experimental_deterministic = False
  ds = ds.interleave(
      _load_records,
      cycle_length=tf.data.experimental.AUTOTUNE,
      num_parallel_calls=tf.data.experimental.AUTOTUNE).with_options(options)

  def _parse_example(serialized_example):
    data_fields = {
        'label': tf.io.FixedLenFeature([params['batch_size']], tf.int64),
        'd0': tf.io.FixedLenFeature([], tf.string),
        'd1': tf.io.FixedLenFeature([], tf.string),
    }
    parsed = tf.io.parse_single_example(serialized_example, data_fields)
    label = tf.cast(parsed['label'], tf.int64)
    label = tf.one_hot(label - 1, params['max_person'])
    d0 = tf.io.parse_tensor(parsed['d0'], tf.float32)
    d0 = tf.reshape(d0, [params['batch_size'], params['max_person'], params['num_left'], params['max_length']])
    d1 = tf.io.parse_tensor(parsed['d1'], tf.float32)
    d1 = tf.reshape(d1, [params['batch_size'], params['max_person'], params['num_right'], params['max_length']])
    return label, d0, d1

  ds = ds.map(_parse_example,
      num_parallel_calls=tf.data.experimental.AUTOTUNE)

  def ds_map(label, d0, d1):
    return (d0, d1), label

  ds = ds.map(ds_map, num_parallel_calls=tf.data.experimental.AUTOTUNE)
  # ds = ds.cache()
  
  ds = ds.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)

  return ds


def _load_records(filename):
  return tf.data.TFRecordDataset(filename, compression_type='GZIP', 
      buffer_size=_READ_RECORD_BUFFER, num_parallel_reads=tf.data.experimental.AUTOTUNE)
