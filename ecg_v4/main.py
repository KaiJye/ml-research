import tensorflow as tf
import numpy as np
from ecg_data import data_pipeline as test_dp
# from ecg_v3 import biometric_model as vm
from ecg_v4 import biometric_model, data_pipeline

class MainTask:
  def __init__(self, params, opt=None, resolver=None):
    self.params = params
    self.opt = opt

    if resolver:
      self.distribution_strategy = tf.distribute.experimental.TPUStrategy(resolver)
      self.params['static_batch'] = True
    else:
      self.distribution_strategy = tf.distribute.MirroredStrategy()
      self.params['static_batch'] = False

  def train(self, validation=True, load_vm=True, fine_tune=False):
    params = self.params

    with self.distribution_strategy.scope():
      model, internal_model = biometric_model.create_training_model(params)
      if fine_tune:
        model.load_weights(params['model_dir'])
      elif load_vm:
        internal_model.verification_model.load_weights(params['verification_model_dir'])
        internal_model.verification_model.trainable = False
      opt = tf.keras.optimizers.Adam(params['learning_rate'],
          beta_1=params['optimizer_adam_beta1'],
          beta_2=params['optimizer_adam_beta2'], 
          epsilon=params['optimizer_adam_epsilon'])

      loss = tf.keras.losses.CategoricalCrossentropy(from_logits=True, label_smoothing=params['label_smoothing'])
      categorical_accuracy = tf.keras.metrics.CategoricalAccuracy(name='ca')
      model.compile(optimizer=opt, loss=loss, metrics=[
        categorical_accuracy
      ])

    model.summary()

    callbacks = self._create_callbacks()

    train_ds: tf.data.Dataset = data_pipeline.get_ds(params, 'train')
    train_ds = train_ds.cache()
    if params['steps_per_epoch']:
      train_ds = train_ds.repeat()
      train_ds = train_ds.shuffle(params['steps_per_epoch'])
    else:
      train_ds = train_ds.shuffle(512)

    if validation:
      val_ds = data_pipeline.get_ds(params, 'val')
      val_ds = val_ds.cache()
    else:
      val_ds = None

    model.fit(train_ds,
        validation_data=val_ds,
        epochs=params['epochs'],
        steps_per_epoch=params['steps_per_epoch'],
        callbacks=callbacks,
        verbose=params['verbose'])

    model.save_weights(self.params['model_dir'])

    return model

  def _create_callbacks(self):
    callbacks = []

    # def scheduler(epoch, lr):
    #   return 0.000012 * tf.math.exp(2 - (0.03 * epoch)) + 0.00004
    # lr_callback = tf.keras.callbacks.LearningRateScheduler(scheduler)
    # callbacks.append(lr_callback)
    
    early_stop_callback = tf.keras.callbacks.EarlyStopping(
      monitor='val_ca', min_delta=0, patience=self.params['patience'], verbose=0, mode='max',
      baseline=None, restore_best_weights=True
    )
    callbacks.append(early_stop_callback)
    
    return callbacks

  def eval(self, split, model=None):
    params = self.params

    with self.distribution_strategy.scope():
      if model is None:
        model, _ = biometric_model.create_training_model(params)
        model.load_weights(params['model_dir'])

      categorical_accuracy = tf.keras.metrics.CategoricalAccuracy(name='ca')
      model.compile(metrics=[
        categorical_accuracy
      ])

    model.summary()

    model.reset_metrics()

    test_ds = data_pipeline.get_ds(params, split)
    model.evaluate(test_ds, verbose=params['verbose'])
    