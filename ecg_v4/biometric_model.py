import tensorflow as tf
from common import model_utils
from ecg_v3 import biometric_model


def get_loss_fn(params):
  def loss_fn(label, logits, mask):
    loss = tf.keras.losses.categorical_crossentropy(
        label, logits, from_logits=True, label_smoothing=params['label_smoothing']
    )
    loss *= mask
    loss = tf.reduce_sum(loss) / tf.reduce_sum(mask)
    return loss
  return loss_fn


def create_training_model(params):
  d0 = tf.keras.layers.Input((None, None, params['max_length']), dtype=tf.float32)
  d1 = tf.keras.layers.Input((None, None, params['max_length']), dtype=tf.float32)
  # label = tf.keras.layers.Input((params['max_length'],), dtype=tf.float32)

  internal_model = IdentificationModel(params)

  logits, _ = internal_model([d0, d1])
  model = tf.keras.Model([d0, d1], logits)

  # loss = tf.keras.layers.Lambda(lambda x: get_loss_fn(params)(x[0], x[1], x[2]))([label, logits, mask])
  # model.add_loss(loss)

  return model, internal_model


class IdentificationModel(tf.keras.Model):

  def __init__(self, params):
    super(IdentificationModel, self).__init__()
    self.params = params

  def build(self, input_shape):
    params = self.params

    # self.reject = self.add_weight(
    #     "reject",
    #     shape=[params["d_model"]],
    #     initializer='glorot_uniform')

    self.verification_model, self.vm = biometric_model.create_training_model(params, return_logit=True)

    self.encoder = biometric_model.Encoder(params["id_num_encoder_layers"], params["d_model"],
        params["num_heads"], params["dff"], params["dropout_rate"])

    self.final = tf.keras.layers.Dense(1)

    super(IdentificationModel, self).build(input_shape)

  def call(self, inputs, training=None):

    batch_size = tf.shape(inputs[0])[0]
    max_person = tf.shape(inputs[0])[1]
    num_left = tf.shape(inputs[0])[2]
    num_right = tf.shape(inputs[1])[2]
    max_length = tf.shape(inputs[0])[3]
    d_model = self.params['d_model']

    person_mask = tf.less(tf.reshape(inputs[1], [batch_size, max_person, num_right*max_length]), tf.constant(-1e7, dtype=tf.float32))
    person_mask = tf.logical_not(tf.reduce_all(person_mask, axis=-1))
    person_mask_int = tf.cast(person_mask, tf.float32)

    left = inputs[0]
    left = tf.reshape(left, [-1, num_left, max_length])
    right = inputs[1]
    right = tf.reshape(right, [-1, num_right, max_length])

    logits = self.verification_model([left, right], training=training)

    logits = tf.reshape(logits, [batch_size, max_person, d_model])
    # reject = self.reject[tf.newaxis, tf.newaxis, :]
    # reject = tf.repeat(reject, batch_size, axis=0)
    # logits = tf.concat([reject, logits], 1)

    # person_mask_int = tf.pad(person_mask_int, [[0, 0], [1, 0]], constant_values=1)
    person_encoder_mask = model_utils.get_padding_bias(person_mask_int)

    logits = self.encoder([logits, person_encoder_mask], training=training)

    logits = self.final(logits)
    logits = tf.reshape(logits, [batch_size, max_person])
    # mask = (person_mask_int - 1) * 1e9
    # logits += mask

    # logits = tf.nn.softmax(logits)

    return logits, person_mask_int
