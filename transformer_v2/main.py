import tensorflow as tf
import os
from transformer_v2 import transformer as tr
from transformer_v1 import optimizer
from transformer_v1 import metrics
from transformer_v1 import data_pipeline
from symmetric_v1 import symmetric_transformer as st

class MainTask:
  def __init__(self, params):
    self.params = params

    try:
      resolver = tf.distribute.cluster_resolver.TPUClusterResolver(tpu='grpc://' + os.environ['COLAB_TPU_ADDR'])
      tf.config.experimental_connect_to_cluster(resolver)
      tf.tpu.experimental.initialize_tpu_system(resolver)
      self.distribution_strategy = tf.distribute.experimental.TPUStrategy(resolver)
      self.params['static_batch'] = True
    except:
      self.distribution_strategy = tf.distribute.MirroredStrategy()
      self.params['static_batch'] = False
  

  def train(self):
    params = self.params

    with self.distribution_strategy.scope():
      inputs = tf.keras.layers.Input((None,), dtype=tf.int64)
      targets = tf.keras.layers.Input((None,), dtype=tf.int64)

      sym_tr = self.load_sym()
      sym_tr.trainable = False
      internal_model = tr.Transformer(params["num_layers"], params["d_model"],
          params["num_heads"], params["dff"], params["dropout_rate"], params["vocab_size"])

      sym_inp, inp_attention_mask = sym_tr(inputs, mode="forward")
      sym_tar, tar_attention_mask = sym_tr(targets, mode="forward")

      tr_output = internal_model([sym_inp, sym_tar, inp_attention_mask])

      logits = sym_tr([tr_output, tar_attention_mask], mode="backward")

      model = tf.keras.Model([inputs, targets], logits)

      opt = optimizer.create_optimizer(params)

      checkpoint_path = params['model_dir']

      ckpt = tf.train.Checkpoint(model=model, optimizer=opt)

      ckpt_manager = tf.train.CheckpointManager(ckpt, checkpoint_path, max_to_keep=params['max_to_keep'])

      current_step = 0
      if ckpt_manager.latest_checkpoint:
        ckpt.restore(ckpt_manager.latest_checkpoint)
        print ('Latest checkpoint restored!!')
        current_step = opt.iterations.numpy()
      
      print('current train step', current_step)
      model.compile(optimizer=opt, loss=metrics.loss_function_2)

    model.summary()

    train_ds = data_pipeline.get_train_ds_2(params)

    callbacks = self._create_callbacks(ckpt_manager)

    current_epoch = 0
    while current_epoch < params['epochs']:
      model.fit(train_ds,
          initial_epoch=current_epoch,
          epochs=current_epoch + 1,
          steps_per_epoch=params['steps_per_epoch'],
          callbacks=callbacks,
          verbose=params['verbose'])

      ckpt_manager.save()

      current_epoch += 1

  def load_sym(self):
    params = self.params
    internal_model = st.SymmetricTransformer(params["num_sym_layers"], params["d_model"],
        params["num_heads"], params["dff"], params["dropout_rate"], params["vocab_size"])
    internal_model.build(tf.TensorShape([None, None]))

    internal_model.load_weights(params["weight_path"])

    return internal_model
  
  def _create_callbacks(self, ckpt_manager=None):
    callbacks = []
    learning_rate_callback = optimizer.LearningRateCallback(self.params)
    callbacks.append(learning_rate_callback)
    # if ckpt_manager:
    #   ckpt_callback = CheckpointCallback(ckpt_manager)
    #   callbacks.append(ckpt_callback)
    return callbacks
