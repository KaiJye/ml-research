import tensorflow as tf
from common import embedding_layer, attention_layer, ffn_layer, model_utils

class EncoderLayer(tf.keras.layers.Layer):
  def __init__(self, d_model, num_heads, dff, dropout_rate):
    super(EncoderLayer, self).__init__()
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate

  def build(self, input_shape):
    self.self_attention = attention_layer.SelfAttention(self.d_model, self.num_heads, self.dropout_rate)
    self.ffn = ffn_layer.FeedForwardNetwork(self.d_model, self.dff, self.dropout_rate)

    self.layernorm1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.layernorm2 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    
    self.dropout1 = tf.keras.layers.Dropout(self.dropout_rate)
    self.dropout2 = tf.keras.layers.Dropout(self.dropout_rate)
    super(EncoderLayer, self).build(input_shape)
  
  def call(self, x, attention_mask, training):
    attn_output = self.self_attention(x, attention_mask, training=training)  # (batch_size, input_seq_len, d_model)
    attn_output = self.dropout1(attn_output, training=training)
    out1 = self.layernorm1(x + attn_output)  # (batch_size, input_seq_len, d_model)
    
    ffn_output = self.ffn(out1, training=training)  # (batch_size, input_seq_len, d_model)
    ffn_output = self.dropout2(ffn_output, training=training)
    out2 = self.layernorm2(out1 + ffn_output)  # (batch_size, input_seq_len, d_model)
    
    return out2

class DecoderLayer(tf.keras.layers.Layer):
  def __init__(self, d_model, num_heads, dff, dropout_rate):
    super(DecoderLayer, self).__init__()
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate
    
  def build(self, input_shape):
    self.self_attention = attention_layer.SelfAttention(self.d_model, self.num_heads, self.dropout_rate)
    self.attention = attention_layer.Attention(self.d_model, self.num_heads, self.dropout_rate)
    self.ffn = ffn_layer.FeedForwardNetwork(self.d_model, self.dff, self.dropout_rate)

    self.layernorm1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.layernorm2 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.layernorm3 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    
    self.dropout1 = tf.keras.layers.Dropout(self.dropout_rate)
    self.dropout2 = tf.keras.layers.Dropout(self.dropout_rate)
    self.dropout3 = tf.keras.layers.Dropout(self.dropout_rate)
    super(DecoderLayer, self).build(input_shape)

  def call(self, x, enc_output, look_ahead_mask, attention_mask, training):
    # enc_output.shape == (batch_size, input_seq_len, d_model)
    attn1 = self.self_attention(x, look_ahead_mask, training=training)  # (batch_size, target_seq_len, d_model)
    attn1 = self.dropout1(attn1, training=training)
    out1 = self.layernorm1(attn1 + x)
    
    attn2 = self.attention(out1, enc_output, attention_mask, training=training)  # (batch_size, target_seq_len, d_model)
    attn2 = self.dropout2(attn2, training=training)
    out2 = self.layernorm2(attn2 + out1)  # (batch_size, target_seq_len, d_model)
    
    ffn_output = self.ffn(out2, training=training)  # (batch_size, target_seq_len, d_model)
    ffn_output = self.dropout3(ffn_output, training=training)
    out3 = self.layernorm3(ffn_output + out2)  # (batch_size, target_seq_len, d_model)
    
    return out3

class Encoder(tf.keras.layers.Layer):
  def __init__(self, num_layers, d_model, num_heads, dff, dropout_rate, vocab_size):
    super(Encoder, self).__init__()
    self.num_layers = num_layers
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate    
    self.vocab_size = vocab_size    
  
  def build(self, input_shape):
    self.enc_layers = [EncoderLayer(self.d_model, self.num_heads, self.dff, self.dropout_rate)
        for _ in range(self.num_layers)]
    super(Encoder, self).build(input_shape)

  def call(self, x, attention_mask, training):
    for layer in self.enc_layers:
      x = layer(x, attention_mask, training)
    
    return x  # (batch_size, input_seq_len, d_model)

class Decoder(tf.keras.layers.Layer):
  def __init__(self, num_layers, d_model, num_heads, dff, dropout_rate, vocab_size):
    super(Decoder, self).__init__()
    self.num_layers = num_layers
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate    
    self.vocab_size = vocab_size    

  def build(self, input_shape):
    self.dec_layers = [DecoderLayer(self.d_model, self.num_heads, self.dff, self.dropout_rate)
        for _ in range(self.num_layers)]
    super(Decoder, self).build(input_shape)

  def call(self, x, enc_output, attention_mask, training):
    seq_len = tf.shape(x)[1]
    look_ahead_mask = model_utils.get_decoder_self_attention_bias(seq_len)

    for layer in self.dec_layers:
      x = layer(x, enc_output, look_ahead_mask, attention_mask, training)
    
    # x.shape == (batch_size, target_seq_len, d_model)
    return x

class Transformer(tf.keras.Model):
  def __init__(self, num_layers, d_model, num_heads, dff, dropout_rate, vocab_size):
    super(Transformer, self).__init__()
    self.num_layers = num_layers
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate
    self.vocab_size = vocab_size    

  def build(self, input_shape):
    self.encoder = Encoder(self.num_layers, self.d_model, self.num_heads, 
        self.dff, self.dropout_rate, self.vocab_size)

    self.decoder = Decoder(self.num_layers, self.d_model, self.num_heads, 
        self.dff, self.dropout_rate, self.vocab_size)

    super(Transformer, self).build(input_shape)
    
  def call(self, inputs, training=None):
    inp, tar, attention_mask = inputs[0], inputs[1], inputs[2]

    enc_output = self.encoder(inp, attention_mask, training=training)  # (batch_size, inp_seq_len, d_model)
    
    dec_output = self.decoder(
        tar, enc_output, attention_mask, training=training)

    return dec_output
    
