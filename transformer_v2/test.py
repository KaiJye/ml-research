import tensorflow as tf
from symmetric_v1 import symmetric_transformer as st
from transformer_v2 import transformer as tr

def load_sym():
  internal_model = st.SymmetricTransformer(8, 512,
      8, 2048, 0.1, 32760)
  internal_model.build(tf.TensorShape([None, None]))

  internal_model.load_weights(r"D:\models\sym_combined_models\weights.h5")

  return internal_model

def create_model():
  inputs = tf.keras.layers.Input((None,), dtype=tf.int64)
  targets = tf.keras.layers.Input((None,), dtype=tf.int64)

  sym_tr = load_sym()
  sym_tr.trainable = False
  internal_model = tr.Transformer(6, 512, 8,
      2048, 0.1, 32760)

  sym_inp, attention_mask = sym_tr(inputs, mode="forward")
  sym_tar, _ = sym_tr(targets, mode="forward")

  tr_output = internal_model([sym_inp, sym_tar, attention_mask])

  logits = sym_tr([tr_output, attention_mask], mode="backward")

  model = tf.keras.Model([inputs, targets], logits)

  return model

model = create_model()
model.summary()