import tensorflow as tf
import tensorflow_datasets as tfds
import numpy as np

a = tf.fill([16], 9)

def preprocess(x):
  shape_x = tf.shape(x)
  length = shape_x[0]

  # [0 0 0 0 0 0 0 -1 -1 -1 -1 -1 0 0 0 0 0 0 0]
  mask_length = tf.cast(tf.cast(length, dtype=tf.float32) * 0.5, dtype=tf.int64)
  window = tf.fill([mask_length], tf.constant(-1, dtype=tf.int64))
  pad_length = tf.cast(length, dtype=tf.float32) * 1.1
  window = tf.pad(window, [[pad_length, pad_length]])

  # [0 0 0 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0]
  # randomize positions of ones
  selector_pad_length = tf.cast(tf.shape(window)[0] - length, dtype=tf.int64)
  cut = tf.random.uniform([], maxval=selector_pad_length, dtype=tf.int64)
  selector = tf.ones(shape_x, dtype=tf.int64)
  selector = tf.pad(selector, [[cut, selector_pad_length - cut]])

  x = tf.pad(x, [[cut, selector_pad_length - cut]])

  index = selector + window
  inp = tf.gather_nd(x, tf.where(tf.equal(index, 1)))

  tar = length / tf.shape(inp)[0]
  
  inp = tf.concat([[5], inp], axis=-1)
  return inp, tar

for _ in np.arange(0, 20):
  print(preprocess(a)[1].numpy())