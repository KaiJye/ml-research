import tensorflow as tf
import tensorflow_datasets as tfds
from common import dataset_utils
import math
import os
import re

_READ_RECORD_BUFFER = 8 * 1000 * 1000

_MIN_BOUNDARY = 8
_BOUNDARY_SCALE = 1.1

SOS_ID = dataset_utils.SOS_ID
EOS_ID = dataset_utils.EOS_ID
MAS_ID = dataset_utils.MAS_ID
POS_ID = dataset_utils.POS_ID
CLS_ID = dataset_utils.CLS_ID

def length_preprocess_fn(params):

  def preprocess(x):
    shape_x = tf.shape(x)
    length = shape_x[0]

    # [0 0 0 0 0 0 0 -1 -1 -1 -1 -1 0 0 0 0 0 0 0]
    mask_length = tf.cast(tf.cast(length, dtype=tf.float32) * 0.5, dtype=tf.int64)
    window = tf.fill([mask_length], tf.constant(-1, dtype=tf.int64))
    pad_length = tf.cast(length, dtype=tf.float32) * 1.1
    window = tf.pad(window, [[pad_length, pad_length]])

    # [0 0 0 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0]
    # randomize positions of ones
    selector_pad_length = tf.cast(tf.shape(window)[0] - length, dtype=tf.int64)
    cut = tf.random.uniform([], maxval=selector_pad_length, dtype=tf.int64)
    selector = tf.ones(shape_x, dtype=tf.int64)
    selector = tf.pad(selector, [[cut, selector_pad_length - cut]])

    x = tf.pad(x, [[cut, selector_pad_length - cut]])

    index = selector + window
    inp = tf.gather_nd(x, tf.where(tf.equal(index, 1)))

    tar = length / tf.shape(inp)[0]
    
    inp = tf.concat([[5], inp], axis=-1)
    return inp, tar
    
  return preprocess

def get_train_length_ds(params):
  file_pattern = os.path.join(params["data_dir"] or "", "*train*")

  ds = tf.data.Dataset.list_files(file_pattern, shuffle=True)

  options = tf.data.Options()
  options.experimental_deterministic = False
  ds = ds.interleave(
      _load_records,
      cycle_length=tf.data.experimental.AUTOTUNE,
      num_parallel_calls=tf.data.experimental.AUTOTUNE).with_options(options)

  ds = ds.map(lambda x: tf.io.parse_tensor(x, tf.int64),
      num_parallel_calls=tf.data.experimental.AUTOTUNE)

  ds = ds.map(length_preprocess_fn(params), num_parallel_calls=tf.data.experimental.AUTOTUNE)
  
  max_length = params['max_length']
  ds = ds.filter(lambda x, y: _filter_max_length((x, y), max_length))

  ds = ds.padded_batch(params['batch_size'] // 8, padded_shapes=([max_length], [max_length]))

  if params['shuffle_buffer_size']:
    ds = ds.shuffle(params['shuffle_buffer_size'])

  ds = ds.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)

  return ds

def get_validation_length_ds(params):
  file_pattern = os.path.join(params["data_dir"] or "", "*validation*")

  ds = tf.data.Dataset.list_files(file_pattern, shuffle=True)

  options = tf.data.Options()
  options.experimental_deterministic = False
  ds = ds.interleave(
      _load_records,
      cycle_length=tf.data.experimental.AUTOTUNE,
      num_parallel_calls=tf.data.experimental.AUTOTUNE).with_options(options)

  ds = ds.map(lambda x: tf.io.parse_tensor(x, tf.int64),
      num_parallel_calls=tf.data.experimental.AUTOTUNE)

  ds = ds.map(length_preprocess_fn(params), num_parallel_calls=tf.data.experimental.AUTOTUNE)
  
  max_length = params['max_length']
  ds = ds.filter(lambda x, y: _filter_max_length((x, y), max_length))

  ds = ds.padded_batch(128, padded_shapes=([max_length], [max_length]))

  ds = ds.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)

  return ds

def get_test_length_ds(params):
  file_pattern = os.path.join(params["data_dir"] or "", "*test*")

  ds = tf.data.Dataset.list_files(file_pattern, shuffle=True)

  options = tf.data.Options()
  options.experimental_deterministic = False
  ds = ds.interleave(
      _load_records,
      cycle_length=tf.data.experimental.AUTOTUNE,
      num_parallel_calls=tf.data.experimental.AUTOTUNE).with_options(options)

  ds = ds.map(lambda x: tf.io.parse_tensor(x, tf.int64),
      num_parallel_calls=tf.data.experimental.AUTOTUNE)

  ds = ds.map(length_preprocess_fn(params), num_parallel_calls=tf.data.experimental.AUTOTUNE)
  
  max_length = params['max_length']
  ds = ds.filter(lambda x, y: _filter_max_length((x, y), max_length))

  ds = ds.padded_batch(params['test_batch_size'], padded_shapes=([max_length], [max_length]))

  if params['shuffle_buffer_size']:
    ds = ds.shuffle(params['shuffle_buffer_size'])

  ds = ds.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)

  return ds

def get_preprocess_fn(params):

  def preprocess(x):
    total_length = tf.cast(tf.shape(x)[0], dtype=tf.int64)

    # mask 'mask_percentage' lots of total_length
    mask_length = tf.cast(tf.cast(total_length, dtype=tf.float32) * params['mask_percentage'], dtype=tf.int64)

    # create mask
    mask = tf.concat([tf.fill([mask_length], True), tf.fill([total_length - mask_length], False)], axis=-1)
    mask = tf.random.shuffle(mask)

    roll = tf.random.uniform(tf.shape(x))
    replacement = tf.where(tf.less(roll, 0.8), tf.constant(MAS_ID, dtype=tf.int64), x)
    replacement = tf.where(tf.greater(roll, 0.9), 
        tf.random.uniform([], minval=1, maxval=params['vocab_size'], dtype=tf.int64), replacement)

    # replace inp at mask positions
    inp = tf.where(mask, replacement, x)

    # inp = tf.concat([[SOS_ID], inp, [EOS_ID]], axis=-1)
    inp = tf.concat([[POS_ID], inp], axis=-1)

    # mask tar at any other positions
    tar = tf.where(mask, x, 0)

    # pad tar at sos and eos positions
    # tar = tf.pad(tar, [[1, 1]])

    # pad tar at pos positions
    tar = tf.pad(tar, [[1, 0]])
    return inp, tar
  
  return preprocess

def get_train_ds(params):
  file_pattern = os.path.join(params["data_dir"] or "", "*train*")

  ds = tf.data.Dataset.list_files(file_pattern, shuffle=True)

  options = tf.data.Options()
  options.experimental_deterministic = False
  ds = ds.interleave(
      _load_records,
      cycle_length=tf.data.experimental.AUTOTUNE,
      num_parallel_calls=tf.data.experimental.AUTOTUNE).with_options(options)

  ds = ds.map(lambda x: tf.io.parse_tensor(x, tf.int64),
      num_parallel_calls=tf.data.experimental.AUTOTUNE)

  ds = ds.map(get_preprocess_fn(params), num_parallel_calls=tf.data.experimental.AUTOTUNE)
  
  max_length = params['max_length']
  ds = ds.filter(lambda x, y: _filter_max_length((x, y), max_length))

  ds = ds.padded_batch(params['batch_size'] // 8, padded_shapes=([max_length], [max_length]))

  if params['shuffle_buffer_size']:
    ds = ds.shuffle(params['shuffle_buffer_size'])

  # def add_weights_fn(inp, tar):
  #   # sample weights
  #   weights = tf.not_equal(tar, 0)
  #   return inp, tar, weights

  # ds = ds.map(add_weights_fn, num_parallel_calls=tf.data.experimental.AUTOTUNE)

  ds = ds.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)

  return ds

def get_validation_ds(params):
  file_pattern = os.path.join(params["data_dir"] or "", "*validation*")

  ds = tf.data.Dataset.list_files(file_pattern, shuffle=True)

  options = tf.data.Options()
  options.experimental_deterministic = False
  ds = ds.interleave(
      _load_records,
      cycle_length=tf.data.experimental.AUTOTUNE,
      num_parallel_calls=tf.data.experimental.AUTOTUNE).with_options(options)

  ds = ds.map(lambda x: tf.io.parse_tensor(x, tf.int64),
      num_parallel_calls=tf.data.experimental.AUTOTUNE)

  ds = ds.map(get_preprocess_fn(params), num_parallel_calls=tf.data.experimental.AUTOTUNE)
  
  max_length = params['max_length']
  ds = ds.filter(lambda x, y: _filter_max_length((x, y), max_length))

  ds = ds.padded_batch(128, padded_shapes=([max_length], [max_length]))

  ds = ds.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)

  return ds

def get_test_ds(params):
  file_pattern = os.path.join(params["data_dir"] or "", "*test*")

  ds = tf.data.Dataset.list_files(file_pattern, shuffle=True)

  options = tf.data.Options()
  options.experimental_deterministic = False
  ds = ds.interleave(
      _load_records,
      cycle_length=tf.data.experimental.AUTOTUNE,
      num_parallel_calls=tf.data.experimental.AUTOTUNE).with_options(options)

  ds = ds.map(lambda x: tf.io.parse_tensor(x, tf.int64),
      num_parallel_calls=tf.data.experimental.AUTOTUNE)

  ds = ds.map(get_preprocess_fn(params), num_parallel_calls=tf.data.experimental.AUTOTUNE)
  
  max_length = params['max_length']
  ds = ds.filter(lambda x, y: _filter_max_length((x, y), max_length))

  ds = ds.padded_batch(params['test_batch_size'], padded_shapes=([max_length], [max_length]))

  if params['shuffle_buffer_size']:
    ds = ds.shuffle(params['shuffle_buffer_size'])

  ds = ds.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)

  return ds

def _filter_max_length(example, max_length=256):
  """Indicates whether the example's length is lower than the maximum length."""
  return tf.logical_and(tf.size(example[0]) <= max_length,
                        tf.size(example[1]) <= max_length)

def _load_records(filename):
  """Read file and return a dataset of tf.Examples."""
  return tf.data.TFRecordDataset(filename, buffer_size=_READ_RECORD_BUFFER)


def _get_example_length(example):
  """Returns the maximum length between the example inputs and targets."""
  return tf.shape(example)[0]


def _create_min_max_boundaries(
    max_length, min_boundary=_MIN_BOUNDARY, boundary_scale=_BOUNDARY_SCALE):
  """Create min and max boundary lists up to max_length.
  For example, when max_length=24, min_boundary=4 and boundary_scale=2, the
  returned values will be:
    buckets_min = [0, 4, 8, 16, 24]
    buckets_max = [4, 8, 16, 24, 25]
  Args:
    max_length: The maximum length of example in dataset.
    min_boundary: Minimum length in boundary.
    boundary_scale: Amount to scale consecutive boundaries in the list.
  Returns:
    min and max boundary lists
  """
  # Create bucket boundaries list by scaling the previous boundary or adding 1
  # (to ensure increasing boundary sizes).
  bucket_boundaries = []
  x = min_boundary
  while x < max_length:
    bucket_boundaries.append(x)
    x = max(x + 1, int(x * boundary_scale))

  # Create min and max boundary lists from the initial list.
  buckets_min = [0] + bucket_boundaries
  buckets_max = bucket_boundaries + [max_length + 1]
  return buckets_min, buckets_max


def _batch_examples(dataset, batch_size, max_length):
  """Group examples by similar lengths, and return batched dataset.
  Each batch of similar-length examples are padded to the same length, and may
  have different number of elements in each batch, such that:
    group_batch_size * padded_length <= batch_size.
  This decreases the number of padding tokens per batch, which improves the
  training speed.
  Args:
    dataset: Dataset of unbatched examples.
    batch_size: Max number of tokens per batch of examples.
    max_length: Max number of tokens in an example input or target sequence.
  Returns:
    Dataset of batched examples with similar lengths.
  """
  # Get min and max boundary lists for each example. These are used to calculate
  # the `bucket_id`, which is the index at which:
  # buckets_min[bucket_id] <= len(example) < buckets_max[bucket_id]
  # Note that using both min and max lists improves the performance.
  buckets_min, buckets_max = _create_min_max_boundaries(max_length)

  # Create list of batch sizes for each bucket_id, so that
  # bucket_batch_size[bucket_id] * buckets_max[bucket_id] <= batch_size
  bucket_batch_sizes = [batch_size // x for x in buckets_max]
  # bucket_id will be a tensor, so convert this list to a tensor as well.
  bucket_batch_sizes = tf.constant(bucket_batch_sizes, dtype=tf.int64)

  def example_to_bucket_id(example_input):
    """Return int64 bucket id for this example, calculated based on length."""
    seq_length = _get_example_length(example_input)

    # TODO(xunkai): investigate if removing code branching improves performance.
    conditions_c = tf.logical_and(
        tf.less_equal(buckets_min, seq_length),
        tf.less(seq_length, buckets_max))
    bucket_id = tf.reduce_min(tf.where(conditions_c))
    return bucket_id

  def window_size_fn(bucket_id):
    """Return number of examples to be grouped when given a bucket id."""
    return bucket_batch_sizes[bucket_id]

  def batching_fn(bucket_id, grouped_dataset):
    """Batch and add padding to a dataset of elements with similar lengths."""
    bucket_batch_size = window_size_fn(bucket_id)

    # Batch the dataset and add padding so that all input sequences in the
    # examples have the same length, and all target sequences have the same
    # lengths as well. Resulting lengths of inputs and targets can differ.
    return grouped_dataset.padded_batch(bucket_batch_size, [None])

  return dataset.apply(tf.data.experimental.group_by_window(
      key_func=example_to_bucket_id,
      reduce_func=batching_fn,
      window_size=None,
      window_size_func=window_size_fn))
