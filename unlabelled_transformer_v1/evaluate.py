import tensorflow as tf
import tensorflow_datasets as tfds
from common import dataset_utils
import math
import collections
import numpy as np
import re
import sys
import unicodedata
import six

def decode(params, ids):
  vocab = tfds.features.text.SubwordTextEncoder.load_from_file(params['vocab_path'])

  decoded = tf.constant([], tf.string)

  def _condition(ids, decoded):
    return tf.greater(tf.shape(ids)[0], 0)

  def _body(ids, decoded):
    encoded = tf.squeeze(ids[0, :])
    nonZeroIndices = tf.squeeze(tf.where(tf.not_equal(encoded, 0)))
    encoded = tf.gather(encoded, nonZeroIndices, axis=0)
    decoded = tf.concat([decoded, [vocab.decode(encoded)]], axis=0)
    return (ids[1:, :], decoded)

  _, translates = tf.while_loop(_condition, _body, loop_vars=(ids, decoded),
      shape_invariants=(tf.TensorShape([None, ids.shape[1]]), tf.TensorShape([None])))

  return translates
