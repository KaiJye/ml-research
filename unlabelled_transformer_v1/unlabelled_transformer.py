import tensorflow as tf
from common import embedding_layer, attention_layer, ffn_layer, model_utils

def create_training_model(params):
  inputs = tf.keras.layers.Input((None,), dtype=tf.int64)

  transformer_stack = TransformerStack(params["num_layers"], params["d_model"],
      params["num_heads"], params["dff"], params["dropout_rate"])
  embedding = Embedding(params["d_model"], params["vocab_size"])
  length_predictor = LengthPredictor(params["d_model"],  params["num_heads"], params["dropout_rate"])
  cloze_predictor = ClozePredictor(params["d_model"], params["num_heads"], params["dff"], params["dropout_rate"])

  logits, attention_mask = embedding(inputs, mode="embedding")
  logits = transformer_stack([logits, attention_mask])
  length = length_predictor([logits, attention_mask])
  cloze = cloze_predictor([logits, attention_mask])
  cloze = embedding(cloze, mode="linear")

  model = tf.keras.Model(inputs, [length, cloze])

  return model, transformer_stack, length_predictor, cloze_predictor

def create_cloze_training_model(params):
  inputs = tf.keras.layers.Input((None,), dtype=tf.int64)

  transformer_stack = TransformerStack(params["num_layers"], params["d_model"],
      params["num_heads"], params["dff"], params["dropout_rate"])
  embedding = Embedding(params["d_model"], params["vocab_size"])
  cloze_predictor = ClozePredictor(params["d_model"], params["num_heads"], params["dff"], params["dropout_rate"])

  logits, attention_mask = embedding(inputs, mode="embedding")
  logits = transformer_stack([logits, attention_mask])
  cloze = cloze_predictor([logits, attention_mask])
  cloze = embedding(cloze, mode="linear")

  model = tf.keras.Model(inputs, cloze)

  return model, transformer_stack, embedding, cloze_predictor

def create_length_training_model(params):
  inputs = tf.keras.layers.Input((None,), dtype=tf.int64)

  transformer_stack = TransformerStack(params["num_layers"], params["d_model"],
      params["num_heads"], params["dff"], params["dropout_rate"])
  embedding = Embedding(params["d_model"], params["vocab_size"])
  length_predictor = LengthPredictor(params["d_model"], params["num_heads"], params["dropout_rate"])

  logits, attention_mask = embedding(inputs, mode="embedding")
  logits = transformer_stack([logits, attention_mask])
  length = length_predictor([logits, attention_mask])

  model = tf.keras.Model(inputs, length)

  return model, transformer_stack, embedding, length_predictor

def collect_fn(x):
  c, t, i = x
  collected_c = tf.gather_nd(c, i)
  collected_t = tf.gather_nd(t, i)
  return tf.expand_dims(collected_c, axis=0), tf.expand_dims(collected_t, axis=0)

def match_fn(x):
  c, t = x
  c = tf.argmax(c, axis=-1, output_type=tf.int64)
  return tf.squeeze(tf.not_equal(t, c))

def similarity_fn(x):
  c, t = x
  c = tf.squeeze(c)
  t = tf.squeeze(t)
  return tf.keras.layers.dot([c, t], axes=-1, normalize=True)

def create_cloze_predict_model(params):
  inputs = tf.keras.layers.Input((None,), dtype=tf.int64)
  tar = tf.keras.layers.Input((None,), dtype=tf.int64)

  transformer_stack = TransformerStack(params["num_layers"], params["d_model"],
      params["num_heads"], params["dff"], params["dropout_rate"])
  embedding = Embedding(params["d_model"], params["vocab_size"])
  cloze_predictor = ClozePredictor(params["d_model"], params["num_heads"], params["dff"], params["dropout_rate"])

  logits, attention_mask = embedding(inputs, mode="embedding")
  logits = transformer_stack([logits, attention_mask])
  cloze = cloze_predictor([logits, attention_mask])

  indices = tf.keras.layers.Lambda(lambda t: tf.where(tf.not_equal(t, 0)))(tar)

  collected_c, collected_t = tf.keras.layers.Lambda(collect_fn)([cloze, tar, indices])

  decoded_c = embedding(collected_c, mode="linear")
  match = tf.keras.layers.Lambda(match_fn)([decoded_c, collected_t])

  embedded_t, _ = embedding(collected_t, mode="embedding")

  model = tf.keras.Model([inputs, tar], [match, collected_c, embedded_t])

  return model, transformer_stack, embedding, cloze_predictor

class _Mode:
  TRAINING = "training"

class LengthPredictor(tf.keras.Model):

  def __init__(self, d_model, num_heads, dropout_rate):
    super(LengthPredictor, self).__init__()
    self.d_model = d_model
    self.num_heads = num_heads
    self.dropout_rate = dropout_rate

  def build(self, input_shape):
    self.self_attention = attention_layer.SelfAttention(self.d_model, self.num_heads, self.dropout_rate)
    self.attention_norm = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.dense_0 = tf.keras.layers.Dense(self.d_model, activation=tf.nn.relu)
    self.dense_1 = tf.keras.layers.Dense(1, activation=None)
    self.relu = tf.keras.layers.ReLU()

    super(LengthPredictor, self).build(input_shape)

  def call(self, x, training=None):
    x, attention_mask = x[0], x[1]

    attention_output = self.self_attention(x, attention_mask, training=training)
    if training:
      attention_output = tf.nn.dropout(attention_output, rate=self.dropout_rate)
    attention_output = self.attention_norm(x + attention_output)

    out = self.dense_0(attention_output[:, 0, :])
    if training:
      out = tf.nn.dropout(out, rate=self.dropout_rate)
    out = self.dense_1(out)
    out = self.relu(out)

    return out

class ClozePredictor(tf.keras.Model):

  def __init__(self, d_model, num_heads, dff, dropout_rate):
    super(ClozePredictor, self).__init__()
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate    

  def build(self, input_shape):
    self.layer = EncoderLayer(self.d_model, self.num_heads, self.dff, self.dropout_rate)
    super(ClozePredictor, self).build(input_shape)

  def call(self, x, training=None):
    x, attention_mask = x[0], x[1]
    out = self.layer(x, attention_mask)
    return out

class EncoderLayer(tf.keras.layers.Layer):
  def __init__(self, d_model, num_heads, dff, dropout_rate):
    super(EncoderLayer, self).__init__()
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate

  def build(self, input_shape):
    self.self_attention = attention_layer.SelfAttention(self.d_model, self.num_heads, self.dropout_rate)
    self.attention_norm = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.ffn = ffn_layer.FeedForwardNetwork(self.d_model, self.dff, self.dropout_rate)
    self.ffn_norm = tf.keras.layers.LayerNormalization(epsilon=1e-6)

    super(EncoderLayer, self).build(input_shape)

  def call(self, x, attention_mask, training):
    attention_output = self.self_attention(x, attention_mask, training=training)  # (batch_size, input_seq_len, d_model)
    if training:
      attention_output = tf.nn.dropout(attention_output, rate=self.dropout_rate)
    attention_output = self.attention_norm(x + attention_output)  # (batch_size, input_seq_len, d_model)
    
    ffn_output = self.ffn(attention_output, training=training)  # (batch_size, input_seq_len, d_model)
    if training:
      ffn_output = tf.nn.dropout(ffn_output, rate=self.dropout_rate)
    ffn_output = self.ffn_norm(attention_output + ffn_output)  # (batch_size, input_seq_len, d_model)

    return ffn_output

class Embedding(tf.keras.Model):

  def __init__(self, d_model, vocab_size):
    super(Embedding, self).__init__()
    self.d_model = d_model
    self.vocab_size = vocab_size

  def build(self, input_shape):
    self.embedding_softmax_layer = embedding_layer.EmbeddingSharedWeights(
        self.vocab_size, self.d_model)
    super(Embedding, self).build(input_shape)

  def call(self, x, mode="embedding", training=None):
    if mode == "embedding":
      attention_mask = model_utils.get_padding_bias(x)
      x = self.embedding_softmax_layer(x, mode=mode)
      return x, attention_mask
    elif mode == "linear":
      x = self.embedding_softmax_layer(x, mode=mode)
      return x

class TransformerStack(tf.keras.Model):

  def __init__(self, num_layers, d_model, num_heads, dff, dropout_rate):
    super(TransformerStack, self).__init__()
    self.num_layers = num_layers
    self.d_model = d_model
    self.num_heads = num_heads
    self.dff = dff
    self.dropout_rate = dropout_rate    

  def build(self, input_shape):
    self.layernorm = tf.keras.layers.LayerNormalization(epsilon=1e-6)
    self.attention_layers = [EncoderLayer(self.d_model, self.num_heads, self.dff, self.dropout_rate)
        for _ in range(self.num_layers)]

    super(TransformerStack, self).build(input_shape)

  def call(self, x, training=None):
    x, attention_mask = x[0], x[1]

    pos_encoding = model_utils.get_position_encoding(
        tf.shape(x)[1] - 1,
        self.d_model)
    pos_encoding = tf.pad(pos_encoding, [[1, 0], [0, 0]])
    x += pos_encoding

    if training:
      x = tf.nn.dropout(x, rate=self.dropout_rate)

    x = self.layernorm(x)

    for layer in self.attention_layers:
      x = layer(x, attention_mask, training=training)

    return x
  