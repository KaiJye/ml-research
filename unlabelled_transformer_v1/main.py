import tensorflow as tf
import tensorflow_datasets as tfds
import os, json
from unlabelled_transformer_v1 import unlabelled_transformer as ut
from transformer_v1 import optimizer
from unlabelled_transformer_v1 import evaluate
from unlabelled_transformer_v1 import metrics
from unlabelled_transformer_v1 import data_pipeline

def replacePads(inp):
  select = tf.equal(inp, 0)
  ret = tf.where(select, 1, inp)
  return ret

class MainTask:
  def __init__(self, params, resolver=None, epoch_callback=None):
    self.params = params
    self.epoch_callback = epoch_callback

    if resolver:
      self.distribution_strategy = tf.distribute.experimental.TPUStrategy(resolver)
      self.params['static_batch'] = True
    else:
      self.distribution_strategy = tf.distribute.MirroredStrategy()
      self.params['static_batch'] = False

  def train(self):
    params = self.params

    with self.distribution_strategy.scope():
      model, _, _, _ = ut.create_cloze_training_model(params)
      opt = optimizer.create_optimizer(params)
      current_step = 0
      
      try:
        model.load_weights(params["model_path"])
        print("model loaded")
      except:
        print("create new model")

      try:
        with open(params["train_state_path"]) as json_file:
          train_state = json.load(json_file)
          opt.iterations = tf.Variable(train_state["steps"])
          current_step = opt.iterations.numpy()
      except:
        train_state = {}
        train_state["steps"] = 0
        train_state["loss"] = []
        train_state["accuracy"] = []

      print(f"current train step: {current_step}")

      loss_fn = metrics.loss_function
      accuracy_function = metrics.accuracy_function
      model.compile(optimizer=opt, loss=loss_fn, metrics=[accuracy_function])

    model.summary()

    train_ds = data_pipeline.get_train_ds(params)
    validation_ds = data_pipeline.get_validation_ds(params)

    callbacks = self._create_callbacks()

    current_epoch = 0
    while current_epoch < params['epochs']:
      history = model.fit(train_ds,
          validation_data=validation_ds,
          initial_epoch=current_epoch,
          epochs=current_epoch + 1,
          steps_per_epoch=params['steps_per_epoch'],
          callbacks=callbacks,
          verbose=params['verbose'])

      print(history.history)

      model.save_weights(params["model_path"], save_format='h5')
      train_state["steps"] = int(history.history["steps"][0])
      train_state["loss"].append(float(history.history["loss"][0]))
      train_state["accuracy"].append(float(history.history["accuracy_function"][0]))
      with open(params["train_state_path"], 'w') as json_file:
        json.dump(train_state, json_file)

      if self.epoch_callback:
        self.epoch_callback()

      current_epoch += 1

  def train_length(self):
    params = self.params

    with self.distribution_strategy.scope():
      model, transformer_stack, embedding, length_predictor = ut.create_length_training_model(params)
      opt = optimizer.create_optimizer(params)
      current_step = 0
      
      model_name = params["model_name"]
      model_dir = params["model_dir"]
      
      transformer_stack_name = f"{model_name}_transformer_stack.h5"
      transformer_stack.load_weights(os.path.join(model_dir, transformer_stack_name))
      transformer_stack.trainable = False

      embedding_name = f"{model_name}_embedding.h5"
      embedding.load_weights(os.path.join(model_dir, embedding_name))
      embedding.trainable = False

      try:
        length_predictor_name = f"{model_name}_length_predictor.h5"
        length_predictor.load_weights(os.path.join(model_dir, length_predictor_name))
        print("length_predictor loaded")
      except:
        print("create new length_predictor")

      with open(params["train_state_path"]) as json_file:
        train_state = json.load(json_file)
        if "length_predictor" not in train_state:
          train_state["length_predictor"] = {}
          train_state["length_predictor"]["steps"] = 0
          train_state["length_predictor"]["loss"] = []
          train_state["length_predictor"]["accuracy"] = []
        else:
          opt.iterations = tf.Variable(train_state["length_predictor"]["steps"])
          current_step = opt.iterations.numpy()

      print(f"current train step: {current_step}")

      loss_fn = metrics.loss_function
      accuracy_function = metrics.accuracy_function
      model.compile(optimizer=opt, loss=loss_fn, metrics=[accuracy_function])

    model.summary()

    train_ds = data_pipeline.get_train_ds(params)
    validation_ds = data_pipeline.get_validation_ds(params)

    callbacks = self._create_callbacks()

    current_epoch = 0
    while current_epoch < params['epochs']:
      history = model.fit(train_ds,
          validation_data=validation_ds,
          initial_epoch=current_epoch,
          epochs=current_epoch + 1,
          steps_per_epoch=params['steps_per_epoch'],
          callbacks=callbacks,
          verbose=params['verbose'])

      print(history.history)

      model.save_weights(params["model_path"], save_format='h5')
      train_state["length_predictor"]["steps"] = int(history.history["steps"][0])
      train_state["length_predictor"]["loss"].append(float(history.history["loss"][0]))
      train_state["length_predictor"]["accuracy"].append(float(history.history["accuracy_function"][0]))
      with open(params["train_state_path"], 'w') as json_file:
        json.dump(train_state, json_file)

      if self.epoch_callback:
        self.epoch_callback()

      current_epoch += 1

  def save_weights_0(self):
    params = self.params

    model, transformer_stack, embedding, cloze_predictor = ut.create_cloze_training_model(params)
      
    model.load_weights(params["model_path"])
    print("model loaded, saving")

    model.summary()

    model_name = params["model_name"]
    model_dir = params["model_dir"]
    
    transformer_stack_name = f"{model_name}_transformer_stack.h5"
    transformer_stack.save_weights(
        os.path.join(model_dir, transformer_stack_name), 
        save_format='h5')

    embedding_name = f"{model_name}_embedding.h5"
    embedding.save_weights(
        os.path.join(model_dir, embedding_name), 
        save_format='h5')

    cloze_predictor_name = f"{model_name}_cloze_predictor.h5"
    cloze_predictor.save_weights(
        os.path.join(params["model_dir"], cloze_predictor_name), 
        save_format='h5')

    if self.epoch_callback:
        self.epoch_callback(transformer_stack_name, embedding_name, cloze_predictor_name)

  def predict(self):
    params = self.params

    with self.distribution_strategy.scope():
      model, transformer_stack, embedding, cloze_predictor = ut.create_cloze_predict_model(params)
      
      model_name = params["model_name"]
      model_dir = params["model_dir"]
      
      transformer_stack_name = f"{model_name}_transformer_stack.h5"
      transformer_stack.load_weights(os.path.join(model_dir, transformer_stack_name))

      embedding_name = f"{model_name}_embedding.h5"
      embedding.load_weights(os.path.join(model_dir, embedding_name))

      cloze_predictor_name = f"{model_name}_cloze_predictor.h5"
      cloze_predictor.load_weights(os.path.join(model_dir, cloze_predictor_name))

    model.summary()

    ds = data_pipeline.get_test_ds(params)

    for ex in ds.take(1):
      match, c, t = model(ex, training=False)
      print(match)
      c = tf.squeeze(c)
      t = tf.squeeze(t)
      print(tf.keras.losses.cosine_similarity(c, t))

  def _create_callbacks(self, ckpt_manager=None):
    callbacks = []
    learning_rate_callback = optimizer.LearningRateCallback(self.params)
    callbacks.append(learning_rate_callback)
    # if ckpt_manager:
    #   ckpt_callback = CheckpointCallback(ckpt_manager)
    #   callbacks.append(ckpt_callback)
    return callbacks

class CheckpointCallback(tf.keras.callbacks.Callback):

  def __init__(self, ckpt_manager):
    super(CheckpointCallback, self).__init__()

    self.ckpt_manager = ckpt_manager

  def on_train_batch_end(self, batch, logs=None):
    if self.model.optimizer.iterations.numpy() < 50000:
      return
    if self.model.optimizer.iterations.numpy() % 10000 == 0:
      self.ckpt_manager.save()
      print("checkpoint saved")

