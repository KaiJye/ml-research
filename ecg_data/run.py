from ecg_data import dataset_builder_2, dataset_builder_3, dataset_builder_identification_2
from ecg_data import json_builder, json_builder_2, json_builder_3, json_builder_id_test, dataset_builder
from ecg_data import data_params


# json_builder.build_all_groups(r'D:/data/raw/64/db_person.json', fs=64)
# json_builder.build_all_groups(r'D:/data/raw/128-4/db_person.json', fs=128)
# json_builder_id_test.build_all_groups(r'D:/data/raw/128-4/db_person.json', fs=128, overwrite=True)
# json_builder_2.build_all_groups(r'D:/data/raw/256', fs=256)

build_info = data_params.data_source
# build_info = { 'ptbx': data_params.data_source['ptbx']}
# jb = json_builder_3.JsonBuilder(r'C:/data/raw/128', build_info, fs=128)
# jb.build()
jb = json_builder_3.JsonBuilder(r'C:/data/raw/256', build_info, fs=256)
jb.build()

# included_db = ['apn', 'laf', 'lt', 'mal', 'som', 'sup', 'stp', 'fan', 'ptb', 'ptbx']
# ds_builder = dataset_builder_identification_2.DatasetBuilder('C:/data/raw/128', included_db)
# val_gen = ds_builder.get_gen(ds_builder.included_db, ds_builder.train_pool, 64)
# for ex in val_gen():
#   print(ex)
#   input()

# ds_builder.build_train('C:/data/128')
# ds_builder.build_val('C:/data/128')

# dataset_builder.get_ds('D:/data/raw/64', 'D:/data/64', 
#     min_left=4, max_left=8, min_right=1, max_right=4, max_length=128,
#     train_batch_size=512, train_shards=128, train_entries=16,
#     val_batch_size=512, val_shards=16, val_entries=4)

# dataset_builder.get_ds('D:/data/raw/128-2', 'D:/data/128-2', 
#     min_left=5, max_left=10, min_right=2, max_right=5, max_length=256,
#     train_batch_size=512, train_shards=128, train_entries=8,
#     val_batch_size=512, val_shards=16, val_entries=4)

# dataset_builder.get_ds('D:/data/raw/128-3', 'D:/data/128', 
#     min_left=6, max_left=12, min_right=2, max_right=5, max_length=192,
#     train_batch_size=512, train_shards=128, train_entries=16, num_neg=1,
#     val_batch_size=512, val_shards=16, val_entries=4)

# dataset_builder_identification.get_ds('D:/data/raw/128-2', 'D:/data/128', 
#     min_left=6, max_left=12, min_right=2, max_right=5, max_length=192, max_person=64,
#     train_batch_size=8, train_shards=128, train_entries=16, num_neg=1,
#     val_batch_size=512, val_shards=16, val_entries=4)

# included_db = ['apn', 'laf', 'lt', 'mal', 'som', 'sup', 'stp', 'fan', 'arr', 'id', 'ptb', 'ptbx']
# ds_builder = dataset_builder_3.DatasetBuilder('D:/data/raw/128', included_db)
# ds_builder.build_train('D:/data/128')
# ds_builder.build_val('D:/data/128')
