import os, json, gzip
import numpy as np
from ecg_data import dataset_builder
from math import floor
from itertools import islice
from random import sample, randint, random, choice, choices
from functools import lru_cache
import tensorflow as tf


class DatasetBuilder:
  def __init__(self, data_path, included_db):
    self.data_path = data_path
    self.included_db = included_db

    with open(os.path.join(data_path, 'meta.json')) as file:
        db_meta = json.load(file)
    self.db_meta = {k:v for (k,v) in db_meta.items() if k in self.included_db}

    self.train_pool, self.val_pool = self.get_pools()

    with gzip.open(os.path.join(data_path, 'ptb'), 'rt') as file:
        self.ptb = json.load(file)
    with gzip.open(os.path.join(data_path, 'ptbx'), 'rt') as file:
        self.ptbx = json.load(file)
    with gzip.open(os.path.join(data_path, 'id'), 'rt') as file:
        self.id_db = json.load(file)

  def get_ecgs_of(self, db_name, person_name):
    if db_name == 'ptbx':
      return self.ptbx[person_name]
    elif db_name == 'ptb':
      return self.ptb[person_name]
    elif db_name == 'id':
      return self.id_db[person_name]
    elif db_name == 'apn' or db_name == 'laf' or db_name == 'atr' or db_name == 'som':
      return self.get_other_ecgs_cache(person_name)
    else:
      return self.get_other_ecgs(person_name)

  def get_other_ecgs(self, person_name):
    with gzip.open(os.path.join(self.data_path, person_name)) as file:
      ecgs=json.load(file)
    return ecgs

  # @lru_cache(maxsize = None)
  def get_other_ecgs_cache(self, person_name):
    with gzip.open(os.path.join(self.data_path, person_name)) as file:
      ecgs=json.load(file)
    return ecgs


  def normalize(self, signal):
    signal=np.array(signal, dtype = np.float32)
    return 2.*(signal - np.amin(signal, axis=-1))/np.ptp(signal, axis = -1) - 1

  def get_one_set(self, signal, num_segments, min_length, max_length, padding = 0):
    pos=randint(0, len(signal) - 1)

    segments=[]
    forward_pos=pos
    backward_pos=pos
    forward=True
    backward=True
    while True:
      if forward:
        length=randint(min_length, max_length)
        curr_pos=forward_pos
        forward_pos += length
        if forward_pos < len(signal):
          segment=signal[curr_pos: forward_pos]
          segment=self.normalize(segment)
          segments.append(segment)
          if len(segments) == num_segments:
            break
          forward_pos += padding
        else:
          forward=False

      if backward:
        length=randint(min_length, max_length)
        curr_pos=backward_pos
        backward_pos -= length
        if backward_pos > 0:
          segment=signal[backward_pos: curr_pos]
          segment=self.normalize(segment)
          segments.insert(0, segment)
          if len(segments) == num_segments:
            break
          backward_pos -= padding
        else:
          backward=False

      if not forward and not backward:
        break

    return segments


  def get_positive_pair(self, ecgs, left_segments = 32, right_segments = 5, min_length = 128, max_length = 192):
    right_segments=randint(1, right_segments)
    if len(ecgs) > 1:
      selected=sample(ecgs, 2)
      left=self.get_one_set(selected[0], left_segments, min_length, max_length)
      right=self.get_one_set(selected[1], right_segments, min_length, max_length)
    else:
      ecg=ecgs[0]
      split_pos=int(len(ecg)/3)
      if random() < 0.5:
        left=ecg[:split_pos*2]
        right=ecg[split_pos*2:]
      else:
        left=ecg[split_pos*2:]
        right=ecg[:split_pos*2]

      if len(ecg) < 15 * min_length:
        left=self.get_one_set(left, left_segments, min_length, min_length)
        right=self.get_one_set(right, right_segments, min_length, min_length)
      elif len(ecg) < (left_segments + right_segments) * max_length * 2:
        left=self.get_one_set(left, left_segments, min_length, max_length)
        right=self.get_one_set(right, right_segments, min_length, max_length)
      else:
        left=self.get_one_set(left, left_segments, min_length,
            max_length, min_length)
        right=self.get_one_set(right, right_segments, min_length, max_length)

    return left, right


  def get_negative(self, ecgs, right_segments = 5, min_length = 128, max_length = 192):
    right_segments=randint(1, right_segments)
    if len(ecgs) > 1:
      selected=choice(ecgs)
    else:
      selected=ecgs[0]
    right=self.get_one_set(selected, right_segments, min_length, max_length)

    return right


  def get_gen(self, included_db, pool, max_person):
    padding_value=tf.constant(-1e9, tf.float32)

    def gen():
      while True:
        selected_dbs = choices(included_db, k=max_person)
        selected_persons = set([(db, choice(list(pool[db].keys()))) for db in selected_dbs])
        num_person = len(selected_persons)
        if num_person == max_person:
          break
      positive_pairs = list([self.get_positive_pair(self.get_ecgs_of(db, person)) for (db, person) in selected_persons])
      lefts = list(map(lambda x: x[0], positive_pairs))
      rights = list(map(lambda x: x[1], positive_pairs))

      for i, right in enumerate(rights):
        yield_lefts = list(lefts)
        is_positive = True

        if is_positive:
          yield_rights = list([right for _ in range(num_person)])
        else:
          yield_lefts.pop(i)
          yield_rights = list([right for _ in range(num_person - 1)])

        yield_lefts = tf.ragged.constant(yield_lefts)
        yield_lefts = yield_lefts.to_tensor(default_value=padding_value)
        yield_rights = tf.ragged.constant(yield_rights)
        yield_rights = yield_rights.to_tensor(default_value=padding_value)
        
        if is_positive:
          yield i + 1, yield_lefts, yield_rights
        else:
          yield 0, yield_lefts, yield_rights

    return gen

  def write_to_files(self, ds, output_path, split_name):
    shard_ds=ds.map(dataset_builder.tf_serialize)

    shard=randint(0, 2147483647)

    with tf.io.TFRecordWriter(
            os.path.join(output_path, f"{split_name}-{shard}.tfrecord"),
            tf.io.TFRecordOptions(compression_type='GZIP')) as writer:
      for ex in shard_ds.as_numpy_iterator():
          writer.write(ex)

  def get_pools(self):
    train_pool={}
    val_pool={}
    for db_name, db in self.db_meta.items():
      train_pool[db_name]={}
      val_pool[db_name]={}
      cutoff=floor(len(db.keys())*0.9)
      i=iter(db.items())
      train_pool[db_name]=dict(islice(i, cutoff))
      val_pool[db_name]=dict(i)
    return train_pool, val_pool


  def build_train(self, output_path, batch_size=8, max_left=32, max_right=5, max_length=192, max_person=64):
    train_gen=self.get_gen(self.included_db, self.train_pool, max_person)
    train_ds=tf.data.Dataset.from_generator(
        train_gen, 
        (tf.int64, tf.float32, tf.float32), 
        output_shapes=([], [None, None, None], [None, None, None]),
    )
    padding_value = tf.constant(-1e9, tf.float32)
    
    while True:
      shard_ds = train_ds.padded_batch(batch_size,
          padded_shapes=([], [max_person, max_left, max_length], [max_person, max_right, max_length]),
          padding_values=(tf.constant(0, tf.int64), padding_value, padding_value),
          drop_remainder=True)
      self.write_to_files(shard_ds, output_path, 'train')

  def build_val(self, output_path, batch_size=8, max_left=32, max_right=5, max_length=192, max_person=64):
    val_gen = self.get_gen(self.included_db, self.train_pool, max_person)
    val_ds = tf.data.Dataset.from_generator(
        val_gen, 
        (tf.int64, tf.float32, tf.float32), 
        output_shapes=([], [None, None, None], [None, None, None]),
    )
    padding_value = tf.constant(-1e9, tf.float32)
    
    for _ in range(16):
      shard_ds = val_ds.padded_batch(batch_size,
          padded_shapes=([], [max_person, max_left, max_length], [max_person, max_right, max_length]),
          padding_values=(tf.constant(0, tf.int64), padding_value, padding_value),
          drop_remainder=True)
      self.write_to_files(shard_ds, output_path, 'val')
 

