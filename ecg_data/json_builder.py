import wfdb
from wfdb import processing
import json, csv
import os
from os import path
from matplotlib import pyplot as plt
import numpy as np
import tensorflow as tf
from random import choice, sample, randint
from ecg_data import data_params

def insert_signals(group, signals, key_prefix, name):
  key = f'{key_prefix}-{name}'
  signals = list(signals)
  if key not in group:
    group[key] = signals
  else:
    group[key].extend(signals)

def process_record(record_entry, data_dir, key_prefix, fs=128, channel=0, max_sample_size=128):
  record_entry = record_entry.strip()
  print(f'{key_prefix}: {record_entry}')
  record = wfdb.rdrecord(path.join(data_dir, record_entry))
  signal = record.p_signal[:, channel]

  # print(record_entry)
  # print(record.__dict__)
  # wfdb.plot_wfdb(record=record)
  # wfdb.plot_items(signal) 
  # input()

  signal = tf.gather_nd(signal, tf.where(tf.math.is_finite(signal)))

  if not tf.reduce_all(tf.math.is_finite(signal)):
    print(f'not finite: {key_prefix} - {record_entry}')
    print(tf.where(tf.math.is_finite(signal)))
    wfdb.plot_items(signal) 
  
  signal, _ = processing.resample_sig(signal.numpy(), record.fs, fs)

  gen = signal_gen(signal, fs * 1.0)
  # if key_prefix == 'ptbx':
  #   gen = signal_gen(signal, fs * 1.0)
  # else:
  #   gen = signal_gen(signal, fs * 1.0, fs * 1.5)
  batched_signals = list(gen)

  if len(batched_signals) >= max_sample_size:
    batched_signals = sample(batched_signals, max_sample_size)

  signals = []

  for s in batched_signals:
    # normalize
    s = processing.normalize_bound(s, lb=-1, ub=1)
    if not np.isfinite(np.sum(np.array(s))):
      print(f'skip not finite: {key_prefix}: {record_entry}')
      continue
    
    # standardize
    # m = np.mean(s)
    # d = np.std(s)
    # s = (s - m) / d
    # wfdb.plot_items(s) 
    signals.append(s.tolist())

  return signals

def signal_gen(signal, lower, upper=None):
  index = 0
  total_len = len(signal)
  while True:
    if upper is None:
      size = int(lower)
    else:
      size = randint(lower, upper)
    end = index + size
    if end >= total_len:
      if total_len - index >= lower:
        yield signal[index:] 
      break
    yield signal[index:end] 
    index = end

def build_group(data_dir, key_prefix, fs=128, channel=0, max_sample_size=128):
  group = {}

  if key_prefix == 'ptbx':
    with open(path.join(data_dir, 'ptbxl_database.csv')) as csvfile:
      reader = csv.DictReader(csvfile)
      rows = list(reader)
    for record_entry in rows:
      signals = process_record(record_entry['filename_hr'], data_dir, key_prefix, fs, channel, max_sample_size)
      insert_signals(group, signals, key_prefix, record_entry['patient_id'])
  else:
    with open(path.join(data_dir, 'RECORDS')) as record_file:
      records = list(record_file)
    for record_entry in records:
      signals = process_record(record_entry, data_dir, key_prefix, fs, channel, max_sample_size)
      if key_prefix == 'ptb' or key_prefix == 'id':
        name = record_entry.split('/')[0]
      elif key_prefix == 'com':
        name = name = record_entry[1:]
      else:
        name = record_entry
      insert_signals(group, signals, key_prefix, name)

  for signals in group.values():
    if len(signals) > max_sample_size:
      signals = sample(signals, max_sample_size)

  return group

def build_all_groups(save_dir, fs=128, overwrite=False):
  try:
    with open(save_dir) as json_file:
      all_groups = json.load(json_file)
  except:
    all_groups = {}

  for data_meta in data_params.data_source.values():
    key_prefix = data_meta['prefix']

    if overwrite or key_prefix not in all_groups:
      data_dir = data_meta['dir']
      channel = data_meta['channel']
      group = build_group(data_dir, key_prefix, fs=fs, channel=channel, max_sample_size=512)
      all_groups[key_prefix] = group
      print(f'{key_prefix}: {len(group)}')

      with open(save_dir, 'w') as json_file:
        json.dump(all_groups, json_file)
    
    else:
      print(f'{key_prefix} already exists')

