import os, json, gzip
import scipy.signal
import numpy as np
from common import dataset_utils
from itertools import islice
from random import randrange, sample, randint, random, choice, shuffle
import tensorflow as tf

def _tf_serialize(label, d0, d1):
  tf_string = tf.py_function(_serialize, (label, d0, d1), tf.string)
  return tf.reshape(tf_string, ())

def _serialize(label, d0, d1):
  feature = {
      'label': dataset_utils.int64_feature([label.numpy()]),
      'd0': dataset_utils.byte_feature([tf.io.serialize_tensor(d0).numpy()]),
      'd1': dataset_utils.byte_feature([tf.io.serialize_tensor(d1).numpy()]),
  }
  example_proto = tf.train.Example(features=tf.train.Features(feature=feature))
  return example_proto.SerializeToString()

def _normalize(signal):
  signal=np.array(signal, dtype=np.float32)
  return (signal - np.mean(signal)) / np.std(signal)
  # return 2.*(signal - np.amin(signal, axis=-1))/np.ptp(signal, axis=-1) - 1
  # return signal / np.mean(signal)

def _get_items_in_db(pool: dict, db_name: str, special_picks: list = None):
  if not db_name.endswith('_'):
    db_name = f'{db_name}_'
  if special_picks == None:
    f = filter(lambda x: x[0].startswith(db_name), pool.items())
  else:
    f = filter(lambda x: x[0].startswith(db_name) and x[0][len(db_name):] in special_picks, pool.items())
  return f

def _get_names_in_db(pool: dict, db_name: str):
  if not db_name.endswith('_'):
    db_name = f'{db_name}_'
  f = filter(lambda x: x.startswith(db_name), pool.keys())
  return f

def _filter(data, filter_a, filter_b):
  output_signal = scipy.signal.filtfilt(filter_a, filter_b, data)
  return output_signal

def _random_filter(data, l_n=4, u_n=5, l_wn=0.6, u_wn=0.8):
  filter_a, filter_b = scipy.signal.butter(randint(l_n, u_n), [.01, (random()*(u_wn-l_wn))+l_wn], 'bandpass')
  return _filter(data, filter_a, filter_b)

def _get_db_len(pool: dict, db_name: str):
  names = list(_get_names_in_db(pool, db_name))
  return len(names)

def _get_chunk(ecg, length):
  ecg_len = len(ecg)
  if ecg_len < length:
    raise Exception('ecg is too short')
  pos = randint(0, ecg_len - length)
  return ecg[pos:pos+length]

def _split_chunk(ecg, size, length, filter_a, filter_b):
  indices = np.linspace(0, len(ecg)-length, size)
  for i in indices:
    int_i = int(i)
    yield _normalize(_filter(ecg[int_i:int_i+length], filter_a, filter_b))

def _split_chunk_random(ecg, size, length, filter_a, filter_b):
  chunk = _get_chunk(ecg, size*length)
  i = 0
  end = length
  while end <= len(chunk):
    yield _normalize(_filter(chunk[i:end], filter_a, filter_b))
    i += length
    end += length

class DatasetBuilder:

  def __init__(self, data_path, output_path, included_db, fs=128, filter_n=1, filter_wn=[.01, .4], **kwargs):
    self._data_path = data_path
    self._output_path = output_path
    self._fs = fs
    self._padding_value=tf.constant(-1e9, tf.float32)
    self._filter_a, self._filter_b = scipy.signal.butter(filter_n, filter_wn, 'bandpass')
    self._ptb_special_picks = kwargs.get("ptb_special_picks", None)
    self._id_special_picks = kwargs.get("id_special_picks", None)

    with open(os.path.join(data_path, 'meta.json')) as file:
      self._db_meta = json.load(file)

    with gzip.open(os.path.join(data_path, 'ptbx'), 'rt') as file:
      self._ptbx = json.load(file)

    self._train_pool = {}
    self._val_pool = {}
    self._train_pool_db = []
    self._val_pool_db = []
    for db_name in included_db:
      it = _get_items_in_db(self._db_meta, db_name)

      if _get_db_len(self._db_meta, db_name) >= 64:
        self._val_pool.update(dict(islice(it, 32)))
        self._val_pool_db.append(db_name)
      elif _get_db_len(self._db_meta, db_name) >= 32:
        self._val_pool.update(dict(islice(it, 16)))
        self._val_pool_db.append(db_name)
      elif _get_db_len(self._db_meta, db_name) >= 16:
        self._val_pool.update(dict(islice(it, 8)))
        self._val_pool_db.append(db_name)
      elif _get_db_len(self._db_meta, db_name) >= 8:
        self._val_pool.update(dict(islice(it, 4)))
        self._val_pool_db.append(db_name)

      self._train_pool.update(dict(it))
      self._train_pool_db.append(db_name)

  def _get_names_in_train_pool(self, db_name: str):
    return _get_names_in_db(self._train_pool, db_name)

  def _get_names_in_val_pool(self, db_name: str):
    return _get_names_in_db(self._val_pool, db_name)

  def _get_ecg_of(self, file_name: str):
    if file_name.startswith('ptbx_'):
      splits = file_name.split('_')
      i = int(splits[-1])
      key = '_'.join(splits[:-1])
      return self._ptbx[key][i]
    else:
      with gzip.open(os.path.join(self._data_path, file_name)) as file:
        ecg=json.load(file)
      return ecg

  def _filter(self, data):
    output_signal = _filter(data, self._filter_a, self._filter_b)
    return output_signal

  def _get_one(self, name, length):
    ecgs = list(self._db_meta[name])
    num_ecgs = len(ecgs)
    if num_ecgs > 1:
      file_name = choice(ecgs)
    else:
      file_name = ecgs[0]
    ecg = self._get_ecg_of(file_name)
    chunk = _get_chunk(ecg, length)
    chunk = self._filter(chunk)
    chunk = _normalize(chunk)
    return chunk

  def _get_pair(self, name, length):
    ecgs = list(self._db_meta[name])
    num_ecgs = len(ecgs)
    if num_ecgs > 2:
      shuffle(ecgs)
      file_names = sample(ecgs, 2)
      file_name_0, file_name_1 = file_names[0], file_names[1]
      ecg_0 = self._get_ecg_of(file_name_0)
      chunk_0 = _get_chunk(ecg_0, length)
      ecg_1 = self._get_ecg_of(file_name_1)
      chunk_1 = _get_chunk(ecg_1, length)
    else:
      ecg = self._get_ecg_of(ecgs[0])
      split_pos = int(len(ecg)/2)
      if random() > 0.5:
        chunk_0 = _get_chunk(ecg[:split_pos], length)
        chunk_1 = _get_chunk(ecg[split_pos:], length)
      else:
        chunk_0 = _get_chunk(ecg[split_pos:], length)
        chunk_1 = _get_chunk(ecg[:split_pos], length)

    chunk_0 = self._filter(chunk_0)
    chunk_1 = self._filter(chunk_1)
    chunk_0 = _normalize(chunk_0)
    chunk_1 = _normalize(chunk_1)
    return chunk_0, chunk_1

  def _get_gen(self, train, max_size, length):
    if train:
      included_db = self._train_pool_db
      get_names_fn = self._get_names_in_train_pool
    else:
      included_db = self._val_pool_db
      get_names_fn = self._get_names_in_val_pool

    def gen():
      while True:
        # size = randint(16, max_size)
        size = max_size

        if random() > 0.5:
          db_name = choice(included_db)
          names = list(get_names_fn(db_name))
          if len(names) > size:
            names = sample(names, size)
          shuffle(names)
          the_one = choice(names)
          while len(names) < size:
            while True:
              other_db_name = choice(included_db)
              if other_db_name != db_name:
                break
            other_names = list(get_names_fn(other_db_name))
            while True:
              other_name = choice(other_names)
              if other_names not in names:
                break
            names.append(other_name)
        else:
          names = []
          while len(names) < size:
            other_db_name = choice(included_db)
            other_names = list(get_names_fn(other_db_name))
            other_name = choice(other_names)
            if other_names not in names:
              names.append(other_name)
          the_one = choice(names)

        templates = []
        index = None
        candidate = None
        
        try:
          for i, name in enumerate(names):
            if name == the_one:
              template, candidate = self._get_pair(name, length)
              index = i
            else:
              template = self._get_one(name, length)
            templates.append(template)
        except:
          print('no ecg')
          continue

        if index is not None and candidate is not None:
          templates: tf.RaggedTensor = tf.ragged.constant(templates)
          templates = templates.to_tensor(default_value=self._padding_value)

          if tf.reduce_all(tf.math.is_finite(templates)) and tf.reduce_all(tf.math.is_finite(candidate)):
            yield index, candidate, templates
          else:
            print('not finite')
        
        else:
          print('no candidate')
    
    return gen
    
  def _write_to_files(self, ds, split_name):
    shard_ds=ds.map(_tf_serialize)
    shard=randint(0, 2147483647)
    with tf.io.TFRecordWriter(
            os.path.join(self._output_path, f"{split_name}-{shard}.tfrecord"),
            tf.io.TFRecordOptions(compression_type='GZIP')) as writer:
      for ex in shard_ds.as_numpy_iterator():
          writer.write(ex)

  def build_train(self, size=32, length=1.5, shards=512, num_per_shard=2048):
    train_gen = self._get_gen(True, size, int(length*self._fs))
    train_ds = tf.data.Dataset.from_generator(
        train_gen, 
        (tf.int64, tf.float32, tf.float32), 
        output_shapes=([], [None], [None, None]),
    )
    for i in range(shards):
      print(f'processing: {i}')
      shard_ds = train_ds.take(num_per_shard)
      self._write_to_files(shard_ds, 'train')

  def build_val(self, size=24, length=1.5, shards=8, num_per_shard=1024):
    val_gen = self._get_gen(False, size, int(length*self._fs))
    val_ds = tf.data.Dataset.from_generator(
        val_gen, 
        (tf.int64, tf.float32, tf.float32), 
        output_shapes=([], [None], [None, None]),
    )
    for i in range(shards):
      print(f'processing: {i}')
      shard_ds = val_ds.take(num_per_shard)
      self._write_to_files(shard_ds, 'val')

  def get_test(self, db_name, batch_size=8, p_0=1, p_1=2, template_size=16, candidate_size=1, length=1.5):
    length = int(length*self._fs)
    if db_name == 'ptb':
      all_items = _get_items_in_db(self._db_meta, db_name, self._ptb_special_picks)
    elif db_name == 'id':
      all_items = _get_items_in_db(self._db_meta, db_name, self._id_special_picks)
    else:
      all_items = _get_items_in_db(self._db_meta, db_name)
    templates = []
    candidates = []
    for _, v in all_items:
      template_ecg = self._get_ecg_of(v[p_0])
      template = list(_split_chunk(template_ecg, template_size, length, self._filter_a, self._filter_b))
      candidate_ecg = self._get_ecg_of(v[p_1])
      candidate = list(_split_chunk_random(candidate_ecg, candidate_size, length, self._filter_a, self._filter_b))
      templates.append(template)
      candidates.append(candidate)

    for template in templates:
      shuffle(template)
    
    pool_size = len(templates)

    templates = tf.ragged.constant(templates)
    templates = templates.to_tensor(default_value=self._padding_value)
    candidates = tf.ragged.constant(candidates)
    candidates = candidates.to_tensor(default_value=self._padding_value)

    options = tf.data.Options()
    options.experimental_distribute.auto_shard_policy = tf.data.experimental.AutoShardPolicy.DATA
    for i, candidate in enumerate(candidates):
      def positive_gen():
        for ci in range(candidate_size):
          for ti in range(template_size):
            yield candidate[ci, :], templates[:, ti, :]
      positive_ds = tf.data.Dataset.from_generator(
          positive_gen, 
          (tf.float32, tf.float32), 
          output_shapes=([length], [pool_size, length]),
      )
      positive_ds = positive_ds.batch(batch_size).with_options(options)

      n_templates = tf.unstack(templates)
      del n_templates[i]
      n_templates = tf.stack(n_templates)
      def negative_gen():
        for ci in range(candidate_size):
          for ti in range(template_size):
            yield candidate[ci, :], n_templates[:, ti, :]
      negative_ds = tf.data.Dataset.from_generator(
          negative_gen, 
          (tf.float32, tf.float32), 
          output_shapes=([length], [pool_size-1, length]),
      )
      negative_ds = negative_ds.batch(batch_size).with_options(options)

      yield positive_ds, negative_ds
