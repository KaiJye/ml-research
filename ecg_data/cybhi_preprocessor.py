import numpy as np
import os
import pandas as pd
import datetime
import matplotlib.pyplot as plt
import json
import scipy
import scipy.signal as signal
from wfdb import processing

root_path = "D:\data\ecg\CYBHi\CYBHi"
long_term_data_folder_path = os.path.join(root_path, "data", "long-term")
short_term_data_folder_path = os.path.join(root_path, "data", "short-term")
timestamp_header = "# StartDateTime: "
sampling_frequency_header = "# SamplingFrequency: "
end_of_header = "# EndOfHeader"
resample_frequency = 128

def _min_max_normalize(signal):
    return 2.*(signal - np.amin(signal, axis=-1))/np.ptp(signal, axis=-1) - 1

def _normalize(signal):
    signal=np.array(signal, dtype=np.float32)
    return (signal - np.mean(signal)) / np.std(signal)

def _filter(data, filter_n=5, filter_wn=[1, 50], fs=128):
    sos = signal.butter(filter_n, filter_wn, fs=fs, btype='bandpass', output='sos')
    output_signal = signal.sosfilt(sos, data)
    return output_signal

def _downsample(data, original_frequency, target_frequency):
    ratio = original_frequency / target_frequency
    previous = original_frequency

    new_data = []

    for index, d in enumerate(data):
        mod = index % ratio
        if previous > mod:
            new_data.append(d)
        previous = mod

    return np.array(new_data)

def _downsample_mean(data, original_frequency, target_frequency):
    ratio = original_frequency / target_frequency
    previous = original_frequency

    new_data = []

    for index in range(len(data)):
        mod = index % ratio
        if previous > mod:
            start = index - int(ratio)
            new_data.append(np.median(data[start:index]))
        previous = mod

    return np.array(new_data)

def read_long_term_file(path):
    start_data = False
    data = []
    
    with open(path, 'r') as file:
        for line in file:
            if start_data:
                data_str = line.strip("\n")
                data.append(int(data_str))
            elif timestamp_header in line:
                timestamp_str = line[line.index(timestamp_header) + len(timestamp_header):]
                timestamp_str = timestamp_str.strip("\n")
                timestamp = datetime.datetime.strptime(timestamp_str, "%Y-%m-%d %H:%M:%S")
            elif sampling_frequency_header in line:
                sampling_frequency_str = line[line.index(sampling_frequency_header) + len(sampling_frequency_header):]
                sampling_frequency_str.strip("\n")
                sampling_frequency = int(sampling_frequency_str)
            elif end_of_header in line:
                start_data = True
    
    return np.array(data), timestamp, sampling_frequency


def read_short_term_file(path, column):
    start_data = False
    data = []
    
    with open(path, 'r') as file:
        for line in file:
            if start_data:
                data_str = line.strip("\n")
                data_split = data_str.split("\t")
                data.append(int(data_split[column]))
            elif timestamp_header in line:
                timestamp_str = line[line.index(timestamp_header) + len(timestamp_header):]
                timestamp_str = timestamp_str.strip("\n")
                timestamp = datetime.datetime.strptime(timestamp_str, "%Y-%m-%d %H:%M:%S")
            elif sampling_frequency_header in line:
                sampling_frequency_str = line[line.index(sampling_frequency_header) + len(sampling_frequency_header):]
                sampling_frequency_str.strip("\n")
                sampling_frequency = int(sampling_frequency_str)
            elif end_of_header in line:
                start_data = True

    return np.array(data), timestamp, sampling_frequency


def process_long_term_subject(subject):
    files = []

    path = os.path.join(long_term_data_folder_path, f"{subject['T1 File']}-A0-35.txt")
    data, timestamp, sampling_frequency = read_long_term_file(path)
    data, _ = processing.resample_sig(data, sampling_frequency, resample_frequency)
    file_path = f"long_{subject['Subject']}_0.npy"
    np.save(os.path.join("generated", file_path), data)
    files.append(file_path)
    
    path = os.path.join(long_term_data_folder_path, f"{subject['T2 File']}-A0-35.txt")
    data, timestamp, sampling_frequency = read_long_term_file(path)
    data, _ = processing.resample_sig(data, sampling_frequency, resample_frequency)
    file_path = f"long_{subject['Subject']}_1.npy"
    np.save(os.path.join("generated", file_path), data)
    files.append(file_path)

    meta = {
        "name": subject['Subject'],
        "files": files
    }

    return meta


def process_long_term():
    xls_file_path = os.path.join(root_path, "documents", "Participants List A0.xls")

    subject_metas = []

    df = pd.read_excel(xls_file_path)

    for index, row in df.iterrows():
        subject_meta = process_long_term_subject(row)

        subject_metas.append(subject_meta)

    with open(os.path.join("generated", "cybhi_long.json"), 'w') as file:
        json.dump({ "subjects": subject_metas }, file)


def process_short_term_subject(subject):
    palm_files = []

    path = os.path.join(short_term_data_folder_path, f"{subject['File']}-CI-8B.txt")
    data, timestamp, sampling_frequency = read_short_term_file(path, 3)
    data, _ = processing.resample_sig(data, sampling_frequency, resample_frequency)
    file_path = f"short_palm_{subject['File']}_0.npy"
    np.save(os.path.join("generated", file_path), data)
    palm_files.append(file_path)
    
    path = os.path.join(short_term_data_folder_path, f"{subject['File']}-A1-8B.txt")
    data, timestamp, sampling_frequency = read_short_term_file(path, 3)
    data, _ = processing.resample_sig(data, sampling_frequency, resample_frequency)
    file_path = f"short_palm_{subject['File']}_1.npy"
    np.save(os.path.join("generated", file_path), data)
    palm_files.append(file_path)
    
    path = os.path.join(short_term_data_folder_path, f"{subject['File']}-A2-8B.txt")
    data, timestamp, sampling_frequency = read_short_term_file(path, 3)
    data, _ = processing.resample_sig(data, sampling_frequency, resample_frequency)
    file_path = f"short_palm_{subject['File']}_2.npy"
    np.save(os.path.join("generated", file_path), data)
    palm_files.append(file_path)
    
    palm_meta = {
        "name": subject['File'],
        "files": palm_files
    }

    finger_files = []

    path = os.path.join(short_term_data_folder_path, f"{subject['File']}-CI-85.txt")
    data, timestamp, sampling_frequency = read_short_term_file(path, 3)
    data, _ = processing.resample_sig(data, sampling_frequency, resample_frequency)
    file_path = f"short_finger_{subject['File']}_0.npy"
    np.save(os.path.join("generated", file_path), data)
    finger_files.append(file_path)
    
    path = os.path.join(short_term_data_folder_path, f"{subject['File']}-A1-85.txt")
    data, timestamp, sampling_frequency = read_short_term_file(path, 3)
    data, _ = processing.resample_sig(data, sampling_frequency, resample_frequency)
    file_path = f"short_finger_{subject['File']}_1.npy"
    np.save(os.path.join("generated", file_path), data)
    finger_files.append(file_path)
    
    path = os.path.join(short_term_data_folder_path, f"{subject['File']}-A2-85.txt")
    data, timestamp, sampling_frequency = read_short_term_file(path, 3)
    data, _ = processing.resample_sig(data, sampling_frequency, resample_frequency)
    file_path = f"short_finger_{subject['File']}_2.npy"
    np.save(os.path.join("generated", file_path), data)
    finger_files.append(file_path)
    
    finger_meta = {
        "name": subject['File'],
        "files": finger_files
    }

    return palm_meta, finger_meta


def process_short_term():
    xls_file_path = os.path.join(root_path, "documents", "Participants List CI-A1-A2.xls")

    palm_metas = []
    finger_metas = []

    df = pd.read_excel(xls_file_path)

    for index, row in df.iterrows():
        palm_meta, finger_meta = process_short_term_subject(row)

        palm_metas.append(palm_meta)
        finger_metas.append(finger_meta)

    with open(os.path.join("generated", "cybhi_short_palm.json"), 'w') as file:
        json.dump({ "subjects": palm_metas }, file)

    with open(os.path.join("generated", "cybhi_short_finger.json"), 'w') as file:
        json.dump({ "subjects": finger_metas }, file)


def main():
    process_long_term()

    process_short_term()


if __name__ == "__main__":
    main()