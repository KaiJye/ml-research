import os, json, gzip
import numpy as np
from ecg_data import dataset_builder
from math import floor
from itertools import islice
from random import sample, randint, random, choice
from functools import lru_cache
import tensorflow as tf


class DatasetBuilder:
  def __init__(self, data_path, included_db, fs=128, pool_size=8):
    self.data_path = data_path
    self.included_db = included_db
    self.fs = fs
    self.pool_size = pool_size

    with open(os.path.join(data_path, 'meta.json')) as file:
      db_meta = json.load(file)
    self.db_meta = {k:v for (k,v) in db_meta.items() if k in self.included_db}

    self.train_pool, self.val_pool = self.get_pools()

    with gzip.open(os.path.join(data_path, 'ptb'), 'rt') as file:
      self.ptb = json.load(file)
    with gzip.open(os.path.join(data_path, 'ptbx'), 'rt') as file:
      self.ptbx = json.load(file)
    with gzip.open(os.path.join(data_path, 'id'), 'rt') as file:
      self.id_db = json.load(file)

  def get_ecgs_of(self, db_name, person_name):
    if db_name == 'ptbx':
      return self.ptbx[person_name]
    elif db_name == 'ptb':
      return self.ptb[person_name]
    elif db_name == 'id':
      return self.id_db[person_name]
    elif db_name == 'apn' or db_name == 'laf' or db_name == 'atr' or db_name == 'som':
      return self.get_other_ecgs_cache(person_name)
    else:
      return self.get_other_ecgs(person_name)

  def get_other_ecgs(self, person_name):
    with gzip.open(os.path.join(self.data_path, person_name)) as file:
      ecgs=json.load(file)
    return ecgs

  # @lru_cache(maxsize = 64)
  def get_other_ecgs_cache(self, person_name):
    with gzip.open(os.path.join(self.data_path, person_name)) as file:
      ecgs=json.load(file)
    return ecgs


  def normalize(self, signal):
    signal=np.array(signal, dtype = np.float32)
    return 2.*(signal - np.amin(signal, axis=-1))/np.ptp(signal, axis = -1) - 1

  def get_one_set(self, signal, num_segments, min_length, max_length, padding = 0):
    pos=randint(0, len(signal) - 1)

    segments=[]
    forward_pos=pos
    backward_pos=pos
    forward=True
    backward=True
    while True:
      if forward:
        length=randint(min_length, max_length)
        curr_pos=forward_pos
        forward_pos += length
        if forward_pos < len(signal):
          segment=signal[curr_pos: forward_pos]
          segment=self.normalize(segment)
          segments.append(segment)
          if len(segments) == num_segments:
            break
          forward_pos += padding
        else:
          forward=False

      if backward:
        length=randint(min_length, max_length)
        curr_pos=backward_pos
        backward_pos -= length
        if backward_pos > 0:
          segment=signal[backward_pos: curr_pos]
          segment=self.normalize(segment)
          segments.insert(0, segment)
          if len(segments) == num_segments:
            break
          backward_pos -= padding
        else:
          backward=False

      if not forward and not backward:
        break

    return segments


  def get_positive_pair(self, ecgs, left_segments, right_segments, min_length, max_length):
    # right_segments=randint(2, right_segments)
    if len(ecgs) > 1:
      selected=sample(ecgs, 2)
      left=self.get_one_set(selected[0], left_segments, min_length, max_length)
      right=self.get_one_set(selected[1], right_segments, min_length, max_length)
    else:
      ecg=ecgs[0]
      ecg_length = len(ecg)
      req_length = (left_segments+right_segments)*max_length*2
      if ecg_length > req_length:
        pos = randint(0, ecg_length - req_length)
        ecg = ecg[pos:pos+req_length]
      split_pos=int(len(ecg)*left_segments/(left_segments+right_segments))
      if random() < 0.5:
        left=ecg[:split_pos]
        right=ecg[split_pos:]
      else:
        left=ecg[-1*split_pos:]
        right=ecg[:-1*split_pos]

      if len(ecg) < 15 * min_length:
        left=self.get_one_set(left, left_segments, min_length, min_length)
        right=self.get_one_set(right, right_segments, min_length, min_length)
      elif len(ecg) < (left_segments + right_segments) * max_length * 2:
        left=self.get_one_set(left, left_segments, min_length, max_length)
        right=self.get_one_set(right, right_segments, min_length, max_length)
      else:
        left=self.get_one_set(left, left_segments, min_length,
            max_length, padding=0)
        right=self.get_one_set(right, right_segments, min_length, max_length)

    return left, right


  def get_negative(self, ecgs, right_segments, min_length, max_length):
    # right_segments=randint(2, right_segments)
    if len(ecgs) > 1:
      selected=choice(ecgs)
    else:
      selected=ecgs[0]
    right=self.get_one_set(selected, right_segments, min_length, max_length)

    return right


  def get_id_gen(self, ecgs, num_left, num_right, length):
    padding_value = tf.constant(-1e9, tf.float32)

    person_left = []
    person_right = []
    for v in ecgs.values():
      positive_left, positive_right = self.get_positive_pair(
          v, 
          left_segments=num_left, right_segments=num_right, 
          min_length=length, max_length=length
      )
          
      positive_left = tf.ragged.constant(positive_left)
      positive_right = tf.ragged.constant(positive_right)
      positive_left = positive_left.to_tensor(default_value = padding_value)
      positive_right = positive_right.to_tensor(default_value = padding_value)
      
      person_left.append(positive_left)
      person_right.append(positive_right)

    person_left = tf.stack(person_left)
    total_person = len(ecgs)

    def gen():
      for i in range(total_person):
        yield i, (person_left, tf.repeat(person_right[i][tf.newaxis, :, :], total_person, axis=0))
    
    return gen()


  def get_gen_with_keys(self, ecgs, num_left, num_right, length):
    padding_value=tf.constant(-1e9, tf.float32)

    def gen():
      for key in ecgs.keys():
        for _ in range(10):
          positive_ecgs=ecgs[key]
          positive_left, positive_right = self.get_positive_pair(
              positive_ecgs, 
              left_segments=num_left, right_segments=num_right, 
              min_length=length, max_length=length
          )

          if len(positive_left) < 0:
            print(f'{key} has no +ve left')
          if len(positive_right) < 0:
            print(f'{key} has no +ve right')

          positive_left=tf.ragged.constant(positive_left)
          positive_right=tf.ragged.constant(positive_right)
          positive_left= positive_left.to_tensor(default_value = padding_value)
          positive_right= positive_right.to_tensor(default_value = padding_value)

          if tf.logical_not(tf.reduce_all(tf.math.is_finite(positive_left))):
            print(f'{key} has not finite +ve left')
          if tf.logical_not(tf.reduce_all(tf.math.is_finite(positive_right))):
            print(f'{key} has not finite +ve right')
          yield 1, positive_left, positive_right

          while True:
            other_key = choice(list(ecgs.keys()))
            if other_key != key:
              break

          negative_ecgs=ecgs[other_key]
          negative_right = self.get_negative(negative_ecgs, right_segments=num_right, min_length=length, max_length=length)

          if len(negative_right) < 0:
            print(f'{other_key} has no -ve right')

          negative_right=tf.ragged.constant(negative_right)
          negative_right= negative_right.to_tensor(default_value = padding_value)

          if tf.logical_not(tf.reduce_all(tf.math.is_finite(negative_right))):
            print(f'{other_key} has not finite +ve right')

          yield 0, positive_left, negative_right

    return gen


  def get_id_gen_vote(self, ecgs, num_left, num_right, length):
    padding_value = tf.constant(-1e9, tf.float32)

    person_left = []
    person_right = []
    for v in ecgs.values():
      while True:
        positive_left, positive_right = self.get_positive_pair(
            v, 
            left_segments=num_left, right_segments=num_right, 
            min_length=length, max_length=length
        )

        if len(positive_left) == num_left and len(positive_right) == num_right:
          break

      left = []
      right = []
      for l in positive_left:
        for r in positive_right:
          left.append([l])
          right.append([r])
          
      left=tf.ragged.constant(left)
      left=left.to_tensor(default_value=padding_value)
      right=tf.ragged.constant(right)
      right= right.to_tensor(default_value=padding_value)
      
      person_left.append(left)
      person_right.append(right)

    person_left = tf.stack(person_left)
    total_person = len(ecgs)

    def gen():
      for i in range(total_person):
        yield i, (person_left, tf.repeat(person_right[i][tf.newaxis, :, :, :], total_person, axis=0))
    
    return gen()


  def get_gen_with_keys_vote(self, ecgs, num_left, num_right, length):
    padding_value=tf.constant(-1e9, tf.float32)

    def gen():
      for key in ecgs.keys():
        for _ in range(10):
          positive_ecgs=ecgs[key]
          positive_left, positive_right = self.get_positive_pair(
              positive_ecgs, 
              left_segments=num_left, right_segments=num_right, 
              min_length=length, max_length=length
          )

          if len(positive_left) < 0:
            print(f'{key} has no +ve left')
          if len(positive_right) < 0:
            print(f'{key} has no +ve right')

          left = []
          right = []
          for l in positive_left:
            for r in positive_right:
              left.append([l])
              right.append([r])

          left=tf.ragged.constant(left)
          left= left.to_tensor(default_value=padding_value)
          right=tf.ragged.constant(right)
          right= right.to_tensor(default_value=padding_value)

          if tf.logical_not(tf.reduce_all(tf.math.is_finite(left))):
            print(f'{key} has not finite +ve left')
          if tf.logical_not(tf.reduce_all(tf.math.is_finite(right))):
            print(f'{key} has not finite +ve right')
          yield 1, left, right

          while True:
            other_key = choice(list(ecgs.keys()))
            if other_key != key:
              break

          negative_ecgs=ecgs[other_key]
          negative_right = self.get_negative(negative_ecgs, right_segments=num_right, min_length=length, max_length=length)

          if len(negative_right) < 0:
            print(f'{other_key} has no -ve right')

          right = []
          for l in positive_left:
            for r in negative_right:
              right.append([r])

          right=tf.ragged.constant(right)
          right= right.to_tensor(default_value=padding_value)

          if tf.logical_not(tf.reduce_all(tf.math.is_finite(right))):
            print(f'{other_key} has not finite +ve right')

          yield 0, left, right

    return gen


  def get_gen(self, included_db, pool, left_segments, right_segments, min_length, max_length):
    padding_value=tf.constant(-1e9, tf.float32)

    def gen():
      while True:
        selected_db = choice(included_db)
        ecgs = {}
        if len(pool[selected_db]) <= 2:
          continue
        elif len(pool[selected_db]) > self.pool_size:
          keys = sample(set(pool[selected_db].keys()), self.pool_size)
        else:
          keys = set(pool[selected_db].keys())
        for selected in keys:
          ecgs[selected] = self.get_ecgs_of(selected_db, selected)

        print(f'pool filled: {selected_db}')
        
        for positive_selected, positive_ecgs in ecgs.items():
          if 'apn' in positive_selected:
            m = 24
          elif 'laf' in positive_selected:
            m = 24
          elif 'som' in positive_selected:
            m = 16
          elif 'arr' in positive_selected:
            m = 2
          elif 'fan' in positive_selected:
            m = 4
          else:
            m = 1
          for _ in range(m):
            positive_left, positive_right = self.get_positive_pair(
                positive_ecgs, left_segments=left_segments, right_segments=right_segments,
                min_length=min_length, max_length=max_length
            )

            if len(positive_left) > 0 and len(positive_right) > 0:
              positive_left=tf.ragged.constant(positive_left)
              positive_right=tf.ragged.constant(positive_right)
              positive_left= positive_left.to_tensor(default_value = padding_value)
              positive_right= positive_right.to_tensor(default_value = padding_value)

              if tf.reduce_all(tf.math.is_finite(positive_left)) and tf.reduce_all(tf.math.is_finite(positive_right)):
                yield 1, positive_left, positive_right
                while True:
                  negative_selected = choice(list(ecgs.keys()))
                  if negative_selected != positive_selected:
                    negative_ecgs = ecgs[negative_selected]
                    negative_right = self.get_negative(
                        negative_ecgs, right_segments=right_segments,
                        min_length=min_length, max_length=max_length
                    )
                    if len(negative_right) > 0:
                      negative_right = tf.ragged.constant(negative_right)
                      negative_right = negative_right.to_tensor(default_value=padding_value)
                      if tf.reduce_all(tf.math.is_finite(negative_right)):
                        yield 0, positive_left, negative_right
                        break

    return gen

  def write_to_files(self, ds, output_path, split_name):
    shard_ds=ds.map(dataset_builder.tf_serialize)

    shard=randint(0, 2147483647)

    with tf.io.TFRecordWriter(
            os.path.join(output_path, f"{split_name}-{shard}.tfrecord"),
            tf.io.TFRecordOptions(compression_type='GZIP')) as writer:
      for ex in shard_ds.as_numpy_iterator():
          writer.write(ex)

  def get_pools(self):
    train_pool={}
    val_pool={}
    for db_name, db in self.db_meta.items():
      train_pool[db_name]={}
      val_pool[db_name]={}
      cutoff=floor(len(db.keys())*0.8)
      i=iter(db.items())
      train_pool[db_name]=dict(islice(i, cutoff))
      val_pool[db_name]=dict(i)
    return train_pool, val_pool


  def build_train(self, output_path, entries=16, batch_size=256, max_left=1, max_right=1, min_length=192, max_length=192):
    train_gen=self.get_gen(self.included_db, self.train_pool, 
        left_segments=max_left, right_segments=max_right, min_length=min_length, max_length=max_length
    )
    train_ds=tf.data.Dataset.from_generator(
        train_gen, 
        (tf.int64, tf.float32, tf.float32), 
        output_shapes=([], [None, None], [None, None]),
    )
    padding_value = tf.constant(-1e9, tf.float32)
    
    for _ in range(64):
      shard_ds = train_ds.take(entries * batch_size)
      shard_ds = shard_ds.shuffle(batch_size)
      shard_ds = shard_ds.padded_batch(batch_size,
          padded_shapes=([], [max_left, max_length], [max_right, max_length]),
          padding_values=(tf.constant(0, tf.int64), padding_value, padding_value))
      self.write_to_files(shard_ds, output_path, 'train')

  def build_val(self, output_path, entries=4, batch_size=256, max_left=1, max_right=1, min_length=192, max_length=192):
    val_gen = self.get_gen(self.included_db, self.val_pool, 
        left_segments=max_left, right_segments=max_right, min_length=min_length, max_length=max_length
    )
    val_ds = tf.data.Dataset.from_generator(
        val_gen, 
        (tf.int64, tf.float32, tf.float32), 
        output_shapes=([], [None, None], [None, None]),
    )
    padding_value = tf.constant(-1e9, tf.float32)
    
    for _ in range(32):
      shard_ds = val_ds.take(entries * batch_size)
      shard_ds = shard_ds.padded_batch(batch_size,
          padded_shapes=([], [max_left, max_length], [max_right, max_length]),
          padding_values=(tf.constant(0, tf.int64), padding_value, padding_value))
      self.write_to_files(shard_ds, output_path, 'val')
 

