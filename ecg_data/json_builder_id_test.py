import wfdb
from wfdb import processing
import json, csv
import os
from os import path
from matplotlib import pyplot as plt
import numpy as np
import tensorflow as tf
from random import choice, sample, randint
from ecg_data import data_params

def insert_signals(group, signals, key_prefix, name):
  key = f'{key_prefix}-{name}'
  signals = list(signals)
  if key not in group:
    group[key] = signals
  else:
    group[key].extend(signals)

def process_record(record_entry, data_dir, key_prefix, left=True, length=20, padding=0, fs=128, channel=0):
  record_entry = record_entry.strip()
  print(f'{key_prefix}: {record_entry}')
  record = wfdb.rdrecord(path.join(data_dir, record_entry))
  signal = record.p_signal[:, channel]

  # print(record_entry)
  # print(record.__dict__)
  # wfdb.plot_wfdb(record=record)
  # wfdb.plot_items(signal) 
  # input()

  signal = tf.gather_nd(signal, tf.where(tf.math.is_finite(signal)))

  if not tf.reduce_all(tf.math.is_finite(signal)):
    print(f'not finite: {key_prefix} - {record_entry}')
    print(tf.where(tf.math.is_finite(signal)))
    wfdb.plot_items(signal) 
  
  signal, _ = processing.resample_sig(signal.numpy(), record.fs, fs)

  if left:
    padding = padding * fs
    length = length * fs
    signal = signal[padding : padding + length]
  else:
    padding = padding * fs
    length = length * fs
    if padding == 0:
      signal = signal[-1*(padding + length) :]
    else:
      signal = signal[-1*(padding + length) : -1*padding]

  return signal

def signal_gen(signal, lower, upper=None):
  index = 0
  total_len = len(signal)
  while True:
    if upper is None:
      size = int(lower)
    else:
      size = randint(lower, upper)
    end = index + size
    if end >= total_len:
      if total_len - index > lower:
        yield signal[index:] 
      break
    yield signal[index:end] 
    index = end

def build_group(data_dir, key_prefix, fs=128, channel=0):
  group = {}

  with open(path.join(data_dir, 'RECORDS')) as record_file:
    records = set(record_file)

  person_record_map = {}

  if key_prefix == 'id':
    for record_entry in records:
      record_entry = record_entry.strip()
      name = record_entry.split('/')[0]
      if name not in person_record_map:
        person_record_map[name] = []
      person_record_map[name].append(record_entry)
  elif key_prefix == 'com':
    for record_entry in records:
      record_entry = record_entry.strip()
      name = record_entry[1:]
      if name not in person_record_map:
        person_record_map[name] = [f'b{name}', f'p{name}']
  else:
    for record_entry in records:
      record_entry = record_entry.strip()
      name = record_entry
      if name not in person_record_map:
        person_record_map[name] = [name]

  for key in person_record_map.keys():
    if key_prefix == 'id':
      if len(person_record_map[key]) >= 2:
        sampled = sample(person_record_map[key], 2)
        left_key = sampled[0]
        right_key = sampled[1]
      else:
        left_key = person_record_map[key][0]
        right_key = person_record_map[key][0]
    elif key_prefix == 'com':
      left_key = person_record_map[key][0]
      right_key = person_record_map[key][1]
    else:
      left_key = person_record_map[key][0]
      right_key = person_record_map[key][0]

    while True:
      left_padding = randint(100, 500)
      right_padding = randint(100, 500)
      is_left = True
      is_right = False
      left_length = 128
      right_length = 16

      if key_prefix == 'sin':
        left_padding = randint(30000, 50000)
        right_padding = left_padding + 480 + left_length
        is_left = True
        is_right = True
      elif key_prefix == 'atr':
        left_padding = randint(250, 3000)
        right_padding = left_padding + 480 + left_length
        is_left = True
        is_right = True
      elif key_prefix == 'stc':
        left_padding = randint(100, 500)
        right_padding = left_padding + 480 + left_length
        is_left = True
        is_right = True
      elif key_prefix == 'arr':
        left_padding = randint(250, 2000)
        right_padding = left_padding + 480 + left_length
        is_left = True
        is_right = True
      elif key_prefix == 'com':
        left_padding = 0
        right_padding = 250

      left_signal = process_record(left_key, data_dir, key_prefix, left=is_left, length=left_length, padding=left_padding, fs=fs, channel=channel)      
      right_signal = process_record(right_key, data_dir, key_prefix, left=is_right, length=right_length, padding=right_padding, fs=fs, channel=channel)

      if np.shape(left_signal)[0] == 0 or np.shape(right_signal)[0] == 0:
        continue
      # plot = np.stack([left_signal, right_signal], axis=1)
      # wfdb.plot_items(left_signal, sig_name=f'{key_prefix} {key} left')
      # wfdb.plot_items(right_signal, sig_name=f'{key_prefix} {key} right')

      # d = input()
      # if d == '':
      #   break
      break

    group[key] = []
    group[key].append(left_signal.tolist())
    group[key].append(right_signal.tolist())

  return group

def build_all_groups(save_dir, fs=128, overwrite=False):
  try:
    with open(save_dir) as json_file:
      all_groups = json.load(json_file)
  except:
    all_groups = {}

  for data_meta in data_params.data_source.values():
    key_prefix = data_meta['prefix']

    if overwrite or key_prefix not in all_groups:
      data_dir = data_meta['dir']
      channel = data_meta['channel']
      group = build_group(data_dir, key_prefix, fs=fs, channel=channel)
      all_groups[key_prefix] = group
      print(f'{key_prefix}: {len(group)}')

      with open(save_dir, 'w') as json_file:
        json.dump(all_groups, json_file)
    
    else:
      print(f'{key_prefix} already exists')

