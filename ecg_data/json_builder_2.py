import wfdb
from wfdb import processing
import json, csv
import os
from os import path
from matplotlib import pyplot as plt
import numpy as np
import tensorflow as tf
from random import choice, sample, randint
from ecg_data import data_params
import gzip

def insert_signals(group, signals, key_prefix, name):
  key = f'{key_prefix}-{name}'
  if key not in group:
    group[key] = [signals]
  else:
    group[key].append(signals)

def process_record(record_entry, data_dir, key_prefix, fs, channel=0):
  record_entry = record_entry.strip()
  print(f'{key_prefix}: {record_entry}')
  record = wfdb.rdrecord(path.join(data_dir, record_entry))
  signal = record.p_signal[:, channel]

  # print(record_entry)
  # print(record.__dict__)
  # wfdb.plot_wfdb(record=record)
  # wfdb.plot_items(signal) 
  # input()
  
  signal = tf.gather_nd(signal, tf.where(tf.math.is_finite(signal)))

  if not tf.reduce_all(tf.math.is_finite(signal)):
    print(f'not finite: {key_prefix} - {record_entry}')
    print(tf.where(tf.math.is_finite(signal)))
    wfdb.plot_items(signal) 
  
  signal, _ = processing.resample_sig(signal.numpy(), record.fs, fs)

  return signal.tolist()

def build_group(data_dir, key_prefix, fs, channel=0):
  group = {}

  if key_prefix == 'ptbx':
    with open(path.join(data_dir, 'ptbxl_database.csv')) as csvfile:
      reader = csv.DictReader(csvfile)
      rows = list(reader)
    for record_entry in rows:
      signals = process_record(record_entry['filename_hr'], data_dir, key_prefix, fs, channel)
      insert_signals(group, signals, key_prefix, record_entry['patient_id'])
  else:
    with open(path.join(data_dir, 'RECORDS')) as record_file:
      records = list(record_file)
    for record_entry in records:
      record_entry = record_entry.strip()
      signals = process_record(record_entry, data_dir, key_prefix, fs, channel)
      if key_prefix == 'ptb' or key_prefix == 'id':
        name = record_entry.split('/')[0]
        insert_signals(group, signals, key_prefix, name)
      elif key_prefix == 'com':
        name = name = record_entry[1:]
        insert_signals(group, signals, key_prefix, name)
      elif key_prefix == 'som':
        name = name = record_entry[:5]
        insert_signals(group, signals, key_prefix, name)
      elif key_prefix == 'apn':
        name = record_entry
        insert_signals(group, signals, key_prefix, name)
      else:
        name = record_entry
        insert_signals(group, signals, key_prefix, name)

  return group

def build_all_groups(save_dir, fs=128, overwrite=False):

  meta_path = path.join(save_dir, 'meta.json')
  if path.exists(meta_path):
    with open(meta_path) as json_file:
      meta = json.load(json_file)
  else:
    meta = {}

  for data_meta in data_params.data_source.values():
    key_prefix = data_meta['prefix']

    data_dir = data_meta['dir']
    channel = data_meta['channel']
    group = build_group(data_dir, key_prefix, fs=fs, channel=channel)
    print(f'{key_prefix}: {len(group)}')

    if key_prefix == 'ptb' or key_prefix == 'ptbx' or key_prefix == 'id':
      save_path = path.join(save_dir, f'{key_prefix}')
      with gzip.open(save_path, 'wt', compresslevel=1) as file:
        json.dump(group, file)
    else:
      for k, v in group.items():
        save_path = path.join(save_dir, f'{k}')
        with gzip.open(save_path, 'wt', compresslevel=1) as file:
          json.dump(v, file)

    meta[key_prefix] = {}
    for name, ecgs in group.items():
      meta[key_prefix][name] = []
      for ecg in ecgs:
        meta[key_prefix][name].append(len(ecg))

    with open(meta_path, 'w') as json_file:
      json.dump(meta, json_file, indent=2)

    
