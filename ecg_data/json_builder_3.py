import csv
import gzip
import json
from os import path
import tensorflow as tf

import wfdb
from wfdb import processing

_points_per_file = 32

def _batch_signals(signal, fs):
  i = 0
  signal_len = len(signal)
  while i < signal_len:
    if i + (2 * _points_per_file * fs) >= signal_len:
      end = signal_len
      yield signal[i:end]
      break
    else:
      end = i + (_points_per_file * fs)
      yield signal[i:end]
    i = end


class JsonBuilder:
  def __init__(self, save_dir, build_info, fs=128):
    self.save_dir = save_dir
    self.build_info = build_info
    self.fs = fs
    self.meta_path = path.join(save_dir, 'meta.json')

  def build(self):
    for build_meta in self.build_info.values():
      self.build_group(build_meta)
  
  def build_group(self, build_meta):
    key_prefix = build_meta['prefix']
    data_dir = build_meta['dir']
    channel = build_meta['channel']

    if key_prefix == 'ptbx':
      with open(path.join(data_dir, 'ptbxl_database.csv')) as csvfile:
        reader = csv.DictReader(csvfile)
        rows = list(reader)
      
      if path.exists(self.meta_path):
        with open(self.meta_path) as json_file:
          meta = json.load(json_file)
      else:
        meta = {}
      
      signals = {}
      for i, record_entry in enumerate(rows):
        signal = self.process_record(record_entry['filename_hr'], data_dir, key_prefix, channel)
        key = f"{key_prefix}_{record_entry['patient_id']}".replace('.', '_')
        print(key)
        if key not in meta:
          meta[key] = []
        if key not in signals:
          signals[key] = []
        for s in _batch_signals(signal, self.fs):
          i = len(meta[key])
          file_name = f'{key}_{i}'
          meta[key].append(file_name)
          signals[key].append(s)

      save_path = path.join(self.save_dir, 'ptbx')
      with gzip.open(save_path, 'wt', compresslevel=1) as file:
        json.dump(signals, file)
      with open(self.meta_path, 'w') as json_file:
        json.dump(meta, json_file, indent=2)

    else:
      with open(path.join(data_dir, 'RECORDS')) as record_file:
        records = list(record_file)
      for record_entry in records:
        record_entry = record_entry.strip()
        signals = self.process_record(record_entry, data_dir, key_prefix, channel)
        if key_prefix == 'ptb' or key_prefix == 'id':
          name = record_entry.split('/')[0]
        elif key_prefix == 'com':
          name = record_entry[1:]
        elif key_prefix == 'som':
          name = record_entry[:5]
        else:
          name = record_entry
        self.save_signals(signals, key_prefix, name)

  def process_record(self, record_entry, data_dir, key_prefix, channel=0):
    record_entry = record_entry.strip()
    record = wfdb.rdrecord(path.join(data_dir, record_entry))
    signal = record.p_signal[:, channel]

    # print(record_entry)
    # print(record.__dict__)
    # wfdb.plot_wfdb(record=record)
    # wfdb.plot_items(signal) 
    # input()
    
    signal = tf.gather_nd(signal, tf.where(tf.math.is_finite(signal)))

    if not tf.reduce_all(tf.math.is_finite(signal)):
      print(f'not finite: {key_prefix} - {record_entry}')
      print(tf.where(tf.math.is_finite(signal)))
      # wfdb.plot_items(signal)
    
    signal, _ = processing.resample_sig(signal.numpy(), record.fs, self.fs)

    return signal.tolist()


  def save_signals(self, signal, key_prefix, name):
    if path.exists(self.meta_path):
      with open(self.meta_path) as json_file:
        meta = json.load(json_file)
    else:
      meta = {}

    key = f'{key_prefix}_{name}'.replace('.', '_')
    if key not in meta:
      meta[key] = []

    print(key)

    for s in _batch_signals(signal, self.fs):
      i = len(meta[key])
      file_name = f'{key}_{i}'
      save_path = path.join(self.save_dir, f'{file_name}')
      with gzip.open(save_path, 'wt', compresslevel=1) as file:
        json.dump(s, file)
      meta[key].append(file_name)

    with open(self.meta_path, 'w') as json_file:
      json.dump(meta, json_file, indent=2)
    
