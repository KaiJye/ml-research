import os
import json
import wfdb
import numpy as np
from wfdb import processing
import matplotlib.pyplot as plt


ptb_path = "D:\data\ecg\ptb-diagnostic-ecg-database-1.0.0"
ecgid_path = "D:\data\ecg\ecg-id-database-1.0.0"
resample_frequency = 128

def process_record(path, channel):
    record = wfdb.rdrecord(path)
    signal = record.p_signal[:, channel]
    signal, _ = processing.resample_sig(signal, record.fs, resample_frequency)

    # plt.plot(signal[:384])
    # plt.show()
    # print(record.__dict__)
    # wfdb.plot_wfdb(record=record)
    # exit()

    return signal


def process_db(db_path, meta_file_name):
    subjects = {}

    with open(os.path.join(db_path, 'RECORDS')) as record_file:
        records = list(record_file)

    for record_entry in records:
        record_entry = record_entry.strip()

        record_split = record_entry.split("/")
        name = record_split[0]
        file_name = record_split[1]

        data_path = os.path.join(db_path, record_entry)
        data = process_record(data_path, 0)
        file_path = f"{name}_{file_name}.npy"
        np.save(os.path.join("generated", file_path), data)

        if name in subjects:
            subject = subjects[name]
        else:
            subject = {
                "name": name,
                "files": [],
            }
            subjects[name] = subject
        
        files = subject["files"]

        files.append(file_path)
    
    subject_metas = list(subjects.values())

    with open(os.path.join("generated", meta_file_name), 'w') as file:
        json.dump({ "subjects": subject_metas }, file)


def main():
    process_db(ptb_path, "ptbdb.json")
    process_db(ecgid_path, "ecgiddb.json")


if __name__ == "__main__":
    main()