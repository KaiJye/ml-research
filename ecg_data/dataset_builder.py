import os, json
import numpy as np
from ecg_data import data_params
from random import sample, shuffle, randint
from common import dataset_utils
import tensorflow as tf


def equal_gen(ids, num):
  min_left = num[0]
  max_left = num[1]
  min_right = num[2]
  max_right = num[3]
  num_pos = num[4]
  num_neg = num[5]
  max_length = num[6]
  padding_value = tf.constant(-1e9, tf.float32)

  def gen():
    while True:
      num_left = randint(min_left, max_left)
      num_right = randint(min_right, max_right)

      while True:
        sampled_ids = sample(ids, 2)
        shuffle(sampled_ids)
        v0, v1 = sampled_ids[0], sampled_ids[1]
        if len(v0) >= num_left + num_right and len(v1) >= num_right:
          break
      
      positive = sample(v0, num_left + num_right)
      shuffle(positive)

      positive_left: tf.RaggedTensor = tf.ragged.constant(positive[:num_left])
      positive_right: tf.RaggedTensor = tf.ragged.constant(positive[num_left:])

      left = positive_left.to_tensor(default_value=padding_value)
      right = positive_right.to_tensor(default_value=padding_value)

      yield 1, left, right

      for _ in range(num_neg):
        left = sample(v0, num_left)
        # shuffle(left)
        negative_left: tf.RaggedTensor = tf.ragged.constant(left)

        right = sample(v1, num_right)
        # shuffle(right)
        negative_right: tf.RaggedTensor = tf.ragged.constant(right)
        
        left = tf.random.shuffle(negative_left.to_tensor(default_value=padding_value))
        right = tf.random.shuffle(negative_right.to_tensor(default_value=padding_value))

        yield 0, left, right
  
  return gen


def serialize(label, d0, d1):
  feature = {
      'label': dataset_utils.int64_feature(label.numpy()),
      'd0': dataset_utils.byte_feature([tf.io.serialize_tensor(d0).numpy()]),
      'd1': dataset_utils.byte_feature([tf.io.serialize_tensor(d1).numpy()]),
  }
  example_proto = tf.train.Example(features=tf.train.Features(feature=feature))
  return example_proto.SerializeToString()


def tf_serialize(label, d0, d1):
  tf_string = tf.py_function(serialize, (label, d0, d1), tf.string)
  return tf.reshape(tf_string, ())


def write_to_files(ds: tf.data.Dataset, output_path, split_name, shards, entries):
  for shard in range(shards):
    print(f'{split_name}: {shard}')
    shard_ds = ds.take(entries)
    shard_ds = shard_ds.map(tf_serialize)
    
    with tf.io.TFRecordWriter(
        os.path.join(output_path, f"{split_name}-{shard}.tfrecord"), 
        tf.io.TFRecordOptions(compression_type='GZIP')) as writer:
      for ex in shard_ds.as_numpy_iterator():
        writer.write(ex)


def get_ds(json_dir, output_path, **args):
  min_left = args['min_left']
  max_left = args['max_left']
  min_right = args['min_right']
  max_right = args['max_right']
  max_length = args['max_length']

  num_neg = args['num_neg']

  train_batch_size = args['train_batch_size']
  train_shards = args['train_shards']
  train_entries = args['train_entries']

  val_batch_size = args['val_batch_size']
  val_shards = args['val_shards']
  val_entries = args['val_entries']
  
  json_path = os.path.join(json_dir, f"db_person.json")

  with open(json_path) as json_file:
    db_person = json.load(json_file)

  selected_train = []
  selected_val = []
  for meta in data_params.data_source.values():
    prefix = meta['prefix']
    num_train = meta['train']
    num_val = meta['val']
    # pool = list(filter(lambda x: len(x) >= max_left + max_right, db_person[prefix].values()))
    pool = list(db_person[prefix].values())
    db = sample(pool, num_train + num_val)
    shuffle(db)
    selected_train.extend(db[:num_train])
    selected_val.extend(db[num_train:])

  padding_value = tf.constant(-1e9, tf.float32)

  train_gen = equal_gen(selected_train, [min_left, max_left, min_right, max_right, 1, num_neg, max_length])
  train_ds = tf.data.Dataset.from_generator(
      train_gen, 
      (tf.int64, tf.float32, tf.float32), 
      output_shapes=([], [None, None], [None, None]),
  )
  train_ds = train_ds.shuffle(train_batch_size)
  train_ds = train_ds.padded_batch(train_batch_size,
      padded_shapes=([], [max_left, max_length], [max_right, max_length]),
      padding_values=(tf.constant(0, tf.int64), padding_value, padding_value))

  val_gen = equal_gen(selected_val, [min_left, max_left, min_right, max_right, 1, num_neg, max_length])
  val_ds = tf.data.Dataset.from_generator(
      val_gen, 
      (tf.int64, tf.float32, tf.float32), 
      output_shapes=([], [None, None], [None, None]),
  )
  val_ds = val_ds.padded_batch(val_batch_size,
      padded_shapes=([], [max_left, max_length], [max_right, max_length]),
      padding_values=(tf.constant(0, tf.int64), padding_value, padding_value))

  write_to_files(train_ds, output_path, 'train', train_shards, train_entries)
  write_to_files(val_ds, output_path, 'val', val_shards, val_entries)
