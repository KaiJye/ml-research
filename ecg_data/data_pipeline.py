import tensorflow as tf
import os, json
from random import sample, choice

_READ_RECORD_BUFFER = 64 * 1024 * 1024

def get_ds(params, split):
  file_pattern = os.path.join(params['data_dir'] or "", f"*{split}*")
  ds = tf.data.Dataset.list_files(file_pattern, shuffle=True)
  options = tf.data.Options()
  options.experimental_deterministic = False
  ds = ds.interleave(
      _load_records,
      cycle_length=tf.data.experimental.AUTOTUNE,
      num_parallel_calls=tf.data.experimental.AUTOTUNE).with_options(options)

  batch_size = params['batch_size']
  num_left = params['num_left']
  num_right = params['num_right']
  max_length = params['max_length']
  padding_value = tf.cast(-1e9, tf.float32)

  def _parse_example(serialized_example):
    sparse_params = tf.io.parse_tensor(serialized_example, tf.string)
    sparse = tf.SparseTensor(tf.io.parse_tensor(sparse_params[0], tf.int64), 
        tf.io.parse_tensor(sparse_params[1], tf.float32), 
        tf.io.parse_tensor(sparse_params[2], tf.int64))
    
    label = tf.sparse.slice(sparse, [0, 0, 0], [1, 1, 1])
    label = tf.sparse.to_dense(label)
    label = tf.cast(tf.squeeze(label), tf.int64)

    d0 = tf.sparse.slice(sparse, [1, 0, 0], [1, num_left, max_length])
    d0 = tf.sparse.to_dense(d0, padding_value)
    d0 = tf.squeeze(d0)

    d1 = tf.sparse.slice(sparse, [2, 0, 0], [1, num_right, max_length])
    d1 = tf.sparse.to_dense(d1, padding_value)
    d1 = tf.squeeze(d1)

    return label, d0, d1

  ds = ds.map(_parse_example,
      num_parallel_calls=tf.data.experimental.AUTOTUNE)

  ds = ds.padded_batch(batch_size, 
      padded_shapes=([], [num_left, max_length], [num_right, max_length]), 
      padding_values=(tf.constant(0, tf.int64), padding_value, padding_value))

  def ds_map(label, d0, d1):
    return (d0, d1), label

  ds = ds.map(ds_map, num_parallel_calls=tf.data.experimental.AUTOTUNE)
  
  ds = ds.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)

  return ds


def _load_records(filename):
  return tf.data.TFRecordDataset(filename, compression_type='GZIP', 
      buffer_size=_READ_RECORD_BUFFER, num_parallel_reads=tf.data.experimental.AUTOTUNE)
