import csv, json, os
# import wfdb
from random import choice
import numpy as np
# import tensorflow as tf
from os import path
# from common import model_utils
# from ecg_v4 import data_pipeline
from matplotlib import pyplot as plt
import gzip

# with open('D:\data\ecg\ptb-diagnostic-ecg-database-1.0.0\RECORDS') as file:
#   records = list(file)

# ppl = []
# more = set()
# for record_entry in records:
#   name = record_entry.split('/')[0]
#   if len(list(filter(lambda x: x == name, ppl))) >= 2:
#     more.add(name)
#   ppl.append(name)

# print(len(more))

# def get_ds(params, split):
#   file_pattern = os.path.join(params['data_dir'] or "", f"*{split}*")
#   ds = tf.data.Dataset.list_files(file_pattern, shuffle=True)
#   options = tf.data.Options()
#   options.experimental_deterministic = False
#   ds = ds.interleave(
#       _load_records,
#       cycle_length=tf.data.experimental.AUTOTUNE,
#       num_parallel_calls=tf.data.experimental.AUTOTUNE).with_options(options)

#   def _parse_example(serialized_example):
#     data_fields = {
#         'label': tf.io.FixedLenFeature([], tf.int64),
#         'd0': tf.io.FixedLenFeature([], tf.string),
#         'd1': tf.io.FixedLenFeature([], tf.string),
#     }
#     parsed = tf.io.parse_single_example(serialized_example, data_fields)
#     label = tf.cast(parsed['label'], tf.int64)
#     d0 = tf.io.parse_tensor(parsed['d0'], tf.float32)
#     d1 = tf.io.parse_tensor(parsed['d1'], tf.float32)
#     return label, d0, d1

#   ds = ds.map(_parse_example,
#       num_parallel_calls=tf.data.experimental.AUTOTUNE)
  
#   padding_value = tf.constant(-1e9, tf.float32)
#   ds = ds.padded_batch(
#       params['batch_size'],
#       padded_shapes=([], [params['max_length']], [params['num_right'], params['max_length']]),
#       padding_values=(tf.constant(0, tf.int64), padding_value, padding_value)
#   )

#   def ds_map(label, d0, d1):
#     return ((label, d0, d1), )

#   ds = ds.map(ds_map, num_parallel_calls=tf.data.experimental.AUTOTUNE)
  
#   ds = ds.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)

#   return ds


# def _load_records(filename):
#   return tf.data.TFRecordDataset(filename, compression_type='GZIP', 
#       buffer_size=256 * 1024 * 1024, num_parallel_reads=tf.data.experimental.AUTOTUNE)


# params = {
#   'data_dir': r'D:\data\128',
#   'batch_size': 8,
#   'max_length': 384,
#   'num_right': 32,
# }

# ds = get_ds(params, 'val')
# for ex in ds.take(1):
#   # print(tf.shape(ex[0][2]))
#   plt.plot(ex[0][2][0][5])
#   plt.show()


with gzip.open(r"C:\Users\Kai\Downloads\apn_a01_0", 'rt') as file:
    data = json.load(file)
print(len(data))
plt.plot(data[64*0:], color='black')
plt.show()