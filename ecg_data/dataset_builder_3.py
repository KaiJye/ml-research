import os, json, gzip
import numpy as np
from ecg_data import dataset_builder
from math import floor
from itertools import islice
from random import sample, randint, random, choice
from functools import lru_cache
import tensorflow as tf


class DatasetBuilder:
  def __init__(self, data_path, included_db, fs=128, pool_size=8):
    self.data_path = data_path
    self.included_db = included_db
    self.fs = fs
    self.pool_size = pool_size

    with open(os.path.join(data_path, 'meta.json')) as file:
        db_meta = json.load(file)
    self.db_meta = {k:v for (k,v) in db_meta.items() if k in self.included_db}

    self.train_pool, self.val_pool = self.get_pools()

    with gzip.open(os.path.join(data_path, 'ptb'), 'rt') as file:
        self.ptb = json.load(file)
    with gzip.open(os.path.join(data_path, 'ptbx'), 'rt') as file:
        self.ptbx = json.load(file)
    with gzip.open(os.path.join(data_path, 'id'), 'rt') as file:
        self.id_db = json.load(file)

  def get_ecgs_of(self, db_name, person_name):
    if db_name == 'ptbx':
      return self.ptbx[person_name]
    elif db_name == 'ptb':
      return self.ptb[person_name]
    elif db_name == 'id':
      return self.id_db[person_name]
    elif db_name == 'apn' or db_name == 'laf' or db_name == 'atr' or db_name == 'som':
      return self.get_other_ecgs_cache(person_name)
    else:
      return self.get_other_ecgs(person_name)

  def get_other_ecgs(self, person_name):
    with gzip.open(os.path.join(self.data_path, person_name)) as file:
      ecgs=json.load(file)
    return ecgs

  # @lru_cache(maxsize = 64)
  def get_other_ecgs_cache(self, person_name):
    with gzip.open(os.path.join(self.data_path, person_name)) as file:
      ecgs=json.load(file)
    return ecgs


  def normalize(self, signal):
    signal=np.array(signal, dtype = np.float32)
    return 2.*(signal - np.amin(signal))/np.ptp(signal) - 1

  def get_one_set(self, signal, max_length):
    if len(signal) <= max_length:
      return self.normalize(signal)
    else:
      pos=randint(0, len(signal) - max_length)
      return self.normalize(signal[pos: pos + max_length])

  def get_positive_pair(self, ecgs, left_length, right_length):
    right_length=randint(2*self.fs, right_length)
    if len(ecgs) > 1:
      selected=sample(ecgs, 2)
      left=self.get_one_set(selected[0], left_length)
      right=self.get_one_set(selected[1], right_length)
    else:
      ecg=ecgs[0]
      split_pos=int(len(ecg)/3)
      if random() < 0.5:
        left=ecg[:split_pos*2]
        right=ecg[split_pos*2:]
      else:
        left=ecg[split_pos*2:]
        right=ecg[:split_pos*2]

      left=self.get_one_set(left, left_length)
      right=self.get_one_set(right, right_length)

    return left, right


  def get_negative(self, ecgs, right_length):
    right_length=randint(2*self.fs, right_length)
    if len(ecgs) > 1:
      selected=choice(ecgs)
    else:
      selected=ecgs[0]
    right=self.get_one_set(selected, right_length)

    return right


  def get_gen_with_keys(self, ecgs, left_length, right_length):

    def gen():
      for key in ecgs.keys():
        for _ in range(10):
          positive_ecgs=ecgs[key]
          positive_left, positive_right = self.get_positive_pair(
              positive_ecgs, left_length, right_length
          )

          if len(positive_left) < 2*self.fs:
            print(f'{key} has no +ve left')
          if len(positive_right) < 1*self.fs:
            print(f'{key} has no +ve right')

          if tf.logical_not(tf.reduce_all(tf.math.is_finite(positive_left))):
            print(f'{key} has not finite +ve left')
          if tf.logical_not(tf.reduce_all(tf.math.is_finite(positive_right))):
            print(f'{key} has not finite +ve right')
          yield 1, positive_left, positive_right

          while True:
            other_key = choice(list(ecgs.keys()))
            if other_key != key:
              break

          negative_ecgs = ecgs[other_key]
          negative_right = self.get_negative(negative_ecgs, right_length)

          if len(negative_right) < 0:
            print(f'{other_key} has no -ve right')

          if tf.logical_not(tf.reduce_all(tf.math.is_finite(negative_right))):
            print(f'{other_key} has not finite +ve right')

          yield 0, positive_left, negative_right

    return gen


  def get_gen(self, included_db, pool, left_length, right_length):

    def gen():
      while True:
        selected_db = choice(included_db)
        ecgs = {}
        if len(pool[selected_db]) <= 2:
          continue
        elif len(pool[selected_db]) > self.pool_size:
          keys = sample(set(pool[selected_db].keys()), self.pool_size)
        else:
          keys = set(pool[selected_db].keys())
        for selected in keys:
          ecgs[selected] = self.get_ecgs_of(selected_db, selected)

        print(f'pool filled: {selected_db}')
        
        for positive_selected, positive_ecgs in ecgs.items():
          if 'apn' in positive_selected:
            m = 24
          elif 'laf' in positive_selected:
            m = 24
          elif 'lt' in positive_selected:
            m = 8
          elif 'som' in positive_selected:
            m = 16
          elif 'arr' in positive_selected:
            m = 4
          else:
            m = 1
          for _ in range(m):
            positive_left, positive_right = self.get_positive_pair(
                positive_ecgs, left_length, right_length
            )

            if len(positive_left) > 2*self.fs and len(positive_right) > 1*self.fs:

              if tf.reduce_all(tf.math.is_finite(positive_left)) and tf.reduce_all(tf.math.is_finite(positive_right)):
                yield 1, positive_left, positive_right
                while True:
                  negative_selected = choice(list(ecgs.keys()))
                  if negative_selected != positive_selected:
                    negative_ecgs = ecgs[negative_selected]
                    negative_right = self.get_negative(
                        negative_ecgs, right_length
                    )
                    if len(negative_right) > 0:
                      if tf.reduce_all(tf.math.is_finite(negative_right)):
                        yield 0, positive_left, negative_right
                        break

    return gen

  def write_to_files(self, ds, output_path, split_name):
    shard_ds=ds.map(dataset_builder.tf_serialize)

    shard=randint(0, 2147483647)

    with tf.io.TFRecordWriter(
            os.path.join(output_path, f"{split_name}-{shard}.tfrecord"),
            tf.io.TFRecordOptions(compression_type='GZIP')) as writer:
      for ex in shard_ds.as_numpy_iterator():
          writer.write(ex)

  def get_pools(self):
    train_pool={}
    val_pool={}
    for db_name, db in self.db_meta.items():
      train_pool[db_name]={}
      val_pool[db_name]={}
      cutoff=floor(len(db.keys())*0.9)
      i=iter(db.items())
      train_pool[db_name]=dict(islice(i, cutoff))
      val_pool[db_name]=dict(i)
    return train_pool, val_pool


  def build_train(self, output_path, entries=8, batch_size=256, left_length=16, right_length=6):
    left_length = left_length*self.fs
    right_length = right_length*self.fs
    train_gen=self.get_gen(
        self.included_db, self.train_pool, 
        left_length, right_length
    )
    train_ds=tf.data.Dataset.from_generator(
        train_gen, 
        (tf.int64, tf.float32, tf.float32), 
        output_shapes=([], [None], [None]),
    )
    padding_value = tf.constant(-1e9, tf.float32)
    
    while True:
      shard_ds = train_ds.take(entries * batch_size)
      shard_ds = shard_ds.shuffle(batch_size)
      shard_ds = shard_ds.padded_batch(batch_size,
          padded_shapes=([], [left_length], [right_length]),
          padding_values=(tf.constant(0, tf.int64), padding_value, padding_value))
      self.write_to_files(shard_ds, output_path, 'train')

  def build_val(self, output_path, entries=4, batch_size=256, left_length=16, right_length=6):
    left_length = left_length*self.fs
    right_length = right_length*self.fs
    val_gen = self.get_gen(
        self.included_db, self.val_pool, 
        left_length, right_length
    )
    val_ds = tf.data.Dataset.from_generator(
        val_gen, 
        (tf.int64, tf.float32, tf.float32), 
        output_shapes=([], [None], [None]),
    )
    padding_value = tf.constant(-1e9, tf.float32)
    
    for _ in range(32):
      shard_ds = val_ds.take(entries * batch_size)
      shard_ds = shard_ds.padded_batch(batch_size,
          padded_shapes=([], [left_length], [right_length]),
          padding_values=(tf.constant(0, tf.int64), padding_value, padding_value))
      self.write_to_files(shard_ds, output_path, 'val')
 

