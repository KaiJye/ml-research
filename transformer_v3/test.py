import tensorflow as tf

a = tf.constant([[1, 5.04, -2.3, -5.6, 0, 0], [10, 1.04, 2.3, 0, 0, 0]])
z = tf.cast(tf.equal(a, 0), dtype=tf.float32)
print(tf.logical_not(tf.equal(a, 0)))

z = z * 1e9
a += z
b = tf.argsort(a, stable=True)
print(b)

# a = tf.constant([[[1], [2], [3]], [[1], [2], [3]]])
# print(tf.squeeze(a))