import tensorflow as tf
import tensorflow_datasets as tfds
import os
from transformer_v3 import transformer
from transformer_v1 import optimizer
from transformer_v3 import data_pipeline
from transformer_v1 import evaluate
from transformer_v3 import metrics

class TransformerTask:
  def __init__(self, params):
    self.params = params

    try:
      resolver = tf.distribute.cluster_resolver.TPUClusterResolver(tpu='grpc://' + os.environ['COLAB_TPU_ADDR'])
      tf.config.experimental_connect_to_cluster(resolver)
      tf.tpu.experimental.initialize_tpu_system(resolver)
      self.distribution_strategy = tf.distribute.experimental.TPUStrategy(resolver)
      self.params['static_batch'] = True
    except:
      self.distribution_strategy = tf.distribute.MirroredStrategy()
      self.params['static_batch'] = False

  def train(self):
    params = self.params

    with self.distribution_strategy.scope():
      model = transformer.create_training_model(params)
      opt = optimizer.create_optimizer(params)

      checkpoint_path = params['model_dir']

      ckpt = tf.train.Checkpoint(model=model, optimizer=opt)

      ckpt_manager = tf.train.CheckpointManager(ckpt, checkpoint_path, max_to_keep=params['max_to_keep'])

      current_step = 0
      if ckpt_manager.latest_checkpoint:
        ckpt.restore(ckpt_manager.latest_checkpoint)
        print ('Latest checkpoint restored!!')
        current_step = opt.iterations.numpy()
      
      print('current train step', current_step)
      loss_fn = metrics.loss_function_1(params["label_smoothing"], params["vocab_size"])
      model.compile(optimizer=opt, loss=(metrics.position_loss_fn, loss_fn))

    model.summary()

    train_ds = data_pipeline.get_train_ds_3(params)

    callbacks = self._create_callbacks(ckpt_manager)

    current_epoch = 0
    while current_epoch < params['epochs']:
      model.fit(train_ds,
          initial_epoch=current_epoch,
          epochs=current_epoch + 1,
          steps_per_epoch=params['steps_per_epoch'],
          callbacks=callbacks,
          verbose=params['verbose'])

      ckpt_manager.save()

      current_epoch += 1

  def eval(self):
    params = self.params

    with self.distribution_strategy.scope():
      model = transformer.create_eval_model(params)

      checkpoint_path = params['model_dir']

      ckpt = tf.train.Checkpoint(model=model)

      ckpt_manager = tf.train.CheckpointManager(ckpt, checkpoint_path, max_to_keep=params['max_to_keep'])

      # if a checkpoint exists, restore the latest checkpoint.
      if ckpt_manager.latest_checkpoint:
        ckpt.restore(ckpt_manager.latest_checkpoint).expect_partial()
        print ('Latest checkpoint restored!!')
      
      model.compile()

    model.summary()

    ds = data_pipeline.get_test_ds(params)
    # ds = data_pipeline.eval_input_fn(params)
    # map_data_fn = data_pipeline.map_test_data_for_transformer_fn
    # ds = ds.map(
    #     map_data_fn, num_parallel_calls=params["num_parallel_calls"])

    if params['single_batch_test']:
      ds = ds.take(1)

    reference = tf.constant([], tf.string)
    translated = tf.constant([], tf.string)
    for (en, de) in ds:
      de = evaluate.decode(params, de[:, 1:])
      reference = tf.concat([reference, de], axis=0)
      ids = model(en)
      translated_batch = evaluate.decode(params, ids[:, 1:])

      if params['single_batch_test']:
        en = evaluate.decode(params, en[:, 1:])
        print(en)
        print(de)
        print(translated_batch)

      translated = tf.concat([translated, translated_batch], axis=0)
      print("translated", translated.shape[0])

    print("computing bleu")
    bleu_score = evaluate.compute_bleu(reference, translated)

    return bleu_score

  def _create_callbacks(self, ckpt_manager=None):
    callbacks = []
    learning_rate_callback = optimizer.LearningRateCallback(self.params)
    callbacks.append(learning_rate_callback)
    # if ckpt_manager:
    #   ckpt_callback = CheckpointCallback(ckpt_manager)
    #   callbacks.append(ckpt_callback)
    return callbacks

class CheckpointCallback(tf.keras.callbacks.Callback):

  def __init__(self, ckpt_manager):
    super(CheckpointCallback, self).__init__()

    self.ckpt_manager = ckpt_manager

  def on_train_batch_end(self, batch, logs=None):
    if self.model.optimizer.iterations.numpy() < 50000:
      return
    if self.model.optimizer.iterations.numpy() % 10000 == 0:
      self.ckpt_manager.save()
      print("checkpoint saved")

